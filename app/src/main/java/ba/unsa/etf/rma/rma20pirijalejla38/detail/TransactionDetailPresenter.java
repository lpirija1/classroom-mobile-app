package ba.unsa.etf.rma.rma20pirijalejla38.detail;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;
import ba.unsa.etf.rma.rma20pirijalejla38.detail.ITransactionDetailPresenter;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionDBOpenHelper;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionListInteractor;

public class TransactionDetailPresenter implements ITransactionDetailPresenter, TransactionDetailInteractor.TransactionsDeleteDone, TransactionDetailInteractor.OnTransactionsSearchDone, TransactionDetailInteractor.VracanjeTransakcija {
    private Context context;
    private IDetailView view;
    private Transaction transaction;
    private ArrayList<Transaction> transactions;
    private ITransactionDetailInteractor transactionDetailInteractor;
    private static int id = 0;

    public TransactionDetailPresenter(Context context, IDetailView view) {
        this.context = context;
        this.view = view;
        transaction = new Transaction();
        transactions = new ArrayList<>();
        transactionDetailInteractor = new TransactionDetailInteractor();
    }


    public TransactionDetailPresenter(Context context) {
        this.context = context;
        transaction = new Transaction();
        transactions = new ArrayList<>();
        this.transactionDetailInteractor=new TransactionDetailInteractor();
    }

    @Override
    public void onDone(Transaction transakcija) {
        this.transaction = transakcija;
        if(view != null) {
            view.setAccount();
        }
    }

    @Override
    public void getListTransactios(String... query){
        new TransactionDetailInteractor((TransactionDetailInteractor.OnTransactionsSearchDone)
                this).execute(query);
    }

    @Override
    public void dajSveTransakcije(String... query) {
        new TransactionDetailInteractor((TransactionDetailInteractor.VracanjeTransakcija)this).execute(query);
    }

    @Override
    public void onDone(Transaction transaction, ArrayList<Transaction> transactions) {
        this.transactions.addAll(transactions);
       if(transaction != null) view.postaviPodatke(transaction);
       else {
           if(view != null) {
               view.setAccount();
           }
       }
    }



    public void setTransaction(Parcelable p){
        transaction = (Transaction) p;
    }

    public Transaction getTransaction(){
        return transaction;
    }

    public void changeTransaction(String ... query){
        new TransactionDetailInteractor((TransactionDetailInteractor.OnTransactionsSearchDone) this).execute(query);
    }
    public void addTransaction(String ... query){
        new TransactionDetailInteractor((TransactionDetailInteractor.OnTransactionsSearchDone) this).execute(query);
    }

    public void deleteTransaction(int id) {
        new TransactionDetailInteractor((TransactionDetailInteractor.TransactionsDeleteDone) this).execute(Integer.toString(id));
    }

    public double iznosZaMjesec(Date date) {
        double iznos = 0;
       for(Transaction t: transactions){
            SimpleDateFormat f1 = new SimpleDateFormat("MM");
            String s = f1.format(date);
            int currentMonth = Integer.parseInt(s);
            SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
            String s1 = f2.format(date);
            int currentYear = Integer.parseInt(s1);
            if (Transaction.TransactionType.INDIVIDUALPAYMENT.equals(t.getType()) || Transaction.TransactionType.INDIVIDUALINCOME.equals(t.getType()) || Transaction.TransactionType.PURCHASE.equals(t.getType())) {
                if ((Integer.parseInt(f1.format(t.getDate())) == currentMonth ||  Integer.parseInt(f2.format(t.getDate())) == currentYear)){
                    if (t.getType().equals(Transaction.TransactionType.INDIVIDUALPAYMENT) || t.getType().equals(Transaction.TransactionType.PURCHASE)) {
                        iznos-=t.getAmount();
                    }
                    else iznos+=t.getAmount();
                }
            } else {
                if ((currentMonth>=Integer.parseInt(f1.format(t.getDate())) && currentYear==Integer.parseInt(f2.format(t.getDate())) &&currentYear==Integer.parseInt(f2.format(t.getEndDate())) && currentMonth<=Integer.parseInt(f1.format(t.getEndDate())))
                || (currentYear>Integer.parseInt(f2.format(t.getDate())) && currentYear==Integer.parseInt(f2.format(t.getEndDate())) && currentMonth<=Integer.parseInt(f1.format(t.getEndDate())))
                || (currentYear>Integer.parseInt(f2.format(t.getDate())) && currentYear<Integer.parseInt(f2.format(t.getEndDate())))
                || (currentYear == Integer.parseInt(f2.format(t.getDate())) && currentYear<Integer.parseInt(f2.format(t.getDate())) && currentMonth>=Integer.parseInt(f1.format(t.getDate())))){
                      if(t.getType().equals(Transaction.TransactionType.REGULARPAYMENT)) iznos-=t.getAmount();
                      else iznos+=t.getAmount();
                }}}
        return iznos;
    }
    public double ukupanIznos() {
        double suma = 0;
        double iznos;
       for(Transaction t: transactions) {
            if(t.getType().equals(Transaction.TransactionType.INDIVIDUALPAYMENT) || t.getType().equals(Transaction.TransactionType.REGULARPAYMENT) || t.getType().equals(Transaction.TransactionType.PURCHASE)){
                iznos = t.getAmount()*(-1);
            }
            else{
                iznos = t.getAmount();
            }
            suma+=iznos;
        }
        return suma;
    }
    @Override
    public void create(Date date, double amount, String title, Transaction.TransactionType type, String itemDescription, String transactionInterval, String endDate){
        transaction = new Transaction(date, amount, title, type, itemDescription, transactionInterval, endDate);
    }

    @Override
    public Transaction kreirajTransakciju(int id, Date date, double amount, String title, Transaction.TransactionType type, String itemDescription, String transactionInterval, String endDate, String akcija){
        return new Transaction(id, date, amount, title, type, itemDescription, transactionInterval, endDate, akcija);
    }
    @Override
    public void onDone(ArrayList<Transaction> transakcije) {
        transactions.addAll(transakcije);
        view.vratiSeNaView();
    }

    @Override
    public void getDatabaseTransaction(int id) {
        transaction = transactionDetailInteractor.getTransaction(context,id);
    }


    @Override
    public void dodajTransakciju(Context context, int id, Date datum, double iznos, String title, Transaction.TransactionType type, String itemDescription, String tInterval, String endDate, String akcija) {
        transactionDetailInteractor.dodajTransakciju(context,id, datum,iznos,title,type, itemDescription, tInterval, endDate, akcija);
    }

    @Override
    public void obrisiTransakcijuUBazi(Transaction transaction) {
        transactionDetailInteractor.obrisiTransakcijuUBazi(context, transaction.getId());
    }

    @Override
    public void izmijeniTransakciju(Context context, int id, Date datum, double iznos, String title, Transaction.TransactionType type, String itemDescription, String tInterval, String endDate, String akcija) {
        transactionDetailInteractor.izmijeniTransakciju(context,id, datum,iznos,title,type, itemDescription, tInterval, endDate, akcija);
    }
}
