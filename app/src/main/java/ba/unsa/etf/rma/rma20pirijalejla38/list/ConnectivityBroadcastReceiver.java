package ba.unsa.etf.rma.rma20pirijalejla38.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;


import ba.unsa.etf.rma.rma20pirijalejla38.detail.TransactionDetailPresenter;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.AccountListInteractor;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.AccountListPresenter;

public class ConnectivityBroadcastReceiver extends BroadcastReceiver {
    private static int p = 0;
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) {
            if(p != 0) {
                AccountListInteractor interactor = new AccountListInteractor();
                Cursor cursor = interactor.getAccountCursor(context);
                if(cursor.moveToFirst()) {
                    interactor.deleteAccountInDB(context, cursor.getInt(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_ID)));
                }

                interactor.dodajAccountUBazu(context);
            }
            else {
                AccountListInteractor interactor = new AccountListInteractor();
                Cursor cursor = interactor.getAccountCursor(context);
                if(!cursor.moveToFirst()) {
                    interactor.kreirajAccountUBazi(context);
                }
            }
            p++;
            Toast toast = Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            p++;
            TransactionListInteractor interactor = new TransactionListInteractor();
            TransactionDetailPresenter presenter = new TransactionDetailPresenter(context.getApplicationContext());
            Cursor cursor = interactor.getTransactionCursor(context.getApplicationContext());
            if(cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AKCIJA)).equalsIgnoreCase("offline brisanje")) {
                        presenter.deleteTransaction(cursor.getInt(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID)));
                    } else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AKCIJA)).equalsIgnoreCase("offline dodavanje")) {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                        String jsonInputString = null;
                        try {
                            jsonInputString = "{\n" +
                                    "\"title\" : " + "\"" + cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE)) + "\"" + "," + "\n" +
                                    "\"date\" : " + "\"" + format.parse(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE))) + "\"" + "," + "\n" +
                                    "\"amount\" : " + Math.abs(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT)));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (!cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE)).equalsIgnoreCase("null")) {
                            try {
                                jsonInputString += "," + "\n" + "\"endDate\": " + "\"" + format.parse(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE))) + "\"";
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        if (!cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL)).equalsIgnoreCase("null")) {
                            jsonInputString += "," + "\n" + "\"transactionInterval\": " + Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL)));
                        }
                        if (!cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION)).equalsIgnoreCase("null"))
                            jsonInputString += "," + "\n" + "\"itemDescription\": " + "\"" + cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION)) + "\"";

                        String[] zaQuery = {"transactions", jsonInputString, cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE))};
                        presenter.addTransaction(zaQuery);
                    } else {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                        String jsonInputString = null;
                        try {
                            jsonInputString = "{\n" +
                                    "\"title\" : " + "\"" + cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE)) + "\"" + "," + "\n" +
                                    "\"date\" : " + "\"" + format.parse(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE))) + "\"" + "," + "\n" +
                                    "\"amount\" : " + Math.abs(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT)));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (!cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE)).equalsIgnoreCase("null")) {
                            try {
                                jsonInputString += "," + "\n" + "\"endDate\": " + "\"" + format.parse(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE))) + "\"";
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        if (!cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL)).equalsIgnoreCase("null")) {
                            jsonInputString += "," + "\n" + "\"transactionInterval\": " + Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL)));
                        }
                        if (!cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION)).equalsIgnoreCase("null"))
                            jsonInputString += "," + "\n" + "\"itemDescription\": " + "\"" + cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION)) + "\"";

                        presenter.changeTransaction("transactionID", Integer.toString(cursor.getInt(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID))), cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)), jsonInputString);
                    }
                    cursor.moveToNext();
                }
            }
            else {
                System.out.println("prazno");
            }
            AccountListInteractor accountListInteractor = new AccountListInteractor();
            Cursor cursor1 = accountListInteractor.getAccountCursor(context);

            AccountListPresenter accountListPresenter = new AccountListPresenter(context);
            if(cursor1.moveToFirst()){
                String[] values = {"account", Double.toString(cursor1.getDouble(cursor1.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET))), Double.toString(cursor1.getDouble(cursor1.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT))), Double.toString(cursor1.getDouble(cursor1.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT)))};
                accountListPresenter.editAccount(values);
            }
            interactor.deleteDB(context);
            Toast toast = Toast.makeText(context, "Connected", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
