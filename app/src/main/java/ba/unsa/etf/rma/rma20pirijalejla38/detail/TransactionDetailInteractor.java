package ba.unsa.etf.rma.rma20pirijalejla38.detail;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionDBOpenHelper;


public class TransactionDetailInteractor extends AsyncTask<String, Integer, Void> implements ITransactionDetailInteractor {
    private TransactionsDeleteDone caller;
    private OnTransactionsSearchDone caller1;
    private VracanjeTransakcija caller2;
    private Transaction transactionVrati;
    private ArrayList<Transaction.TransactionType> transactionTypes;
    private ArrayList<Transaction> transactions;
    public static int id = 0;

    public interface TransactionsDeleteDone {
        void onDone(Transaction transakcija);
    }

    public interface VracanjeTransakcija {
        void onDone(ArrayList<Transaction> transakcije);
    }

    public interface OnTransactionsSearchDone {
        public void onDone(Transaction transaction, ArrayList<Transaction> transactions);
    }

    public TransactionDetailInteractor() {
    }

    ;


    public TransactionDetailInteractor(TransactionsDeleteDone p) {
        caller = p;
        transactions = new ArrayList<>();
        transactionTypes = new ArrayList<>();
        for (int i = 0; i < 10; i++) transactionTypes.add(null);
    }

    ;

    public TransactionDetailInteractor(OnTransactionsSearchDone p) {
        caller1 = p;
        transactionVrati = null;
        transactions = new ArrayList<>();
        transactionTypes = new ArrayList<>();
        for (int i = 0; i < 10; i++) transactionTypes.add(null);
    }

    public TransactionDetailInteractor(VracanjeTransakcija p) {
        caller2 = p;
        transactions = new ArrayList<>();
        transactionTypes = new ArrayList<>();
        for (int i = 0; i < 10; i++) transactionTypes.add(null);
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }


    @Override
    protected Void doInBackground(String... strings) {

        if (!strings[0].equalsIgnoreCase("transactions") && !strings[0].equalsIgnoreCase("transactionID")) { //brisanje
            String query = null;
            try {
                query = URLEncoder.encode(strings[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            String mainUrl = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/";

            int page = 0;
            try {
                String urlPom = mainUrl + "account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/transactions" + "?page=" + page;
                URL urlPom1 = new URL(urlPom);
                HttpURLConnection urlConnectionPom = (HttpURLConnection) urlPom1.openConnection();
                InputStream in1 = new BufferedInputStream(urlConnectionPom.getInputStream());
                String result1 = convertStreamToString(in1);
                JSONObject jo1 = new JSONObject(result1);
                JSONArray results1 = jo1.getJSONArray("transactions");

                String urlForTypes = mainUrl + "transactionTypes";
                URL urlTypes = new URL(urlForTypes);
                HttpURLConnection connection = (HttpURLConnection) urlTypes.openConnection();
                InputStream is = new BufferedInputStream(connection.getInputStream());
                String string = convertStreamToString(is);
                JSONObject j = new JSONObject(string);
                JSONArray ja = j.getJSONArray("rows");
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject obj = ja.getJSONObject(i);
                    if (obj.getString("name").equalsIgnoreCase("Regular payment"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARPAYMENT);
                    else if (obj.getString("name").equalsIgnoreCase("Regular income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARINCOME);
                    else if (obj.getString("name").equalsIgnoreCase("Purchase"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.PURCHASE);
                    else if (obj.getString("name").equalsIgnoreCase("Individual income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALINCOME);
                    else
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALPAYMENT);

                }

                while (results1.length() > 0) {
                    String url1 = mainUrl + "account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/transactions" + "?page=" + page;
                    URL url = new URL(url1);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(in);
                    JSONObject jo = new JSONObject(result);
                    JSONArray results = jo.getJSONArray("transactions");
                    if (results.length() == 0) break;
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject transaction = results.getJSONObject(i);
                        int idTrans = transaction.getInt("id");
                        String datumPocetka = transaction.getString("date");
                        String naziv = transaction.getString("title");
                        double iznos = transaction.getDouble("amount");
                        String itemDescription = transaction.getString("itemDescription");
                        String interval = transaction.getString("transactionInterval");
                        String endDate = transaction.getString("endDate");
                        int transactionTypeID = transaction.getInt("TransactionTypeId");

                        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                        Date output = parser.parse(datumPocetka);

                        if (idTrans == Integer.parseInt(query)) {
                            transactionVrati = new Transaction(idTrans, output, iznos, naziv, transactionTypes.get(transactionTypeID), itemDescription, interval, endDate);
                            System.out.println("naziv  " + transactionVrati.getTitle());
                            break;
                        }
                    }
                    results1 = results;
                    page++;

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String Url = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/transactions/" + query;

            try {
                URL url = new URL(Url);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("DELETE");
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.connect();

                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    System.out.println(response.toString());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (strings[0].equalsIgnoreCase("transactions") && strings.length > 2 && strings[1].equalsIgnoreCase("sveTransakcije")) {
            String query = null;
            int id = Integer.parseInt(strings[2]);
            try {
                query = URLEncoder.encode(strings[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String mainUrl = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/";

            int page = 0;
            try {
                String urlPom = mainUrl + "account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/" + query + "?page=" + page;
                URL urlPom1 = new URL(urlPom);
                HttpURLConnection urlConnectionPom = (HttpURLConnection) urlPom1.openConnection();
                InputStream in1 = new BufferedInputStream(urlConnectionPom.getInputStream());
                String result1 = convertStreamToString(in1);
                JSONObject jo1 = new JSONObject(result1);
                JSONArray results1 = jo1.getJSONArray("transactions");

                String urlForTypes = mainUrl + "transactionTypes";
                URL urlTypes = new URL(urlForTypes);
                HttpURLConnection connection = (HttpURLConnection) urlTypes.openConnection();
                InputStream is = new BufferedInputStream(connection.getInputStream());
                String string = convertStreamToString(is);
                JSONObject j = new JSONObject(string);
                JSONArray ja = j.getJSONArray("rows");
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject obj = ja.getJSONObject(i);
                    if (obj.getString("name").equalsIgnoreCase("Regular payment"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARPAYMENT);
                    else if (obj.getString("name").equalsIgnoreCase("Regular income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARINCOME);
                    else if (obj.getString("name").equalsIgnoreCase("Purchase"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.PURCHASE);
                    else if (obj.getString("name").equalsIgnoreCase("Individual income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALINCOME);
                    else
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALPAYMENT);

                }

                while (results1.length() > 0) {
                    String url1 = mainUrl + "account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/" + query + "?page=" + page;
                    URL url = new URL(url1);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(in);
                    JSONObject jo = new JSONObject(result);
                    JSONArray results = jo.getJSONArray("transactions");
                    if (results.length() == 0) break;
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject transaction = results.getJSONObject(i);
                        int idTrans = transaction.getInt("id");
                        String datumPocetka = transaction.getString("date");
                        String naziv = transaction.getString("title");
                        double iznos = transaction.getDouble("amount");
                        String itemDescription = transaction.getString("itemDescription");
                        String interval = transaction.getString("transactionInterval");
                        String endDate = transaction.getString("endDate");
                        int transactionTypeID = transaction.getInt("TransactionTypeId");

                        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                        Date output = parser.parse(datumPocetka);

                        if (idTrans == id) {
                            transactionVrati = new Transaction(idTrans, output, iznos, naziv, transactionTypes.get(transactionTypeID), itemDescription, interval, endDate);
                        }
                        transactions.add(new Transaction(idTrans, output, iznos, naziv, transactionTypes.get(transactionTypeID), itemDescription, interval, endDate));
                    }
                    results1 = results;
                    page++;

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (strings[0].equalsIgnoreCase("transactions") && strings.length == 2 && strings[1].equalsIgnoreCase("sveTransakcije")) {
            String query = null;
            try {
                query = URLEncoder.encode(strings[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String mainUrl = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/";

            int page = 0;
            try {
                String urlPom = mainUrl + "account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/" + query + "?page=" + page;
                URL urlPom1 = new URL(urlPom);
                HttpURLConnection urlConnectionPom = (HttpURLConnection) urlPom1.openConnection();
                InputStream in1 = new BufferedInputStream(urlConnectionPom.getInputStream());
                String result1 = convertStreamToString(in1);
                JSONObject jo1 = new JSONObject(result1);
                JSONArray results1 = jo1.getJSONArray("transactions");

                String urlForTypes = mainUrl + "transactionTypes";
                URL urlTypes = new URL(urlForTypes);
                HttpURLConnection connection = (HttpURLConnection) urlTypes.openConnection();
                InputStream is = new BufferedInputStream(connection.getInputStream());
                String string = convertStreamToString(is);
                JSONObject j = new JSONObject(string);
                JSONArray ja = j.getJSONArray("rows");
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject obj = ja.getJSONObject(i);
                    if (obj.getString("name").equalsIgnoreCase("Regular payment"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARPAYMENT);
                    else if (obj.getString("name").equalsIgnoreCase("Regular income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARINCOME);
                    else if (obj.getString("name").equalsIgnoreCase("Purchase"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.PURCHASE);
                    else if (obj.getString("name").equalsIgnoreCase("Individual income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALINCOME);
                    else
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALPAYMENT);

                }

                while (results1.length() > 0) {
                    String url1 = mainUrl + "account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/" + query + "?page=" + page;
                    URL url = new URL(url1);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(in);
                    JSONObject jo = new JSONObject(result);
                    JSONArray results = jo.getJSONArray("transactions");
                    if (results.length() == 0) break;
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject transaction = results.getJSONObject(i);
                        int idTrans = transaction.getInt("id");
                        String datumPocetka = transaction.getString("date");
                        String naziv = transaction.getString("title");
                        double iznos = transaction.getDouble("amount");
                        String itemDescription = transaction.getString("itemDescription");
                        String interval = transaction.getString("transactionInterval");
                        String endDate = transaction.getString("endDate");
                        int transactionTypeID = transaction.getInt("TransactionTypeId");

                        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                        Date output = parser.parse(datumPocetka);
                        transactions.add(new Transaction(idTrans, output, iznos, naziv, transactionTypes.get(transactionTypeID), itemDescription, interval, endDate));
                    }
                    results1 = results;
                    page++;

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (strings[0].equalsIgnoreCase("transactionID")) {  //azuriranje
            String mainUrl = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/";
            String urlForTypes = mainUrl + "transactionTypes";
            URL urlTypes = null;
            try {
                urlTypes = new URL(urlForTypes);
                HttpURLConnection connection = (HttpURLConnection) urlTypes.openConnection();
                InputStream is = new BufferedInputStream(connection.getInputStream());
                String string = convertStreamToString(is);
                JSONObject j = new JSONObject(string);
                JSONArray ja = j.getJSONArray("rows");
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject obj = ja.getJSONObject(i);
                    if (obj.getString("name").equalsIgnoreCase("Regular payment"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARPAYMENT);
                    else if (obj.getString("name").equalsIgnoreCase("Regular income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARINCOME);
                    else if (obj.getString("name").equalsIgnoreCase("Purchase"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.PURCHASE);
                    else if (obj.getString("name").equalsIgnoreCase("Individual income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALINCOME);
                    else
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALPAYMENT);

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            int tip = 0;
            String typeId = null;
            String idTransakcije = null;
            try {
                idTransakcije = URLEncoder.encode(strings[1], "utf-8");
                typeId = URLEncoder.encode(strings[2], "utf-8");
                if (typeId.equalsIgnoreCase("INDIVIDUALINCOME"))
                    tip = transactionTypes.indexOf(Transaction.TransactionType.INDIVIDUALINCOME);
                else if (typeId.equalsIgnoreCase("INDIVIDUALPAYMENT"))
                    tip = transactionTypes.indexOf(Transaction.TransactionType.INDIVIDUALPAYMENT);
                else if (typeId.equalsIgnoreCase("PURCHASE"))
                    tip = transactionTypes.indexOf(Transaction.TransactionType.PURCHASE);
                else if (typeId.equalsIgnoreCase("REGULARINCOME"))
                    tip = transactionTypes.indexOf(Transaction.TransactionType.REGULARINCOME);
                else tip = transactionTypes.indexOf(Transaction.TransactionType.REGULARPAYMENT);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String Url = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/transactions/" + idTransakcije;
            URL url = null;

            String jsonInputString = strings[3];
            jsonInputString += "," + "\n" + "\"typeId\" : " + tip + "}";
            System.out.println(jsonInputString);
            try {
                url = new URL(Url);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.connect();
                try (OutputStream os = con.getOutputStream()) {
                    byte[] input = jsonInputString.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = con.getResponseCode();
                System.out.println(code);

                try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    String provjera = "{" + "\"error\":" + "\"" + "Nije moguće ažurirati transakciju!" + "\"" + "}";
                    if(response.toString().equalsIgnoreCase(provjera)) {
                        dodajNaServer(jsonInputString);
                    }
                    else {
                        System.out.println(response.toString());
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {      //dodavanje
            String mainUrl = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/";
            String urlForTypes = mainUrl + "transactionTypes";
            URL urlTypes = null;
            try {
                urlTypes = new URL(urlForTypes);
                HttpURLConnection connection = (HttpURLConnection) urlTypes.openConnection();
                InputStream is = new BufferedInputStream(connection.getInputStream());
                String string = convertStreamToString(is);
                JSONObject j = new JSONObject(string);
                JSONArray ja = j.getJSONArray("rows");
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject obj = ja.getJSONObject(i);
                    if (obj.getString("name").equalsIgnoreCase("Regular payment"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARPAYMENT);
                    else if (obj.getString("name").equalsIgnoreCase("Regular income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARINCOME);
                    else if (obj.getString("name").equalsIgnoreCase("Purchase"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.PURCHASE);
                    else if (obj.getString("name").equalsIgnoreCase("Individual income"))
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALINCOME);
                    else
                        transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALPAYMENT);

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            int tip = 0;
            String typeId = null;
            try {
                typeId = URLEncoder.encode(strings[2], "utf-8");
                if (typeId.equalsIgnoreCase("INDIVIDUALINCOME"))
                    tip = transactionTypes.indexOf(Transaction.TransactionType.INDIVIDUALINCOME);
                else if (typeId.equalsIgnoreCase("INDIVIDUALPAYMENT"))
                    tip = transactionTypes.indexOf(Transaction.TransactionType.INDIVIDUALPAYMENT);
                else if (typeId.equalsIgnoreCase("PURCHASE"))
                    tip = transactionTypes.indexOf(Transaction.TransactionType.PURCHASE);
                else if (typeId.equalsIgnoreCase("Regular income"))
                    tip = transactionTypes.indexOf(Transaction.TransactionType.REGULARINCOME);
                else tip = transactionTypes.indexOf(Transaction.TransactionType.REGULARPAYMENT);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            String query1 = null;
            try {
                query1 = URLEncoder.encode(strings[0], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String Url = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/" + query1;
            URL url = null;

            String jsonInputString = strings[1];
            jsonInputString += "," + "\n" + "\"typeId\" : " + tip + "}";
            System.out.println(jsonInputString);
            try {
                url = new URL(Url);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.connect();
                try (OutputStream os = con.getOutputStream()) {
                    byte[] input = jsonInputString.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = con.getResponseCode();
                System.out.println(code);

                try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    System.out.println(response.toString());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (caller != null) caller.onDone(transactionVrati);
        if (caller1 != null) caller1.onDone(transactionVrati, transactions);
        if (caller2 != null) caller2.onDone(transactions);
    }

    @Override
    public Transaction getTransaction(Context context, Integer id) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = null;
        Uri adresa = ContentUris.withAppendedId(Uri.parse("content://rma.provider.transactions/elements"), id);
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cursor = cr.query(adresa, kolone, where, whereArgs, order);
        if (cursor != null) {
            cursor.moveToFirst();
            int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
            int internalId = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
            int titlePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
            int datePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
            int amountPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
            int typePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE);
            int itemDescriptionPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION);
            int endDatePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE);
            int intervalPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL);
            int akcijaPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AKCIJA);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            Transaction.TransactionType tip;
            if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("INDIVIDUALPAYMENT"))
                tip = Transaction.TransactionType.INDIVIDUALPAYMENT;
            else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("REGULARPAYMENT"))
                tip = Transaction.TransactionType.REGULARPAYMENT;
            else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("PURCHASE"))
                tip = Transaction.TransactionType.PURCHASE;
            else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("INDIVIDUALINCOME")) {
                tip = Transaction.TransactionType.INDIVIDUALINCOME;
            } else {
                tip = Transaction.TransactionType.REGULARINCOME;
            }

            try {
                transactionVrati = new Transaction(cursor.getInt(idPos), format.parse(cursor.getString(datePos)), cursor.getDouble(amountPos), cursor.getString(titlePos), tip, cursor.getString(itemDescriptionPos), cursor.getString(intervalPos), cursor.getString(endDatePos), cursor.getString(akcijaPos));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        cursor.close();
        return transactionVrati;
    }

    @Override
    public void dodajTransakciju(Context context, int idT, Date datum, double iznos, String title, Transaction.TransactionType type, String itemDescription, String tInterval, String endDate, String akcija) {
        ContentValues values = new ContentValues();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        if(idT == -1) {
            values.put(TransactionDBOpenHelper.TRANSACTION_ID, id);
            id++;
        }
        else values.put(TransactionDBOpenHelper.TRANSACTION_ID, idT);
        values.put(TransactionDBOpenHelper.TRANSACTION_DATE, format.format(datum));
        values.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, iznos);
        values.put(TransactionDBOpenHelper.TRANSACTION_TITLE, title);
        values.put(TransactionDBOpenHelper.TRANSACTION_TYPE, type.toString());
        values.put(TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION, itemDescription);
        values.put(TransactionDBOpenHelper.TRANSACTION_INTERVAL, tInterval);
        values.put(TransactionDBOpenHelper.TRANSACTION_ENDDATE, endDate);
        values.put(TransactionDBOpenHelper.TRANSACTION_AKCIJA, akcija);


        ContentResolver resolver = context.getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://rma.provider.transactions/elements");
        resolver.insert(uri, values);
    }

    private void dodajNaServer(String json) {
        System.out.println("dodaj");
        String Url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/" + "transactions";
        URL url1 = null;
        System.out.println(json);
        try {
            url1 = new URL(Url1);
            HttpURLConnection con1 = (HttpURLConnection) url1.openConnection();
            con1.setRequestMethod("POST");
            con1.setDoOutput(true);
            con1.setDoInput(true);
            con1.setRequestProperty("Content-Type", "application/json");
            con1.setRequestProperty("Accept", "application/json");
            con1.connect();
            try (OutputStream os = con1.getOutputStream()) {
                byte[] input = json.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            int code1 = con1.getResponseCode();
            System.out.println(code1);

            try (BufferedReader br1 = new BufferedReader(new InputStreamReader(con1.getInputStream(), "utf-8"))) {
                StringBuilder response1 = new StringBuilder();
                String responseLine1 = null;
                while ((responseLine1 = br1.readLine()) != null) {
                    response1.append(responseLine1.trim());
                }
                System.out.println(response1.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void obrisiTransakcijuUBazi(Context context, int idTransakcije) {
        ContentResolver resolver = context.getApplicationContext().getContentResolver();
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.transactions/elements"), idTransakcije);
        String where = TransactionDBOpenHelper.TRANSACTION_ID + "=" +idTransakcije;
        String[] whereArgs = null;

        resolver.delete(uri, where, whereArgs);
    }

    @Override
    public void izmijeniTransakciju(Context context, int idT, Date datum, double iznos, String title, Transaction.TransactionType type, String itemDescription, String tInterval, String endDate, String akcija) {
        ContentValues values = new ContentValues();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        values.put(TransactionDBOpenHelper.TRANSACTION_ID, idT);
        values.put(TransactionDBOpenHelper.TRANSACTION_DATE, format.format(datum));
        values.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, iznos);
        values.put(TransactionDBOpenHelper.TRANSACTION_TITLE, title);
        values.put(TransactionDBOpenHelper.TRANSACTION_TYPE, type.toString());
        values.put(TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION, itemDescription);
        values.put(TransactionDBOpenHelper.TRANSACTION_INTERVAL, tInterval);
        values.put(TransactionDBOpenHelper.TRANSACTION_ENDDATE, endDate);
        values.put(TransactionDBOpenHelper.TRANSACTION_AKCIJA, akcija);


        ContentResolver resolver = context.getApplicationContext().getContentResolver();
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.transactions/elements"), idT);
        String where = TransactionDBOpenHelper.TRANSACTION_ID + "=" +idT;
        String[] whereArgs = null;
        resolver.update(uri, values, where, whereArgs);
    }


}
