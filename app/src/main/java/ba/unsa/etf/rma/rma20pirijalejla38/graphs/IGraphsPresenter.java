package ba.unsa.etf.rma.rma20pirijalejla38.graphs;

import java.io.Serializable;
import java.util.Date;

public interface IGraphsPresenter {
    double dajPotrosnjuZaMjesec(int mjesec, int godina, Date datum);

    Date getDatum();

    void setDatum(Serializable datum);

    double dajZaraduZaMjesec(int mjesec, int trenutnaGodina, Date datum);

    double dajUkupanIznosZaMjesec(int mjesec, int trenutnaGodina, Date datum);

    double dajPotrosnjuZaDan(Date datum);

    double dajZaraduZaDan(Date datum);

    double dajUkupnoStanjeZaDan(Date datum);

    double dajZaraduZaSedmicu(int mjesec, int godina, int sedmica);

    double dajPotrosnjuZaSedmicu(int mjesec, int godina, int sedmica);

    double dajUkupanIznosZaSedmicu(int mjesec, int godina, int sedmica);

    public void getListTransactios(String query);
}
