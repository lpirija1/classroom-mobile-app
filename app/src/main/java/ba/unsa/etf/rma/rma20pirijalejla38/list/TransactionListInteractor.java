package ba.unsa.etf.rma.rma20pirijalejla38.list;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;
import ba.unsa.etf.rma.rma20pirijalejla38.data.TransactionsModel;

public class TransactionListInteractor extends AsyncTask<String, Integer, Void> implements ITransactionListInteractor {

    private String api_key="";
    public static ArrayList<Transaction> transactions = new ArrayList<>();   //SVE TRANSAKCIJE
    private ArrayList<Transaction.TransactionType> transactionTypes;
    private OnTransactionsSearchDone caller;



    @Override
    public  ArrayList<Transaction> getTransactions() {
        return transactions;
    }


    public interface OnTransactionsSearchDone{
        public void onDone(ArrayList<Transaction> results);
    }

    public TransactionListInteractor() {
    }

    public TransactionListInteractor(OnTransactionsSearchDone p) {
        caller = p;
        transactions = new ArrayList<Transaction>();
        transactionTypes = new ArrayList<>();
        for(int i = 0; i<10; i++) transactionTypes.add(null);
    };


    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    protected Void doInBackground(String... strings) {
        String query = null;
        transactions.clear();
        try {
            query = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String mainUrl = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/";
        if(query.equalsIgnoreCase("transactions")) {
            int page = 0;
            try {
                String urlPom = mainUrl + "account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/" + query + "?page="+page;
                URL urlPom1 = new URL(urlPom);
                HttpURLConnection urlConnectionPom = (HttpURLConnection) urlPom1.openConnection();
                InputStream in1 = new BufferedInputStream(urlConnectionPom.getInputStream());
                String result1 = convertStreamToString(in1);
                JSONObject jo1 = new JSONObject(result1);
                JSONArray results1 = jo1.getJSONArray("transactions");

                String urlForTypes = mainUrl + "transactionTypes";
                URL urlTypes = new URL(urlForTypes);
                HttpURLConnection connection = (HttpURLConnection) urlTypes.openConnection();
                InputStream is = new BufferedInputStream(connection.getInputStream());
                String string = convertStreamToString(is);
                JSONObject j = new JSONObject(string);
                JSONArray ja = j.getJSONArray("rows");
                for(int i = 0; i<ja.length(); i++) {
                    JSONObject obj = ja.getJSONObject(i);
                    if(obj.getString("name").equalsIgnoreCase("Regular payment")) transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARPAYMENT);
                    else if(obj.getString("name").equalsIgnoreCase("Regular income")) transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.REGULARINCOME);
                    else if(obj.getString("name").equalsIgnoreCase("Purchase")) transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.PURCHASE);
                    else if(obj.getString("name").equalsIgnoreCase("Individual income")) transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALINCOME);
                    else transactionTypes.set(obj.getInt("id"), Transaction.TransactionType.INDIVIDUALPAYMENT);

                }

                while(results1.length()>0) {
                    String url1 = mainUrl + "account/" + "f3237344-c9ef-46aa-ad98-f549896c2ab9" + "/" + query + "?page="+page;
                    URL url = new URL(url1);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(in);
                    JSONObject jo = new JSONObject(result);
                    JSONArray results = jo.getJSONArray("transactions");
                    if(results.length()==0) break;
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject transaction = results.getJSONObject(i);
                        int id = transaction.getInt("id");
                        String datumPocetka = transaction.getString("date");
                        String naziv = transaction.getString("title");
                        double iznos = transaction.getDouble("amount");
                        String itemDescription = transaction.getString("itemDescription");
                        String interval = transaction.getString("transactionInterval");
                        String endDate = transaction.getString("endDate");
                        int transactionTypeID = transaction.getInt("TransactionTypeId");
                        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                        Date output = parser.parse(datumPocetka);
                        transactions.add(new Transaction(id, output, iznos, naziv, transactionTypes.get(transactionTypeID), itemDescription, interval, endDate));
                    }
                    results1 = results;
                    page++;

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone(transactions);
    }

    @Override
    public Cursor getTransactionCursor(Context context) {
        ContentResolver cr = context.getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID,
                TransactionDBOpenHelper.TRANSACTION_ID,
                TransactionDBOpenHelper.TRANSACTION_TITLE,
                TransactionDBOpenHelper.TRANSACTION_DATE,
                TransactionDBOpenHelper.TRANSACTION_AMOUNT,
                TransactionDBOpenHelper.TRANSACTION_TYPE,
                TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION,
                TransactionDBOpenHelper.TRANSACTION_ENDDATE,
                TransactionDBOpenHelper.TRANSACTION_INTERVAL,
                TransactionDBOpenHelper.TRANSACTION_AKCIJA
        };
        Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cur = cr.query(adresa,kolone,where,whereArgs,order);
        return cur;
    }
    @Override
    public void deleteDB(Context context) {
        ContentResolver resolver = context.getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://rma.provider.transactions/elements");
        resolver.delete(uri, null, null);
        Uri uri1 = Uri.parse("content://rma.provider.account/elements");
        resolver.delete(uri1, null, null);
    }

}

