package ba.unsa.etf.rma.rma20pirijalejla38.detailAccount;


import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcelable;
import android.view.View;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;
import ba.unsa.etf.rma.rma20pirijalejla38.detail.IDetailView;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.AccountListInteractor;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.IAccountListPresenter;
import ba.unsa.etf.rma.rma20pirijalejla38.list.ITransactionListView;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionDBOpenHelper;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionListFragment;

public class AccountListPresenter implements IAccountListPresenter, AccountListInteractor.OnAccountSearchDone, AccountListInteractor.EditAccount, AccountListInteractor.AccountZbogBrisanjaTransakcije, AccountListInteractor.AccountZbogEditovanjaTransakcije {
    private Context context;
    private ITransactionListView view1;
    private  IAccountDetailView view2;
    private IDetailView view3;
    private Account account1;
    private IAccountListInteractor accountListInteractor;

    public AccountListPresenter(Context context, ITransactionListView view) {
        this.context = context;
           view1 = view;
    }
    public AccountListPresenter(Context context, IAccountDetailView view) {
        this.context = context;
        view2 = view;
    }

    public AccountListPresenter(Context context, IDetailView view) {
        this.context = context;
        view3 = view;
    }

    public AccountListPresenter(Context context){
        this.context = context;
    }

    public AccountListPresenter(Context context, ITransactionListView view, String razlika) {
        this.context = context;
        view1 = (ITransactionListView) view;
        account1 = new Account();
        accountListInteractor = new AccountListInteractor();
        //account1 = accountListInteractor.dajAccountOffline();
    }

    public AccountListPresenter(Context context, IDetailView view, String razlika) {
        this.context = context;
        view3 = (IDetailView) view;
        account1 = new Account();
        accountListInteractor = new AccountListInteractor();
        account1 = accountListInteractor.dajAccountOffline();
    }

    public AccountListPresenter(Context context, IAccountDetailView view, String razlika) {
        this.context = context;
        view2 = (IAccountDetailView) view;
        account1 = new Account();
        accountListInteractor = new AccountListInteractor();
       // account1 = accountListInteractor.dajAccountOffline();
    }






    @Override
    public void searchAccount(String... query){
        new AccountListInteractor((AccountListInteractor.OnAccountSearchDone)
                this).execute(query);
    }

    @Override
    public void dohvatiAccount(String ... query) {
        new AccountListInteractor((AccountListInteractor.AccountZbogEditovanjaTransakcije)
                this).execute(query);
    }

    @Override
    public void editAccount(String... query) {
        new AccountListInteractor((AccountListInteractor.EditAccount)
                this).execute(query);
    }

    @Override
    public void dajAccountZbogBrisanja(String... query) {
        new AccountListInteractor((AccountListInteractor.AccountZbogBrisanjaTransakcije)this).execute(query);
    }

    @Override
    public void onDone(Account account) {
        if(account!= null) account1 = new Account(account.getBudget(), account.getTotalLimit(), account.getMonthLimit(), account.getId(), account.getAcHash(), account.getEmail());
        if(view1 != null )view1.setAccount(account);
        if(view2 != null ) view2.setAccount(account);
        if(view3 != null) {
             view3.dodajTransakciju(account);
        }
    }


    public void setAccount(Parcelable a) {
        this.account1 =(Account) a;
    }
    public Account getAccount(){
        return this.account1;
    }

    @Override
    public void onDone(Account account1, Account account2) {
        if(view3 != null) {
          view3.vratiSeNazad();
        }
    }

    @Override
    public void onDone(Account account, String brisanje) {
        this.account1 = account;
        view3.obrisiTransakciju();
    }

    @Override
    public void onDone(Account account1, String editovanje, boolean pomocna) {
      if(account1 != null) this.account1 = new Account(account1.getBudget(), account1.getTotalLimit(), account1.getMonthLimit(), account1.getId(), account1.getAcHash(), account1.getEmail());
      if(view3 != null) {
          view3.editujTransakciju(account1);
      }
    }

    @Override
    public void updateAccount(Context context, int id, double iznos, double monthLImit, double totalLimit){
        accountListInteractor.updateAccountInDB(context,id,iznos,monthLImit,totalLimit);
    }

    @Override
    public void dajAccountIzBaze(Context context){
       Cursor cursor = accountListInteractor.getAccountCursor(context);
       if(cursor.moveToFirst()){
           account1 = new Account();
           account1.setId(cursor.getInt(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_ID)));
           account1.setBudget(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET)));
           account1.setMonthLimit(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT)));
           account1.setTotalLimit(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT)));
           System.out.println("if " + account1.getId() + "  " +  account1.getTotalLimit());
       }
    }

}
