package ba.unsa.etf.rma.rma20pirijalejla38.detail;

import android.content.Context;
import android.os.Parcelable;

import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public interface ITransactionDetailPresenter {
    Transaction getTransaction();
    void changeTransaction(String ... query);
    void addTransaction(String ... query);
    void deleteTransaction(int id);
    void getListTransactios(String... query);
    void dajSveTransakcije(String... query);
    void create(Date date, double amount, String title, Transaction.TransactionType type, String itemDescription, String transactionInterval, String endDate);
    void setTransaction(Parcelable transaction);
    void getDatabaseTransaction(int id);
    void dodajTransakciju(Context context, int id, Date datum, double iznos, String title, Transaction.TransactionType type, String itemDescription, String tInterval, String endDate, String akcija);
    Transaction kreirajTransakciju(int id, Date date, double amount, String title, Transaction.TransactionType type, String itemDescription, String transactionInterval, String endDate, String akcija);
    void obrisiTransakcijuUBazi(Transaction transaction);
    void izmijeniTransakciju(Context context, int id, Date datum, double iznos, String title, Transaction.TransactionType type, String itemDescription, String tInterval, String endDate, String akcija);
}
