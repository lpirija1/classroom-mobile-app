package ba.unsa.etf.rma.rma20pirijalejla38.list;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.AccountListPresenter;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.IAccountListPresenter;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class TransactionListFragment extends Fragment implements ITransactionListView {
    private TextView editTextGlobalAmount;
    private TextView editTextLimit;
    private Spinner spinnerFilter;
    private Button buttonLeft;
    private Button buttonRight;
    private Spinner spinnerSort;
    private ListView listViewTransactions;
    private Button buttonAdd;
    private Calendar ca = GregorianCalendar.getInstance();
    private TransactionListPresenter transactionListPresenter;
    private IAccountListPresenter accountListPresenter;
    private TransactionListAdapter transactionListAdapter;
    private OnItemClick onItemClick;
    private int pozicija = -1;
    private int brojElemenata = 0;

    private TransactionListCursorAdapter transactionListCursorAdapter;

    public ITransactionListPresenter getPresenter() {
        if (transactionListPresenter == null) {
            transactionListPresenter = new TransactionListPresenter(this, getActivity());
        }
        return transactionListPresenter;
    }

    public IAccountListPresenter getAccountPresenter() {
        if (accountListPresenter == null) {
            accountListPresenter = new AccountListPresenter(getContext(), this);
        }
        return accountListPresenter;
    }


    public interface OnItemClick {
        void onItemClicked(Integer transactionID);
        void onItemClick(Transaction transaction, Double monthLimit, Double totalLimit);
        void onItemClicked(Boolean inDatabase, int id);
        void openFragmentRight(Date datum);
        void openFragmentLeft(Date datum);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View fragmentView = inflater.inflate(R.layout.fragment_list, container, false);
        transactionListAdapter = new TransactionListAdapter(Objects.requireNonNull(getActivity()), R.layout.list_element, new ArrayList<Transaction>());
        transactionListCursorAdapter= new TransactionListCursorAdapter(getActivity(), R.layout.list_element,null,false);

        pozicija = -1;
        editTextGlobalAmount = fragmentView.findViewById(R.id.editTextGlobalAmount);
        editTextLimit = fragmentView.findViewById(R.id.editTextLimit);
        spinnerFilter = fragmentView.findViewById(R.id.spinnerFilter);
        spinnerSort = fragmentView.findViewById(R.id.spinnerSort);
        buttonLeft = fragmentView.findViewById(R.id.buttonLeft);
        buttonRight = fragmentView.findViewById(R.id.buttonRight);
        listViewTransactions = fragmentView.findViewById(R.id.listViewTransactions);
        buttonAdd = fragmentView.findViewById(R.id.buttonAdd);
        onItemClick = (OnItemClick) getActivity();


        if(isConnected()) {
            transactionListPresenter = new TransactionListPresenter(this, getActivity());
            MainActivity.pokretanjeOnline++;
            listViewTransactions.setAdapter(transactionListAdapter);
            listViewTransactions.setOnItemClickListener(listItemClickListener);
            transactionListPresenter.setTime(ca);
            transactionListPresenter.getListTransactios("transactions");
            transactionListPresenter.refreshTransactions();

            accountListPresenter = new AccountListPresenter(getContext(), this);
            accountListPresenter.searchAccount("account");


            String[] myItems = getResources().getStringArray(R.array.sorts_array);
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.select_dialog_item, myItems);
            spinnerSort.setAdapter(arrayAdapter);

            Integer[] integers = new Integer[]{R.drawable.bijela, R.drawable.image, R.drawable.individualpayment, R.drawable.regularpayment, R.drawable.purchase, R.drawable.individualincome, R.drawable.regularincome};
            String[] strings = new String[]{"Filter by", "ALL TRANSACTIONS", "INDIVIDUALPAYMENT", "REGULARPAYMENT", "PURCHASE", "INDIVIDUALINCOME", "REGULARINCOME"};
            final SpinnerAdapter adapter = new
                    SpinnerAdapter(getContext(), integers, strings);
            spinnerFilter.setAdapter(adapter);
            spinnerFilter.setSelection(0);

            spinnerSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    String item = (String) parentView.getItemAtPosition(position);
                    transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
                    transactionListPresenter.sort(item);
                    listViewTransactions.setAdapter(transactionListAdapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {

                }

            });

            TextView set_date = (TextView) fragmentView.findViewById(R.id.textViewDate);
            SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
            try {
                set_date.setText(format.format(ca.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    filter(spinnerFilter.getSelectedItemPosition());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }

            });

            buttonRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
                    Date date = ca.getTime();
                    SimpleDateFormat f1 = new SimpleDateFormat("MM");
                    String s = f1.format(date);
                    int month = Integer.parseInt(s);
                    ca.set(Calendar.MONTH, month);
                    TextView set_d = (TextView) fragmentView.findViewById(R.id.textViewDate);
                    set_d.setText(format.format(ca.getTime()));
                    transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
                    listViewTransactions.setAdapter(transactionListAdapter);
                    try {
                        transactionListPresenter.filter(format.parse(set_d.getText().toString()));
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    transactionListPresenter.refreshTransactions();
                    filter(spinnerFilter.getSelectedItemPosition());
                }
            });
            buttonLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
                    ca.add(Calendar.MONTH, -1);
                    TextView set_d1 = (TextView) fragmentView.findViewById(R.id.textViewDate);
                    set_d1.setText(format.format(ca.getTime()));
                    transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
                    listViewTransactions.setAdapter(transactionListAdapter);
                    try {
                        transactionListPresenter.filter(format.parse(set_d1.getText().toString()));
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    transactionListPresenter.refreshTransactions();
                    filter(spinnerFilter.getSelectedItemPosition());
                }
            });
            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onItemClicked(null);
                }
            });

            fragmentView.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onSwipeRight() {
                    onItemClick.openFragmentRight(ca.getTime());
                }

                @Override
                public void onSwipeLeft() {
                    onItemClick.openFragmentLeft(ca.getTime());
                }
            });
        }
        else if(!isConnected() && MainActivity.pokretanjeOnline > 0) {        //pokrenuta app online, pa nestalo konekcije

             transactionListPresenter = new TransactionListPresenter(this, getContext(), "");
            if(getArguments()!= null && getArguments().containsKey("transaction")){
                Transaction t  = getArguments().getParcelable("transaction");
                if(t.getAkcija().equals("offline dodavanje")) {
                    transactionListPresenter.dodajTransakcijuUListu(t);
                }
                else if(t.getAkcija().equals("Offline izmjena") || t.getAkcija().equalsIgnoreCase("offline brisanje")) {
                    System.out.println("offline izmjena" + t.getId());
                    transactionListPresenter.izmijeniTransakciju(t);
                }
            }
            transactionListAdapter = new TransactionListAdapter(Objects.requireNonNull(getActivity()), R.layout.list_element, new ArrayList<Transaction>());
            listViewTransactions.setAdapter(transactionListAdapter);
            listViewTransactions.setOnItemClickListener(listItemClickListener);

            transactionListPresenter.filter(ca.getTime());
            transactionListPresenter.refreshTransactions();
            MainActivity.pokretanjeOnline++;


            onItemClick= (OnItemClick) getActivity();
            accountListPresenter = new AccountListPresenter(getContext(),this,  "");
            accountListPresenter.dajAccountIzBaze(getContext());
            editTextGlobalAmount.setText(Double.toString(accountListPresenter.getAccount().getBudget()));
            editTextLimit.setText(Double.toString(accountListPresenter.getAccount().getTotalLimit()));


            String[] myItems= getResources().getStringArray(R.array.sorts_array);
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.select_dialog_item, myItems);
            spinnerSort.setAdapter(arrayAdapter);

            Integer[] integers=new Integer[]{R.drawable.bijela, R.drawable.image, R.drawable.individualpayment, R.drawable.regularpayment, R.drawable.purchase, R.drawable.individualincome, R.drawable.regularincome};
            String[] strings=new String[]{"Filter by", "ALL TRANSACTIONS", "INDIVIDUALPAYMENT","REGULARPAYMENT", "PURCHASE", "INDIVIDUALINCOME", "REGULARINCOME"};
            final SpinnerAdapter adapter = new
                    SpinnerAdapter(getContext(),integers,strings);
            spinnerFilter.setAdapter(adapter);
            spinnerFilter.setSelection(0);

            TextView set_date = (TextView) fragmentView.findViewById(R.id.textViewDate);
            SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
            try {
                set_date.setText(format.format(ca.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            buttonRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
                    Date date = ca.getTime();
                    SimpleDateFormat f1 = new SimpleDateFormat("MM");
                    String s = f1.format(date);
                    int month = Integer.parseInt(s);
                    ca.set(Calendar.MONTH, month);
                    TextView set_d = (TextView) fragmentView.findViewById(R.id.textViewDate);
                    set_d.setText(format.format(ca.getTime()));
                    transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
                    listViewTransactions.setAdapter(transactionListAdapter);
                    try {
                        transactionListPresenter.filter(format.parse(set_d.getText().toString()));
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    transactionListPresenter.refreshTransactions();
                    filter(spinnerFilter.getSelectedItemPosition());
                }
            });
            buttonLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
                    ca.add(Calendar.MONTH, -1);
                    TextView set_d1 = (TextView) fragmentView.findViewById(R.id.textViewDate);
                    set_d1.setText(format.format(ca.getTime()));
                    transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
                    listViewTransactions.setAdapter(transactionListAdapter);
                    try {
                        transactionListPresenter.filter(format.parse(set_d1.getText().toString()));
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    transactionListPresenter.refreshTransactions();
                    filter(spinnerFilter.getSelectedItemPosition());
                }
            });
            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(accountListPresenter != null) {
                        onItemClick.onItemClick(null, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                    }
                    else {
                        onItemClick.onItemClick(null, 0., 0.);
                    }
                    }});

            fragmentView.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onSwipeRight(){
                    onItemClick.openFragmentRight(ca.getTime());
                }
                @Override
                public void onSwipeLeft(){
                    onItemClick.openFragmentLeft(ca.getTime());
                }
            });
        }
        else if(!isConnected() && MainActivity.pokretanjeOffline>=0){      //pokretanje u offline režimu
            MainActivity.pokretanjeOffline++;
            listViewTransactions.setAdapter(transactionListCursorAdapter);
            listViewTransactions.setOnItemClickListener(listCursorItemClickListener);
            transactionListPresenter = new TransactionListPresenter(this, getContext(), "");
            transactionListPresenter.getTransactionsCursor();


            if(getArguments()!= null && getArguments().containsKey("transaction")){
                Transaction t  = getArguments().getParcelable("transaction");
                if(t.getAkcija().equals("offline dodavanje")) {
                    transactionListPresenter.dodajTransakcijuUListu(t);
                }
                else if(t.getAkcija().equals("Offline izmjena") || t.getAkcija().equalsIgnoreCase("offline brisanje")) {
                    transactionListPresenter.izmijeniTransakciju(t);
                }
            }

            onItemClick= (OnItemClick) getActivity();

            accountListPresenter = new AccountListPresenter(getContext(),this,  "");
            accountListPresenter.dajAccountIzBaze(getContext());
            if(accountListPresenter.getAccount() != null) {
                System.out.println("udje u if" + accountListPresenter.getAccount().getId());
                System.out.println("total limit" + accountListPresenter.getAccount().getTotalLimit());
                System.out.println("month" + accountListPresenter.getAccount().getMonthLimit());
                editTextGlobalAmount.setText(Double.toString(accountListPresenter.getAccount().getBudget()));
                editTextLimit.setText(Double.toString(accountListPresenter.getAccount().getTotalLimit()));
            }

            String[] myItems= getResources().getStringArray(R.array.sorts_array);
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.select_dialog_item, myItems);
            spinnerSort.setAdapter(arrayAdapter);

            Integer[] integers=new Integer[]{R.drawable.bijela, R.drawable.image, R.drawable.individualpayment, R.drawable.regularpayment, R.drawable.purchase, R.drawable.individualincome, R.drawable.regularincome};
            String[] strings=new String[]{"Filter by", "ALL TRANSACTIONS", "INDIVIDUALPAYMENT","REGULARPAYMENT", "PURCHASE", "INDIVIDUALINCOME", "REGULARINCOME"};
            final SpinnerAdapter adapter = new
                    SpinnerAdapter(getContext(),integers,strings);
            spinnerFilter.setAdapter(adapter);
            spinnerFilter.setSelection(0);

            TextView set_date = (TextView) fragmentView.findViewById(R.id.textViewDate);
            SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
            try {
                set_date.setText(format.format(ca.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            buttonRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
                    Date date = ca.getTime();
                    SimpleDateFormat f1 = new SimpleDateFormat("MM");
                    String s = f1.format(date);
                    int month = Integer.parseInt(s);
                    ca.set(Calendar.MONTH, month);
                    TextView set_d = (TextView) fragmentView.findViewById(R.id.textViewDate);
                    set_d.setText(format.format(ca.getTime()));
                    transactionListCursorAdapter= new TransactionListCursorAdapter(getActivity(), R.layout.list_element,null,false);
                    listViewTransactions.setAdapter(transactionListCursorAdapter);
                    try {
                        transactionListPresenter.filter(format.parse(set_d.getText().toString()));
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    transactionListPresenter.refreshTransactions();
                   // filter(spinnerFilter.getSelectedItemPosition());
                }
            });
            buttonLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleDateFormat format = new SimpleDateFormat("MMMM, yyyy");
                    ca.add(Calendar.MONTH, -1);
                    TextView set_d1 = (TextView) fragmentView.findViewById(R.id.textViewDate);
                    set_d1.setText(format.format(ca.getTime()));
                    transactionListCursorAdapter= new TransactionListCursorAdapter(getActivity(), R.layout.list_element,null,false);
                    listViewTransactions.setAdapter(transactionListCursorAdapter);
                    try {
                        transactionListPresenter.filter(format.parse(set_d1.getText().toString()));
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    transactionListPresenter.refreshTransactions();
                   // filter(spinnerFilter.getSelectedItemPosition());
                }
            });


            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(accountListPresenter.getAccount() != null) {
                        onItemClick.onItemClick(null, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                    }
                    else {
                        onItemClick.onItemClick(null, 0., 0.);
                    }
                }});

            fragmentView.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onSwipeRight(){
                    onItemClick.openFragmentRight(ca.getTime());
                }
                @Override
                public void onSwipeLeft(){
                    onItemClick.openFragmentLeft(ca.getTime());
                }
            });


        }

        return fragmentView;
    }





    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Transaction transaction = transactionListAdapter.getTransaction(position);
            if (isConnected()) {
                if (pozicija == position) {
                    onItemClick.onItemClicked(null);
                    pozicija = -1;
                    listViewTransactions.getChildAt(position).setBackgroundColor(Color.TRANSPARENT);
                } else {
                    for (int i = 0; i < listViewTransactions.getChildCount(); i++) {
                        if (position == i) {
                            listViewTransactions.getChildAt(i).setBackgroundColor(Color.BLUE);
                        } else {
                            listViewTransactions.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                        }
                    }
                    brojElemenata = listViewTransactions.getChildCount();
                    pozicija = position;
                    onItemClick.onItemClicked(transaction.getId());
                }

            } else {
                if (pozicija == position) {
                    onItemClick.onItemClick(null, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                    pozicija = -1;
                    listViewTransactions.getChildAt(position).setBackgroundColor(Color.TRANSPARENT);
                } else {
                    for (int i = 0; i < listViewTransactions.getChildCount(); i++) {
                        if (position == i) {
                            listViewTransactions.getChildAt(i).setBackgroundColor(Color.BLUE);
                        } else {
                            listViewTransactions.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                        }
                    }
                    brojElemenata = listViewTransactions.getChildCount();
                    pozicija = position;
                    onItemClick.onItemClick(transaction, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                }
            }

        }

        ;
    };
    private AdapterView.OnItemClickListener listCursorItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Cursor cursor = (Cursor) parent.getItemAtPosition(position);
            if(cursor != null) {
                onItemClick.onItemClicked(true, cursor.getInt(cursor.getColumnIndex(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID)));
            }
        }
    };

    public void changeList(){
        transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
        listViewTransactions.setAdapter(transactionListAdapter);
        getPresenter().refreshTransactions();
        if(listViewTransactions.getChildCount() != brojElemenata) pozicija = -1;
        double iznos = accountListPresenter.getAccount().getBudget();
        editTextGlobalAmount.setText(Double.toString(iznos));
        filter(spinnerFilter.getSelectedItemPosition());
    }


    private void filter(int position) {
        if(position == 1 || position == 0) {
            transactionListPresenter.filterType(null);
            transactionListPresenter.filter(ca.getTime());
            transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
            listViewTransactions.setAdapter(transactionListAdapter);
            transactionListPresenter.refreshTransactions();
        }
        else if(position == 2) {
            transactionListPresenter.filterType(Transaction.TransactionType.INDIVIDUALPAYMENT);
            transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
            listViewTransactions.setAdapter(transactionListAdapter);
            transactionListPresenter.refreshTransactions();
        }
        else if(position == 3) {
            transactionListPresenter.filterType(Transaction.TransactionType.REGULARPAYMENT);
            transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
            listViewTransactions.setAdapter(transactionListAdapter);
            transactionListPresenter.refreshTransactions();
        }
        else if(position == 4) {
            transactionListPresenter.filterType(Transaction.TransactionType.PURCHASE);
            transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
            listViewTransactions.setAdapter(transactionListAdapter);
            transactionListPresenter.refreshTransactions();
        }
        else if(position == 5) {
            transactionListPresenter.filterType(Transaction.TransactionType.INDIVIDUALINCOME);
            transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
            listViewTransactions.setAdapter(transactionListAdapter);
            transactionListPresenter.refreshTransactions();
        }
        else {
            transactionListPresenter.filterType(Transaction.TransactionType.REGULARINCOME);
            transactionListAdapter = new TransactionListAdapter(getContext(), R.layout.list_element, new ArrayList<Transaction>());
            listViewTransactions.setAdapter(transactionListAdapter);
            transactionListPresenter.refreshTransactions();
        }

    }

    @Override
    public void setTransactions(ArrayList<Transaction> transactions) {
        transactionListAdapter.setTransactions(transactions);
    }

    @Override
    public void setAccount(Account account) {
        editTextGlobalAmount.setText(Double.toString(account.getBudget()));
        editTextLimit.setText(Double.toString(account.getTotalLimit()));
    }

    @Override
    public void notifyTransactionListDataSetChanged() {
        transactionListAdapter.notifyDataSetChanged();
    }

    public boolean isConnected() {
        return ((ConnectivityManager) getContext().getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }


    @Override
    public void setCursor(Cursor cursor) {
        listViewTransactions.setAdapter(transactionListCursorAdapter);
        listViewTransactions.setOnItemClickListener(listCursorItemClickListener);
        transactionListCursorAdapter.changeCursor(cursor);
    }
}
