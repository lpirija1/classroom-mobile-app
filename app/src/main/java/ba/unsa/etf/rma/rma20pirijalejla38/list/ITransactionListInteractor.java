package ba.unsa.etf.rma.rma20pirijalejla38.list;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public interface ITransactionListInteractor{
    ArrayList<Transaction> getTransactions();
    Cursor getTransactionCursor(Context context);
    void deleteDB(Context context);
}
