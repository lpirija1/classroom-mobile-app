package ba.unsa.etf.rma.rma20pirijalejla38.list;

import android.database.Cursor;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public interface ITransactionListView {
    void setTransactions(ArrayList<Transaction> transactions);
    void notifyTransactionListDataSetChanged();
    void setAccount(Account account);
    void setCursor(Cursor cursor);
}
