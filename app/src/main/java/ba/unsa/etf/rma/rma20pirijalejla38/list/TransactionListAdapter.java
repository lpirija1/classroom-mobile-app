package ba.unsa.etf.rma.rma20pirijalejla38.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public class TransactionListAdapter extends ArrayAdapter<Transaction> {
    private int resource;

    public TransactionListAdapter(@NonNull Context context, int _resource, ArrayList<Transaction> items) {
        super(context, _resource, items);
        resource = _resource;
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.clear();
        this.addAll(transactions);
    }

    public Transaction getTransaction(int position) {
        return getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater) getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout) convertView;
        }


        Transaction transaction = getItem(position);

        if (transaction != null) {
            TextView title = newView.findViewById(R.id.titleView);
            title.setText(transaction.getTitle());

            TextView amount = newView.findViewById(R.id.amountView);
            amount.setText(Double.toString(transaction.getAmount()));

            ImageView image = newView.findViewById(R.id.imageViewTransaction);
            if (transaction.getType().equals(Transaction.TransactionType.INDIVIDUALPAYMENT))
                image.setImageResource(R.drawable.individualpayment);
            else if (transaction.getType().equals(Transaction.TransactionType.REGULARPAYMENT))
                image.setImageResource(R.drawable.regularpayment);
            else if (transaction.getType().equals(Transaction.TransactionType.PURCHASE))
                image.setImageResource(R.drawable.purchase);
            else if (transaction.getType().equals(Transaction.TransactionType.INDIVIDUALINCOME)) {
                image.setImageResource(R.drawable.individualincome);
            } else {
                image.setImageResource(R.drawable.regularincome);
            }
        }

        return newView;
    }


}
