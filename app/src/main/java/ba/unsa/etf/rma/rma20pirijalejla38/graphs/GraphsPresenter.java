package ba.unsa.etf.rma.rma20pirijalejla38.graphs;

import android.app.Activity;
import android.content.Context;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;
import ba.unsa.etf.rma.rma20pirijalejla38.list.MainActivity;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionListInteractor;

public class GraphsPresenter implements IGraphsPresenter, GraphsInteractor.OnTransactionsSearchDone {
    private Context context;
    private IGraphView view;
    private Date datum;
    private ArrayList<Transaction> transactions;

    public GraphsPresenter(Context context, IGraphView view) {
        this.context = context;
        this.view = view;
        transactions = new ArrayList<>();
    }

    @Override
    public void getListTransactios(String query){
        new GraphsInteractor((GraphsInteractor.OnTransactionsSearchDone)
                this).execute(query);
    }

    @Override
    public void onDone(ArrayList<Transaction> results) {
        transactions.clear();
       transactions.addAll(results);
       view.setDate();
    }

    public void setDatum(Serializable datum){
        this.datum = (Date) datum;
    }

    public Date getDatum() {
        return this.datum;
    }

    public double dajPotrosnjuZaMjesec(int mjesec, int godina, Date datum) {
     double potrosnja = 0;
        ArrayList<Transaction> lista = transactions;
    for(Transaction t: lista){
         if(t.getType().equals(Transaction.TransactionType.INDIVIDUALPAYMENT) || t.getType().equals(Transaction.TransactionType.PURCHASE)) {
             SimpleDateFormat f1 = new SimpleDateFormat("MM");
             SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
             String s1 = f1.format(t.getDate());
             int mjesecTransakcije = Integer.parseInt(s1);
             String s2 = f2.format(t.getDate());
             int godinaTransakcije = Integer.parseInt(s2);
             if(mjesecTransakcije == mjesec && godinaTransakcije == godina) potrosnja-=t.getAmount();
     }
         else if(t.getType().equals(Transaction.TransactionType.REGULARPAYMENT)) {
             SimpleDateFormat f1 = new SimpleDateFormat("MM");
             SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
             String pocetakMjesec = f1.format(t.getDate());
             String pocetakGodina = f2.format(t.getDate());
             String krajMjesec = f1.format(t.getEndDate());
             String krajGodina = f2.format(t.getEndDate());
             int pocMjesec = Integer.parseInt(pocetakMjesec);
             int pocGodina = Integer.parseInt(pocetakGodina);
             int endMjesec = Integer.parseInt(krajMjesec);
             int endGodina = Integer.parseInt(krajGodina);

            if(pocMjesec == mjesec && pocGodina == godina) potrosnja -= t.getAmount();

            else if((mjesec>=pocMjesec && godina == pocGodina && godina == endGodina && mjesec<=endMjesec)
                    || (pocGodina<godina && endGodina==godina && mjesec<=endMjesec) || (pocGodina==godina && endGodina>godina && mjesec>=pocMjesec)){
                Calendar c = Calendar.getInstance();
                c.setTime(t.getDate());
                while(pocGodina<godina) {
                    int d = c.get(Calendar.DAY_OF_MONTH);
                    c.set(Calendar.DAY_OF_MONTH, d+t.getTransactionInterval());
                    pocGodina = Integer.parseInt(f2.format(c.getTime()));
                }

                if(endGodina==godina) {
                    while(pocMjesec<mjesec && pocMjesec<endMjesec){
                        int d = c.get(Calendar.DAY_OF_MONTH);
                        c.set(Calendar.DAY_OF_MONTH, d + t.getTransactionInterval());
                        Date pom = c.getTime();
                        pocMjesec = Integer.parseInt(f1.format(pom));
                    }

                    if(pocMjesec==mjesec) potrosnja-=t.getAmount();
                }
                else {
                    while(pocMjesec<mjesec){
                        int d = c.get(Calendar.DAY_OF_MONTH);
                        c.set(Calendar.DAY_OF_MONTH, d+t.getTransactionInterval());
                        Date pom = c.getTime();
                        pocMjesec = Integer.parseInt(f1.format(pom));
                    }
                    if(pocMjesec == mjesec) potrosnja-=t.getAmount();
                }
            }
         }
         }
     return potrosnja;
    }

    public double dajZaraduZaMjesec(int mjesec, int godina, Date datum) {
        double zarada = 0;
        ArrayList<Transaction> lista = transactions;
        for(Transaction t: lista){
            if(t.getType().equals(Transaction.TransactionType.INDIVIDUALINCOME)) {
                SimpleDateFormat f1 = new SimpleDateFormat("MM");
                SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
                String s1 = f1.format(t.getDate());
                int mjesecTransakcije = Integer.parseInt(s1);
                String s2 = f2.format(t.getDate());
                int godinaTransakcije = Integer.parseInt(s2);
                if(mjesecTransakcije == mjesec && godinaTransakcije == godina) zarada+=t.getAmount();
            }
            else if(t.getType().equals(Transaction.TransactionType.REGULARINCOME)) {
                SimpleDateFormat f1 = new SimpleDateFormat("MM");
                SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
                String pocetakMjesec = f1.format(t.getDate());
                String pocetakGodina = f2.format(t.getDate());
                String krajMjesec = f1.format(t.getEndDate());
                String krajGodina = f2.format(t.getEndDate());
                int pocMjesec = Integer.parseInt(pocetakMjesec);
                int pocGodina = Integer.parseInt(pocetakGodina);
                int endMjesec = Integer.parseInt(krajMjesec);
                int endGodina = Integer.parseInt(krajGodina);

                if(pocMjesec == mjesec && pocGodina == godina) zarada += t.getAmount();

                else if((mjesec>=pocMjesec && godina == pocGodina && godina == endGodina && mjesec<=endMjesec)
                        || (pocGodina<godina && endGodina==godina && mjesec<=endMjesec) || (pocGodina==godina && endGodina>godina && mjesec>=pocMjesec)){
                    Calendar c = Calendar.getInstance();
                    c.setTime(t.getDate());
                    while(pocGodina<godina) {
                        int d = c.get(Calendar.DAY_OF_MONTH);
                        c.set(Calendar.DAY_OF_MONTH, d+t.getTransactionInterval());
                        pocGodina = Integer.parseInt(f2.format(c.getTime()));
                    }

                    if(endGodina==godina) {
                        while(pocMjesec<mjesec && pocMjesec<endMjesec){
                            int d = c.get(Calendar.DAY_OF_MONTH);
                            c.set(Calendar.DAY_OF_MONTH, d + t.getTransactionInterval());
                            Date pom = c.getTime();
                            pocMjesec = Integer.parseInt(f1.format(pom));
                        }

                        if(pocMjesec==mjesec) zarada+=t.getAmount();
                    }
                    else {
                        while(pocMjesec<mjesec){
                            int d = c.get(Calendar.DAY_OF_MONTH);
                            c.set(Calendar.DAY_OF_MONTH, d+t.getTransactionInterval());
                            Date pom = c.getTime();
                            pocMjesec = Integer.parseInt(f1.format(pom));
                        }
                        if(pocMjesec == mjesec) zarada+=t.getAmount();
                    }
                }
            }
        }
        return zarada;
    }

    public double dajUkupanIznosZaMjesec(int mjesec, int godina, Date datum) {
        return dajZaraduZaMjesec(mjesec, godina, datum) + dajPotrosnjuZaMjesec(mjesec, godina,datum);
    }

    public double dajPotrosnjuZaDan(Date datum) {
        double potrosnja = 0;
        ArrayList<Transaction> lista = transactions;
        for(Transaction t: lista) {
            SimpleDateFormat f1 = new SimpleDateFormat("MM");
            SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
            SimpleDateFormat f3 = new SimpleDateFormat("dd");

            int dan =  Integer.parseInt(f3.format(datum));        //dan za koju racunam potrosnju
            int mjesec = Integer.parseInt(f1.format(datum));
            int godina = Integer.parseInt(f2.format(datum));

            if(t.getType().equals(Transaction.TransactionType.INDIVIDUALPAYMENT) || t.getType().equals(Transaction.TransactionType.PURCHASE)) {
                String s1 = f1.format(t.getDate());
                int mjesecTransakcije = Integer.parseInt(s1);
                String s2 = f2.format(t.getDate());
                int godinaTransakcije = Integer.parseInt(s2);
                String s3 = f3.format(t.getDate());
                int danTransakcije = Integer.parseInt(s3);
                if(danTransakcije == dan && mjesecTransakcije == mjesec && godinaTransakcije == godina) potrosnja-=t.getAmount();
            }
            else if(t.getType().equals(Transaction.TransactionType.REGULARPAYMENT)) {
                Calendar c = Calendar.getInstance();
                c.setTime(t.getDate());
                String s1 = f1.format(t.getDate());
                int mjesecTransakcije = Integer.parseInt(s1);
                int mjesecKrajaTransakcije = Integer.parseInt(f1.format(t.getEndDate()));
                String s2 = f2.format(t.getDate());
                int godinaTransakcije = Integer.parseInt(s2);
                int godinaKrajaTransakcije = Integer.parseInt(f2.format(t.getEndDate()));
                String s3 = f3.format(t.getDate());
                int danTransakcije = Integer.parseInt(s3);
                int danKrajaTransakcije = Integer.parseInt(f3.format(t.getEndDate()));

                //ako je pocetak transakcije prije tretnutne godine i zavrsava u trenutnoj godini
                if(danTransakcije == dan && mjesecTransakcije == mjesec && godinaTransakcije == godina) potrosnja-=t.getAmount();
                else if((t.getDate().before(datum) && t.getEndDate().after(datum)) || (danKrajaTransakcije == dan && mjesecKrajaTransakcije == mjesec && godinaKrajaTransakcije==godina)) {
                    while(godinaTransakcije < godina) {
                        int d = c.get(Calendar.DAY_OF_MONTH);
                        c.set(Calendar.DAY_OF_MONTH, d + t.getTransactionInterval());
                        godinaTransakcije = Integer.parseInt(f2.format(c.getTime()));
                    }

                    while(mjesecTransakcije < mjesec) {
                        int d = c.get(Calendar.DAY_OF_MONTH);
                        c.set(Calendar.DAY_OF_MONTH, d + t.getTransactionInterval());
                        Date datum2 = c.getTime();
                        mjesecTransakcije = Integer.parseInt(f1.format(datum2));
                    }
                    danTransakcije = Integer.parseInt(f3.format(c.getTime()));
                    if(godinaTransakcije == godina && mjesecTransakcije == mjesec && danTransakcije == dan) potrosnja -= t.getAmount();
                }
            }

        }
        return potrosnja;
    }


    public double dajZaraduZaDan (Date datum){
        double zarada = 0;
       ArrayList<Transaction> lista = transactions;
        for(Transaction t: lista) {

            SimpleDateFormat f1 = new SimpleDateFormat("MM");
            SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
            SimpleDateFormat f3 = new SimpleDateFormat("dd");

            int dan =  Integer.parseInt(f3.format(datum));
            int mjesec = Integer.parseInt(f1.format(datum));
            int godina = Integer.parseInt(f2.format(datum));

            if(t.getType().equals(Transaction.TransactionType.INDIVIDUALINCOME)) {
                String s1 = f1.format(t.getDate());
                int mjesecTransakcije = Integer.parseInt(s1);
                String s2 = f2.format(t.getDate());
                int godinaTransakcije = Integer.parseInt(s2);
                String s3 = f3.format(t.getDate());
                int danTransakcije = Integer.parseInt(s3);
                if(danTransakcije == dan && mjesecTransakcije == mjesec && godinaTransakcije == godina) zarada+=t.getAmount();
            }
            else if(t.getType().equals(Transaction.TransactionType.REGULARINCOME)) {
                Calendar c = Calendar.getInstance();
                c.setTime(t.getDate());
                String s1 = f1.format(t.getDate());
                int mjesecTransakcije = Integer.parseInt(s1);
                int mjesecKrajaTransakcije = Integer.parseInt(f1.format(t.getEndDate()));
                String s2 = f2.format(t.getDate());
                int godinaTransakcije = Integer.parseInt(s2);
                int godinaKrajaTransakcije = Integer.parseInt(f2.format(t.getEndDate()));
                String s3 = f3.format(t.getDate());
                int danTransakcije = Integer.parseInt(s3);
                int danKrajaTransakcije = Integer.parseInt(f3.format(t.getEndDate()));
                //ako je pocetak transakcije prije tretnutne godine i zavrsava u trenutnoj godini
                if(danTransakcije == dan && mjesecTransakcije == mjesec && godinaTransakcije == godina) zarada+=t.getAmount();
                else if((t.getDate().before(datum) && t.getEndDate().after(datum)) || (danKrajaTransakcije == dan && mjesecKrajaTransakcije == mjesec && godinaKrajaTransakcije==godina)) {
                    while(godinaTransakcije < godina) {
                        int d = c.get(Calendar.DAY_OF_MONTH);
                        c.set(Calendar.DAY_OF_MONTH, d + t.getTransactionInterval());
                        Date datum1 = c.getTime();
                        godinaTransakcije = Integer.parseInt(f2.format(datum1));
                    }

                    while(mjesecTransakcije < mjesec) {
                        int d = c.get(Calendar.DAY_OF_MONTH);
                        c.set(Calendar.DAY_OF_MONTH, d + t.getTransactionInterval());
                        Date datum2 = c.getTime();
                        mjesecTransakcije = Integer.parseInt(f1.format(datum2));
                    }
                    danTransakcije = Integer.parseInt(f3.format(c.getTime()));
                    if(godinaTransakcije == godina && mjesecTransakcije == mjesec && danTransakcije == dan) zarada += t.getAmount();
                }
            }

        }
        return zarada;
    }

    public double dajUkupnoStanjeZaDan(Date datum) {
        return dajZaraduZaDan(datum) + dajPotrosnjuZaDan(datum);
    }

    public double dajPotrosnjuZaSedmicu(int mjesec, int godina, int sedmica) {
        double potrosnja = 0;
        Calendar c = Calendar.getInstance();
        ArrayList<Transaction> lista = transactions;
        for(Transaction t: lista) {
            SimpleDateFormat f1 = new SimpleDateFormat("MM");
            SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
            SimpleDateFormat f3 = new SimpleDateFormat("dd");
            if(t.getType().equals(Transaction.TransactionType.INDIVIDUALPAYMENT) || t.getType().equals(Transaction.TransactionType.PURCHASE)) {
                int mjesecTransakcije = Integer.parseInt(f1.format(t.getDate()));
                int godinaTransakcije = Integer.parseInt(f2.format(t.getDate()));
                c.setTime(t.getDate());
                if(c.get(Calendar.WEEK_OF_MONTH) == sedmica && mjesecTransakcije == mjesec && godinaTransakcije == godina) potrosnja -= t.getAmount();
            }

            else if(t.getType().equals(Transaction.TransactionType.REGULARPAYMENT)) {
                Calendar c2 = Calendar.getInstance();
               // Calendar c1 = Calendar.getInstance();
                c2.setTime(t.getDate());
                String s1 = f1.format(t.getDate());
                int mjesecTransakcije = Integer.parseInt(s1);
                int mjesecKrajaTransakcije = Integer.parseInt(f1.format(t.getEndDate()));
                String s2 = f2.format(t.getDate());
                int godinaTransakcije = Integer.parseInt(s2);
                int godinaKrajaTransakcije = Integer.parseInt(f2.format(t.getEndDate()));
                String s3 = f3.format(t.getDate());
                //ako je pocetak transakcije prije tretnutne godine i zavrsava u trenutnoj godini
                if(c2.get(Calendar.WEEK_OF_MONTH)==sedmica && mjesecTransakcije == mjesec && godinaTransakcije == godina) potrosnja-=t.getAmount();
                else if((mjesec>=mjesecTransakcije && godina == godinaTransakcije && godina == godinaKrajaTransakcije && mjesec<=mjesecKrajaTransakcije)
                        || (godinaTransakcije<godina && godinaKrajaTransakcije==godina && mjesec<=mjesecKrajaTransakcije) || (godinaTransakcije==godina && godinaKrajaTransakcije>godina && mjesec>=mjesecTransakcije)){
                    c2.setTime(t.getDate());
                    while(godinaTransakcije<godina) {
                        int d = c2.get(Calendar.DAY_OF_MONTH);
                        c2.set(Calendar.DAY_OF_MONTH, d+t.getTransactionInterval());
                        godinaTransakcije = Integer.parseInt(f2.format(c2.getTime()));
                    }

                    if(godinaKrajaTransakcije==godina) {
                        while(mjesecTransakcije<mjesec && mjesecTransakcije<mjesecKrajaTransakcije){
                            int d = c2.get(Calendar.DAY_OF_MONTH);
                            c2.set(Calendar.DAY_OF_MONTH, d + t.getTransactionInterval());
                            Date pom = c2.getTime();
                            mjesecTransakcije = Integer.parseInt(f1.format(pom));
                        }

                        if(mjesecTransakcije==mjesec && c2.get(Calendar.WEEK_OF_MONTH)==sedmica) potrosnja-=t.getAmount();
                    }
                    else {
                        while(mjesecTransakcije<mjesec){
                            int d = c2.get(Calendar.DAY_OF_MONTH);
                            c2.set(Calendar.DAY_OF_MONTH, d+t.getTransactionInterval());
                            Date pom = c2.getTime();
                            mjesecTransakcije = Integer.parseInt(f1.format(pom));
                        }
                        if(mjesecTransakcije == mjesec && c2.get(Calendar.WEEK_OF_MONTH) == sedmica) potrosnja-=t.getAmount();
                    }
                }
            }
        }
        return potrosnja;
    }
    public double dajZaraduZaSedmicu(int mjesec, int godina, int sedmica) {
        double zarada = 0;
        Calendar c = Calendar.getInstance();
        ArrayList<Transaction> lista = transactions;
        for(Transaction t: lista) {
            SimpleDateFormat f1 = new SimpleDateFormat("MM");
            SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
            SimpleDateFormat f3 = new SimpleDateFormat("dd");
            if(t.getType().equals(Transaction.TransactionType.INDIVIDUALINCOME)) {
                int mjesecTransakcije = Integer.parseInt(f1.format(t.getDate()));
                int godinaTransakcije = Integer.parseInt(f2.format(t.getDate()));
                c.setTime(t.getDate());
                if(c.get(Calendar.WEEK_OF_MONTH) == sedmica && mjesecTransakcije == mjesec && godinaTransakcije == godina) zarada += t.getAmount();
            }

            else if(t.getType().equals(Transaction.TransactionType.REGULARINCOME)) {
                    Calendar c2 = Calendar.getInstance();
                    // Calendar c1 = Calendar.getInstance();
                    c2.setTime(t.getDate());
                    String s1 = f1.format(t.getDate());
                    int mjesecTransakcije = Integer.parseInt(s1);
                    int mjesecKrajaTransakcije = Integer.parseInt(f1.format(t.getEndDate()));
                    String s2 = f2.format(t.getDate());
                    int godinaTransakcije = Integer.parseInt(s2);
                    int godinaKrajaTransakcije = Integer.parseInt(f2.format(t.getEndDate()));
                    String s3 = f3.format(t.getDate());
                    //ako je pocetak transakcije prije tretnutne godine i zavrsava u trenutnoj godini
                    if(c2.get(Calendar.WEEK_OF_MONTH)==sedmica && mjesecTransakcije == mjesec && godinaTransakcije == godina) zarada+=t.getAmount();
                    else if((mjesec>=mjesecTransakcije && godina == godinaTransakcije && godina == godinaKrajaTransakcije && mjesec<=mjesecKrajaTransakcije)
                            || (godinaTransakcije<godina && godinaKrajaTransakcije==godina && mjesec<=mjesecKrajaTransakcije) || (godinaTransakcije==godina && godinaKrajaTransakcije>godina && mjesec>=mjesecTransakcije)){
                        c2.setTime(t.getDate());
                        while(godinaTransakcije<godina) {
                            int d = c2.get(Calendar.DAY_OF_MONTH);
                            c2.set(Calendar.DAY_OF_MONTH, d+t.getTransactionInterval());
                            godinaTransakcije = Integer.parseInt(f2.format(c2.getTime()));
                        }

                        if(godinaKrajaTransakcije==godina) {
                            while(mjesecTransakcije<mjesec && mjesecTransakcije<mjesecKrajaTransakcije){
                                int d=c2.get(Calendar.DAY_OF_MONTH);
                                c2.set(Calendar.DAY_OF_MONTH, d + t.getTransactionInterval());
                                Date pom = c2.getTime();
                                mjesecTransakcije = Integer.parseInt(f1.format(pom));
                            }

                            if(mjesecTransakcije==mjesec && c2.get(Calendar.WEEK_OF_MONTH)==sedmica) zarada+=t.getAmount();
                        }
                        else {
                            while(mjesecTransakcije<mjesec){
                                int d = c2.get(Calendar.DAY_OF_MONTH);
                                c2.set(Calendar.DAY_OF_MONTH, d+t.getTransactionInterval());
                                Date pom = c2.getTime();
                                mjesecTransakcije = Integer.parseInt(f1.format(pom));
                            }
                            if(mjesecTransakcije == mjesec && c2.get(Calendar.WEEK_OF_MONTH)== sedmica) zarada+=t.getAmount();
                        }
                    }
                }
        }
        return zarada;
    }

    public double dajUkupanIznosZaSedmicu(int mjesec, int godina, int sedmica) {
        return dajZaraduZaSedmicu(mjesec, godina, sedmica) + dajPotrosnjuZaSedmicu(mjesec, godina, sedmica);
    }
}
