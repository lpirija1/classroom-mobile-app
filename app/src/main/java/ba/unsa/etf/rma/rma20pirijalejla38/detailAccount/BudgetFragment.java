package ba.unsa.etf.rma.rma20pirijalejla38.detailAccount;

import android.app.Activity;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;
import ba.unsa.etf.rma.rma20pirijalejla38.list.ITransactionListView;
import ba.unsa.etf.rma.rma20pirijalejla38.list.OnSwipeTouchListener;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class BudgetFragment extends Fragment implements IAccountDetailView{
    private EditText editTextMonthLimit;
    private EditText editTextTotalLimit;
    private TextView textViewPrikazBudzeta;
    private Button buttonSaveBudget;
    private IAccountListPresenter accountListPresenter;
    private OnClicButtonsInBudgetFragment onClick;
    private TextView textViewAction;


    public interface OnClicButtonsInBudgetFragment {
        void onClickButtonRightFragmentForBudget();
        void onClickButtonLeftFragmentForBudget(Date datum);
    }

    public IAccountListPresenter getAccountPresenter() {
        if(accountListPresenter == null ){
            accountListPresenter = new AccountListPresenter(getContext(), this);
        }
        return accountListPresenter;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_budget, container, false);
        textViewPrikazBudzeta = view.findViewById(R.id.textViewPrikazBudzeta);
        editTextTotalLimit = view.findViewById(R.id.editTextTotalLimit);
        editTextMonthLimit = view.findViewById(R.id.editTextMonthLimit);
        buttonSaveBudget = view.findViewById(R.id.buttonSaveBudget);
        textViewAction = view.findViewById(R.id.textViewAction);
        onClick = (OnClicButtonsInBudgetFragment) getActivity();

        if(isConnected()) {
            getAccountPresenter();



            view.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onSwipeRight() {
                    onClick.onClickButtonRightFragmentForBudget();
                }

                @Override
                public void onSwipeLeft() {
                    onClick.onClickButtonLeftFragmentForBudget((Date) getArguments().getSerializable("datum"));
                }
            });


            accountListPresenter.searchAccount("account");


            buttonSaveBudget.setOnClickListener(new View.OnClickListener() {
                boolean validanIznosTotalLimit = true;
                boolean validanIznosMonthLimit = true;

                @Override
                public void onClick(View v) {
                    String iznosTotalLimit = editTextTotalLimit.getText().toString();
                    if (iznosTotalLimit.equals("")) {
                        validanIznosTotalLimit = false;
                        editTextTotalLimit.setBackgroundColor(Color.RED);
                    } else {
                        double provjera = -1.0;
                        try {
                            provjera = Double.parseDouble(iznosTotalLimit);
                            if (provjera < 0) {
                                validanIznosTotalLimit = false;
                                editTextTotalLimit.setBackgroundColor(Color.RED);
                            } else {
                                editTextTotalLimit.setBackgroundColor(Color.GREEN);
                            }
                        } catch (NumberFormatException e) {
                            validanIznosTotalLimit = false;
                            editTextTotalLimit.setBackgroundColor(Color.RED);
                        }

                    }


                    String iznosMonthLimit = editTextMonthLimit.getText().toString();
                    if (iznosMonthLimit.equals("")) {
                        validanIznosMonthLimit = false;
                        editTextMonthLimit.setBackgroundColor(Color.RED);
                    } else {
                        double provjera = -1.0;
                        try {
                            provjera = Double.parseDouble(iznosTotalLimit);
                            if (provjera < 0) {
                                validanIznosMonthLimit = false;
                                editTextMonthLimit.setBackgroundColor(Color.RED);
                            } else {
                                editTextMonthLimit.setBackgroundColor(Color.GREEN);
                            }
                        } catch (NumberFormatException e) {
                            validanIznosMonthLimit = false;
                            editTextMonthLimit.setBackgroundColor(Color.RED);
                        }

                    }

                    if (validanIznosTotalLimit && validanIznosMonthLimit) {
                        String[] values = {"account", textViewPrikazBudzeta.getText().toString(), editTextTotalLimit.getText().toString(), editTextMonthLimit.getText().toString()};
                        accountListPresenter.editAccount(values);
                    }
                }
            });
        }
        else {
            textViewAction.setText("Offline izmjena");
            accountListPresenter = new AccountListPresenter(getContext(), this, "");
            accountListPresenter.dajAccountIzBaze(getContext());
            textViewPrikazBudzeta.setText(Double.toString(accountListPresenter.getAccount().getBudget()));
            editTextTotalLimit.setText(Double.toString(accountListPresenter.getAccount().getTotalLimit()));
            editTextMonthLimit.setText(Double.toString(accountListPresenter.getAccount().getMonthLimit()));

            view.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onSwipeRight() {
                    onClick.onClickButtonRightFragmentForBudget();
                }

                @Override
                public void onSwipeLeft() {
                    onClick.onClickButtonLeftFragmentForBudget((Date) getArguments().getSerializable("datum"));

                }
            });
            buttonSaveBudget.setOnClickListener(new View.OnClickListener() {
                boolean validanIznosTotalLimit = true;
                boolean validanIznosMonthLimit = true;

                @Override
                public void onClick(View v) {
                    String iznosTotalLimit = editTextTotalLimit.getText().toString();
                    if (iznosTotalLimit.equals("")) {
                        validanIznosTotalLimit = false;
                        editTextTotalLimit.setBackgroundColor(Color.RED);
                    } else {
                        double provjera = -1.0;
                        try {
                            provjera = Double.parseDouble(iznosTotalLimit);
                            if (provjera < 0) {
                                validanIznosTotalLimit = false;
                                editTextTotalLimit.setBackgroundColor(Color.RED);
                            } else {
                                editTextTotalLimit.setBackgroundColor(Color.GREEN);
                            }
                        } catch (NumberFormatException e) {
                            validanIznosTotalLimit = false;
                            editTextTotalLimit.setBackgroundColor(Color.RED);
                        }

                    }


                    String iznosMonthLimit = editTextMonthLimit.getText().toString();
                    if (iznosMonthLimit.equals("")) {
                        validanIznosMonthLimit = false;
                        editTextMonthLimit.setBackgroundColor(Color.RED);
                    } else {
                        double provjera = -1.0;
                        try {
                            provjera = Double.parseDouble(iznosTotalLimit);
                            if (provjera < 0) {
                                validanIznosMonthLimit = false;
                                editTextMonthLimit.setBackgroundColor(Color.RED);
                            } else {
                                editTextMonthLimit.setBackgroundColor(Color.GREEN);
                            }
                        } catch (NumberFormatException e) {
                            validanIznosMonthLimit = false;
                            editTextMonthLimit.setBackgroundColor(Color.RED);
                        }

                    }

                    if (validanIznosTotalLimit && validanIznosMonthLimit) {
                        System.out.println("validnost");
                        accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(),accountListPresenter.getAccount().getBudget(),accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                    }
                }
            });



        }


        return view;
    }

    @Override
    public void setAccount(Account account) {
        textViewPrikazBudzeta.setText(Double.toString(account.getBudget()));
        editTextTotalLimit.setText(Double.toString(account.getTotalLimit()));
        editTextMonthLimit.setText(Double.toString(account.getMonthLimit()));
    }


    public boolean isConnected() {
        return ((ConnectivityManager) getContext().getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

}
