package ba.unsa.etf.rma.rma20pirijalejla38.data;



import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

import ba.unsa.etf.rma.rma20pirijalejla38.graphs.IGraphView;

public class Transaction implements Parcelable {

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            try {
                return new Transaction(in);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        dest.writeInt(id);
        dest.writeString(format.format(date));
        dest.writeString(Double.toString(amount));
        dest.writeString(title);
        dest.writeString(type.toString());
        dest.writeString(itemDescription);
        dest.writeString(Integer.toString(transactionInterval));
        dest.writeString(format.format(endDate));
        dest.writeString(akcija);
    }

    public enum TransactionType {
        INDIVIDUALPAYMENT, REGULARPAYMENT, PURCHASE, INDIVIDUALINCOME, REGULARINCOME
    };
    private Date date;
    private double amount;
    private String title;
    private TransactionType type;
    private String itemDescription;
    private Integer transactionInterval;
    private Date endDate;
    private Integer id;
    private String akcija = "";

    protected Transaction(Parcel parcel) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        this.id = Integer.parseInt(parcel.readString());
        this.date = format.parse(parcel.readString());
        this.amount = Double.parseDouble(parcel.readString());
        this.title = parcel.readString();
        String tip = parcel.readString();
        if(tip.equalsIgnoreCase("INDIVIDUALPAYMENT")) this.type = TransactionType.INDIVIDUALPAYMENT;
        else if(tip.equalsIgnoreCase("REGULARPAYMENT")) this.type = TransactionType.REGULARPAYMENT;
        else if(tip.equalsIgnoreCase("PURCHASE")) this.type = TransactionType.PURCHASE;
        else if(tip.equalsIgnoreCase("INDIVIDUALINCOME")) this.type = TransactionType.INDIVIDUALINCOME;
        else this.type = TransactionType.REGULARINCOME;
        this.itemDescription = parcel.readString();
        this.transactionInterval = Integer.parseInt(parcel.readString());
        this.endDate = format.parse(parcel.readString());
        this.akcija = parcel.readString();
    }
    public Transaction(int id, Date date, double amount, String title, TransactionType type, String itemDescription, String transactionInterval, String endDate){
               this.id = id;
               this.date = date;
               this.amount = amount;
               this.title = title;
               this.type = type;
               this.itemDescription = itemDescription;
               String pom = "null";
               if(!pom.equals(transactionInterval)){
                   this.transactionInterval = Integer.parseInt(transactionInterval);
               }
               else {
                   this.transactionInterval = null;
               }

               setEndDate(pretvoriDatum(endDate));
    }
    public Transaction(int id, Date date, double amount, String title, TransactionType type, String itemDescription, String transactionInterval, String endDate, String akcija){
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        String pom = "null";
        if(!pom.equals(transactionInterval)){
            this.transactionInterval = Integer.parseInt(transactionInterval);
        }
        else {
            this.transactionInterval = null;
        }

        setEndDate(pretvoriDatum(endDate));
        this.akcija = akcija;
    }

    public Transaction() {
    }




    public Transaction(Date date, double amount, String title, TransactionType type, String itemDescription, String transactionInterval, String endDate){
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        if(transactionInterval != null && !transactionInterval.equalsIgnoreCase("null")){
            this.transactionInterval = Integer.parseInt(transactionInterval);
        }
        else this.transactionInterval = null;

        setEndDate(pretvoriDatum(endDate));
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
            this.title = title;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
            this.itemDescription = itemDescription;

    }

    public Integer getTransactionInterval() {
        return this.transactionInterval;
    }

    public void setTransactionInterval(Integer transactionInterval) {
            this.transactionInterval = transactionInterval;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;

    }

    private Date pretvoriDatum(String date) {
        Date dateVrati = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            try {
                if (!date.equals("") && !date.equalsIgnoreCase("null")) {
                    dateVrati = format.parse(date);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return dateVrati;
        }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAkcija() {
        return akcija;
    }

    public void setAkcija(String akcija) {
        this.akcija = akcija;
    }
}
