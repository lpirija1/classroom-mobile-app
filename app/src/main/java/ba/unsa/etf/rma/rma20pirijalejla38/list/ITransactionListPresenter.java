package ba.unsa.etf.rma.rma20pirijalejla38.list;

import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public interface ITransactionListPresenter {
    void refreshTransactions();
    void sort(String type);
    void filter(Date date);
    void filterType(Transaction.TransactionType type);
    void getListTransactios(String query);
    void dodajTransakcijuUListu(Transaction transaction);
    void getTransactionsCursor();
    void izmijeniTransakciju(Transaction transaction);
}
