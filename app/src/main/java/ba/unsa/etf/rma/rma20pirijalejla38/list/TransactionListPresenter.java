package ba.unsa.etf.rma.rma20pirijalejla38.list;

import android.content.Context;
import android.database.Cursor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public class TransactionListPresenter implements ITransactionListPresenter, TransactionListInteractor.OnTransactionsSearchDone {

    private ITransactionListView view;
    private Context context;
    private ArrayList<Transaction> listTransactions; //pomocna lista
    private ArrayList<Transaction> kopija;
    private Calendar c;
    private TransactionListInteractor transactionListInteractor;


    public TransactionListPresenter(ITransactionListView view, Context context) {
        this.listTransactions = new ArrayList<>();
        this.kopija = new ArrayList<>();
        this.view = view;
        this.context = context;
    }

    public TransactionListPresenter(ITransactionListView view, Context context, String razlika) {
        this.listTransactions = new ArrayList<>();
        this.kopija = new ArrayList<>();
        this.view = view;
        this.context = context;
        this.transactionListInteractor = new TransactionListInteractor();
        if(!TransactionListInteractor.transactions.isEmpty()) {
            kopija.addAll(TransactionListInteractor.transactions);
            listTransactions.addAll(TransactionListInteractor.transactions);
        }
    }

    @Override
    public void getListTransactios(String query){
        new TransactionListInteractor((TransactionListInteractor.OnTransactionsSearchDone)
                this).execute(query);
    }

    @Override
    public void onDone(ArrayList<Transaction> results) {
        kopija.clear();
        listTransactions.clear();
        kopija.addAll(results);
        listTransactions.addAll(results);
        filter(c.getTime());
        refreshTransactions();
    }

    public void sort(String type) {
        if(type.equalsIgnoreCase("Price - Ascending")) {
            Collections.sort(listTransactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    if(o1.getAmount()<o2.getAmount()) return -1;
                    else if(o1.getAmount()>o2.getAmount()) return 1;
                    return 0;
                }});

        }
        else if(type.equalsIgnoreCase("Price - Descending")) {
            Collections.sort(listTransactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    if(o1.getAmount()>o2.getAmount()) return -1;
                    else if(o1.getAmount()<o2.getAmount()) return 1;
                    return 0;
                }});

        }
        else if(type.equalsIgnoreCase("Title - Ascending")){
            Collections.sort(listTransactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getTitle().compareTo(o2.getTitle());
                }
            });
        }
        else if(type.equalsIgnoreCase("Title - Descending")) {
            Collections.sort(listTransactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getTitle().compareTo(o2.getTitle())*(-1);
                }
            });
        }
        else if(type.equalsIgnoreCase("Date - Ascending")) {
            Collections.sort(listTransactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getDate().compareTo(o2.getDate());
                }
            });
        }
        else if(type.equalsIgnoreCase("Date - Descending")){
            Collections.sort(listTransactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getDate().compareTo(o2.getDate())*(-1);
                }
            });
        }
        refreshTransactions();
    }
    public void filter(Date date) {
        listTransactions.clear();
        listTransactions.addAll(kopija);
        for (Transaction t : kopija) {
            SimpleDateFormat f1 = new SimpleDateFormat("MM");
            String s = f1.format(date);
            int currentMonth = Integer.parseInt(s);
            SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
            String s1 = f2.format(date);
            int currentYear = Integer.parseInt(s1);


            if (Transaction.TransactionType.INDIVIDUALPAYMENT.equals(t.getType()) || Transaction.TransactionType.INDIVIDUALINCOME.equals(t.getType()) || Transaction.TransactionType.PURCHASE.equals(t.getType())) {
                if ((Integer.parseInt(f1.format(t.getDate())) != currentMonth || Integer.parseInt(f2.format(t.getDate())) != currentYear)) {
                    listTransactions.remove(t);
                }
            } else {
                if ((currentMonth < Integer.parseInt(f1.format(t.getDate())) && currentYear == Integer.parseInt(f2.format(t.getDate()))) || currentYear < Integer.parseInt(f2.format(t.getDate()))
                        || (currentMonth > Integer.parseInt(f1.format(t.getEndDate())) && currentYear == Integer.parseInt(f2.format(t.getEndDate()))) || currentYear > Integer.parseInt(f2.format(t.getEndDate()))) {
                    listTransactions.remove(t);
                }
            }
        }
        }

    public void filterType(Transaction.TransactionType type) {

        if(type == null) {
            listTransactions.clear();
            listTransactions.addAll(kopija);
        } else {
            for (int i = 0; i < listTransactions.size(); i++) {
                if (!listTransactions.get(i).getType().equals(type)) {
                    listTransactions.remove(listTransactions.get(i));
                    i--;
                }
            }
        }
    }

    @Override
    public void refreshTransactions(){
        view.setTransactions(listTransactions);
        view.notifyTransactionListDataSetChanged();
    }

    public void setTime(Calendar ca) {
        c = ca;
    }

@Override
    public void dodajTransakcijuUListu(Transaction transaction) {
        kopija.add(transaction);
        listTransactions.add(transaction);
}

@Override
public void izmijeniTransakciju(Transaction transaction) {
        int j = 0;
        for(Transaction t: kopija){
            if(t.getId().equals(transaction.getId())){
                kopija.get(j).setTitle(transaction.getTitle());
                kopija.get(j).setAkcija(transaction.getAkcija());
                kopija.get(j).setTransactionInterval(transaction.getTransactionInterval());
                kopija.get(j).setItemDescription(transaction.getItemDescription());
                kopija.get(j).setEndDate(transaction.getEndDate());
                kopija.get(j).setType(transaction.getType());
                kopija.get(j).setDate(transaction.getDate());
                kopija.get(j).setAmount(transaction.getAmount());
            }
            j++;
        }
}

    @Override
    public void getTransactionsCursor() {
        Cursor cursor  = transactionListInteractor.getTransactionCursor(context.getApplicationContext());
        if(cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) {
                int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
                int internalId = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
                int titlePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
                int datePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
                int amountPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
                int typePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE);
                int itemDescriptionPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ITEM_DESCRIPTION);
                int endDatePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ENDDATE);
                int intervalPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL);
                int akcijaPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AKCIJA);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                Transaction.TransactionType tip;
                if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("INDIVIDUALPAYMENT"))
                    tip = Transaction.TransactionType.INDIVIDUALPAYMENT;
                else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("REGULARPAYMENT"))
                    tip = Transaction.TransactionType.REGULARPAYMENT;
                else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("PURCHASE"))
                    tip = Transaction.TransactionType.PURCHASE;
                else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("INDIVIDUALINCOME")) {
                    tip = Transaction.TransactionType.INDIVIDUALINCOME;
                } else {
                    tip = Transaction.TransactionType.REGULARINCOME;
                }

                try {
                    listTransactions.add(new Transaction(cursor.getInt(idPos), format.parse(cursor.getString(datePos)), cursor.getDouble(amountPos), cursor.getString(titlePos), tip, cursor.getString(itemDescriptionPos), cursor.getString(intervalPos), cursor.getString(endDatePos), cursor.getString(akcijaPos)));
                    kopija.add(new Transaction(cursor.getInt(idPos), format.parse(cursor.getString(datePos)), cursor.getDouble(amountPos), cursor.getString(titlePos), tip, cursor.getString(itemDescriptionPos), cursor.getString(intervalPos), cursor.getString(endDatePos), cursor.getString(akcijaPos)));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cursor.moveToNext();
            }
            }
            view.setCursor(cursor);
    }

}
