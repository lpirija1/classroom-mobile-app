package ba.unsa.etf.rma.rma20pirijalejla38.detailAccount;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;

public interface IAccountDetailView {
    public void setAccount(Account account);
}
