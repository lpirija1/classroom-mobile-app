package ba.unsa.etf.rma.rma20pirijalejla38.detailAccount;

import android.content.Context;
import android.os.Parcelable;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;

public interface IAccountListPresenter {
    void setAccount (Parcelable a);
    Account getAccount();
    void searchAccount(String ... query);
    void editAccount(String ... query);
    void dajAccountZbogBrisanja(String... query);
    void dohvatiAccount(String ... query);
    void updateAccount(Context context, int id, double iznos, double monthLImit, double totalLimit);
    void dajAccountIzBaze(Context context);
}
