package ba.unsa.etf.rma.rma20pirijalejla38.detail;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public interface IDetailView {
    public void pozoviOnClick();
    public void postaviPodatke(Transaction transaction);
    public void editujTransakciju(Account account);
    void setAccount();
    void dodajTransakciju(Account account);
    void vratiSeNaView();
    void obrisiTransakciju();
    void vratiSeNazad();
    void refreshView();
}
