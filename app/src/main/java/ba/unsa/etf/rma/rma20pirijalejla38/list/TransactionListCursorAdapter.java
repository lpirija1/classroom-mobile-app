package ba.unsa.etf.rma.rma20pirijalejla38.list;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public class TransactionListCursorAdapter extends ResourceCursorAdapter {
    private TextView title;
    private TextView amount;
    private ImageView image;

    public TransactionListCursorAdapter(Context context, int layout, Cursor c, boolean autoRequery) {
        super(context, layout, c, autoRequery);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

            title = view.findViewById(R.id.titleView);
            title.setText(cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE)));

            amount = view.findViewById(R.id.amountView);
            amount.setText(Double.toString(cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT))));

            image = view.findViewById(R.id.imageViewTransaction);
            if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("INDIVIDUALPAYMENT"))
                image.setImageResource(R.drawable.individualpayment);
            else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("REGULARPAYMENT"))
                image.setImageResource(R.drawable.regularpayment);
            else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("PURCHASE"))
                image.setImageResource(R.drawable.purchase);
            else if (cursor.getString(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE)).equalsIgnoreCase("INDIVIDUALINCOME")) {
                image.setImageResource(R.drawable.individualincome);
            } else {
                image.setImageResource(R.drawable.regularincome);
            }
        }
}
