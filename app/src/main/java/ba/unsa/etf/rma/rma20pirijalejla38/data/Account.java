package ba.unsa.etf.rma.rma20pirijalejla38.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Account  implements Parcelable {
    private double budget;
    private double totalLimit;
    private double monthLimit;
    private int id;
    private String acHash;
    private String email;

    public Account() {
    }

    public Account(double budget, double totalLimit, double monthLimit, int id, String acHash, String email) {
        this.budget = budget;
        this.totalLimit = totalLimit;
        this.monthLimit = monthLimit;
        this.id = id;
        this.acHash = acHash;
        this.email = email;
    }

    protected Account(Parcel in) {
        budget = in.readDouble();
        totalLimit = in.readDouble();
        monthLimit = in.readDouble();
        id = in.readInt();
        acHash = in.readString();
        email = in.readString();
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public double getTotalLimit() {
        return totalLimit;
    }

    public void setTotalLimit(double totalLimit) {
        this.totalLimit = totalLimit;
    }

    public double getMonthLimit() {
        return monthLimit;
    }

    public void setMonthLimit(double monthLimit) {
        this.monthLimit = monthLimit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
       dest.writeDouble(budget);
       dest.writeDouble(totalLimit);
       dest.writeDouble(monthLimit);
       dest.writeInt(id);
       dest.writeString(acHash);
       dest.writeString(email);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAcHash() {
        return acHash;
    }

    public void setAcHash(String acHash) {
        this.acHash = acHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
