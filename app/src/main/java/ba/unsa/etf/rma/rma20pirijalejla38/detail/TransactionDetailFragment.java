package ba.unsa.etf.rma.rma20pirijalejla38.detail;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.IAccountDetailView;
import ba.unsa.etf.rma.rma20pirijalejla38.list.SpinnerAdapter;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.AccountListPresenter;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.IAccountListPresenter;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionContentProvider;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class TransactionDetailFragment extends Fragment implements IDetailView {
    private EditText editTextTransactionTitle;
    private EditText editTextDateNovi;
    private EditText editTextAmount2;
    private EditText editTextItemDescription;
    private EditText editTextTransactionInterval;
    private EditText editTextEndDate;
    private Button buttonSave;
    private Button buttonDelete;
    private Spinner spinnerType;
    private TextView textViewAkcija;

    private TransactionDetailPresenter transactionDetailPresenter;
    private IAccountListPresenter accountListPresenter;
    private OnClickButton onClick;
    private int idT;

    public IAccountListPresenter getAccountPresenter() {
        if(accountListPresenter == null ){
            accountListPresenter = new AccountListPresenter(getContext(), this);
        }
        return accountListPresenter;
    }
    public ITransactionDetailPresenter getPresenter() {
        if (transactionDetailPresenter == null) {
            transactionDetailPresenter = new TransactionDetailPresenter(getContext(), this);
        }
        return transactionDetailPresenter;
    }
    public interface OnClickButton {
        void onClickButtons();
        void onClickButtons(Transaction transaction);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        onClick = (OnClickButton) getActivity();
        Integer[] integers = new Integer[]{R.drawable.bijela, R.drawable.individualpayment, R.drawable.regularpayment, R.drawable.purchase, R.drawable.individualincome, R.drawable.regularincome};
        String[] strings = new String[]{"TYPES", "INDIVIDUALPAYMENT", "REGULARPAYMENT", "PURCHASE", "INDIVIDUALINCOME", "REGULARINCOME"};
        final SpinnerAdapter adapter = new
                SpinnerAdapter(getContext(), integers, strings);
        spinnerType = view.findViewById(R.id.spinnerType);
        spinnerType.setAdapter(adapter);

        editTextTransactionTitle = view.findViewById(R.id.editTextTransactionTitle);
        editTextDateNovi = view.findViewById(R.id.editTextDateNovi);
        editTextAmount2 = view.findViewById(R.id.editTextAmount2);
        editTextItemDescription = view.findViewById(R.id.editTextItemDescription);
        editTextTransactionInterval = view.findViewById(R.id.editTextTransactionInterval);
        editTextEndDate = view.findViewById(R.id.editTextEndDate);
        textViewAkcija = view.findViewById(R.id.textViewAkcija);
        Button buttonSave1 = view.findViewById(R.id.buttonSave);
        buttonDelete = view.findViewById(R.id.buttonDelete);
        getPresenter();

        if (isConnected()) {
            getAccountPresenter();
            if (getArguments() != null && getArguments().containsKey("transaction") && getArguments().getSerializable("transaction") != null) {
                final int idTransakcije = (int) getArguments().getSerializable("transaction");
                String[] niz = {"transactions", "sveTransakcije", Integer.toString(idTransakcije)};
                getPresenter().getListTransactios(niz);

                Button buttonDelete1 = view.findViewById(R.id.buttonDelete);


                final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                buttonSave1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean validanNaziv = true;
                        boolean validanDatum = true;
                        boolean validanIznos = true;
                        boolean validanItemDescription = true;
                        boolean validanInterval = true;
                        boolean validanKraj = true;
                        boolean validanTip = true;

                        final Transaction.TransactionType newType;
                        int tip = spinnerType.getSelectedItemPosition();
                        if (tip == 1) newType = Transaction.TransactionType.INDIVIDUALPAYMENT;
                        else if (tip == 2) newType = Transaction.TransactionType.REGULARPAYMENT;
                        else if (tip == 3) newType = Transaction.TransactionType.PURCHASE;
                        else if (tip == 4) newType = Transaction.TransactionType.INDIVIDUALINCOME;
                        else if (tip == 5) newType = Transaction.TransactionType.REGULARINCOME;
                        else newType = null;

                        if (spinnerType.getSelectedItemPosition() == 0) {
                            spinnerType.setBackgroundColor(Color.RED);
                            validanTip = false;
                        } else {
                            spinnerType.setBackgroundColor(Color.GREEN);
                        }

                        String title = editTextTransactionTitle.getText().toString();
                        if (title.equals("") || title.length() < 3 || title.length() > 15) {
                            validanNaziv = false;
                            editTextTransactionTitle.setBackgroundColor(Color.RED);
                        } else editTextTransactionTitle.setBackgroundColor(Color.GREEN);


                        String iznos = editTextAmount2.getText().toString();
                        if (iznos.equals("")) {
                            validanIznos = false;
                            editTextAmount2.setBackgroundColor(Color.RED);
                        } else {
                            double provjera = -1.0;
                            try {
                                provjera = Double.parseDouble(iznos);
                                if (provjera < 0) {
                                    validanIznos = false;
                                    editTextAmount2.setBackgroundColor(Color.RED);
                                } else {
                                    editTextAmount2.setBackgroundColor(Color.GREEN);
                                }
                            } catch (NumberFormatException e) {
                                validanIznos = false;
                                editTextAmount2.setBackgroundColor(Color.RED);
                            }

                        }
                        String datum = editTextDateNovi.getText().toString();
                        Date newDate = null;
                        if (datum.equals("") || datum.length() != 10 || datum.charAt(0) < '0' || datum.charAt(0) > '9' || datum.charAt(1) < '0' || datum.charAt(1) > '9' || datum.charAt(2) != '-' || datum.charAt(3) < '0' || datum.charAt(3) > '9' ||
                                datum.charAt(4) < '0' || datum.charAt(4) > '9' || datum.charAt(5) != '-' || datum.charAt(6) < '0' || datum.charAt(6) > '9' || datum.charAt(7) < '0' || datum.charAt(7) > '9' || datum.charAt(8) < '0' || datum.charAt(8) > '9' || datum.charAt(9) < '0' || datum.charAt(9) > '9') {
                            validanDatum = false;
                            editTextDateNovi.setBackgroundColor(Color.RED);
                        } else {
                            String[] niz = datum.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if (godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if (godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec - 1]) {
                                validanDatum = false;
                                editTextDateNovi.setBackgroundColor(Color.RED);
                            } else {
                                editTextDateNovi.setBackgroundColor(Color.GREEN);
                            }
                            try {
                                newDate = (Date) format.parse(editTextDateNovi.getText().toString());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        String itemDescription = editTextItemDescription.getText().toString();
                        if ((tip == 4 || tip == 5) && !itemDescription.equals("")) {
                            validanItemDescription = false;
                            editTextItemDescription.setBackgroundColor(Color.RED);
                        } else if ((tip == 4 || tip == 5) && itemDescription.equals("")) {
                            editTextItemDescription.setBackgroundColor(Color.GREEN);
                        } else if ((tip == 1 || tip == 2 || tip == 3) && itemDescription.equals("")) {
                            validanItemDescription = false;
                            editTextItemDescription.setBackgroundColor(Color.RED);
                        } else if ((tip == 1 || tip == 2 || tip == 2) && !itemDescription.equals("")) {
                            editTextItemDescription.setBackgroundColor(Color.GREEN);
                        }

                        if (itemDescription.equals("")) itemDescription = null;
                        String endDate = editTextEndDate.getText().toString();
                        Date newEndDate = null;
                        if ((tip == 2 || tip == 5) && (endDate.equals("") || endDate.length() != 10 || endDate.charAt(0) < '0' || endDate.charAt(0) > '9' || endDate.charAt(1) < '0' || endDate.charAt(1) > '9' || endDate.charAt(2) != '-' || endDate.charAt(3) < '0' || endDate.charAt(3) > '9' ||
                                endDate.charAt(4) < '0' || endDate.charAt(4) > '9' || endDate.charAt(5) != '-' || endDate.charAt(6) < '0' || endDate.charAt(6) > '9' || endDate.charAt(7) < '0' || endDate.charAt(7) > '9' || endDate.charAt(8) < '0' || endDate.charAt(8) > '9' || endDate.charAt(9) < '0' || endDate.charAt(9) > '9')) {
                            validanKraj = false;
                            editTextEndDate.setBackgroundColor(Color.RED);

                        } else if (tip == 2 || tip == 5) {
                            String[] niz = endDate.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if (godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if (godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec - 1]) {
                                validanKraj = false;
                                editTextEndDate.setBackgroundColor(Color.RED);
                            }
                            try {
                                newEndDate = (Date) format.parse(endDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (newEndDate.before(newDate)) {
                                validanKraj = false;
                                editTextEndDate.setBackgroundColor(Color.RED);
                            } else {
                                editTextEndDate.setBackgroundColor(Color.GREEN);
                            }
                        } else if ((tip == 1 || tip == 3 || tip == 4) && !endDate.equals("")) {
                            validanKraj = false;
                            editTextEndDate.setBackgroundColor(Color.RED);
                        } else if ((tip == 1 || tip == 3 || tip == 4) && endDate.equals("")) {
                            editTextEndDate.setBackgroundColor(Color.GREEN);
                        }
                        String transactionInterval = editTextTransactionInterval.getText().toString();

                        if ((tip == 2 || tip == 5) && transactionInterval.equals("")) {
                            validanInterval = false;
                            editTextTransactionInterval.setBackgroundColor(Color.RED);
                        } else if ((tip == 2 || tip == 5) && !transactionInterval.equals("")) {
                            int provjera = -1;
                            try {
                                provjera = Integer.parseInt(editTextTransactionInterval.getText().toString());
                            } catch (NumberFormatException e) {
                                validanInterval = false;
                                editTextTransactionInterval.setBackgroundColor(Color.RED);
                            }
                            if (provjera < 0) {
                                validanInterval = false;
                                editTextTransactionInterval.setBackgroundColor(Color.RED);
                            } else {
                                editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                            }
                        } else if ((tip == 1 || tip == 3 || tip == 4) && !transactionInterval.equals("")) {
                            validanInterval = false;
                            editTextTransactionInterval.setBackgroundColor(Color.RED);
                        } else if ((tip == 1 || tip == 3 || tip == 4) && transactionInterval.equals("")) {
                            editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                        }
                        if (transactionInterval.equals("")) transactionInterval = null;

                        if (validanDatum && validanInterval && validanItemDescription && validanIznos && validanNaziv && validanKraj && validanTip) {
                            idT = idTransakcije;
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                            String end = null;
                            if(newEndDate != null) end = format.format(newEndDate);
                            getPresenter().create(newDate, Double.parseDouble(iznos), title, newType, itemDescription, transactionInterval, end);
                            getAccountPresenter().dohvatiAccount("account");

                        }
                    }
                });
//kraj promjene

                buttonDelete1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("Are you sure you want to delete this transaction?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        accountListPresenter.dajAccountZbogBrisanja("account");
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.setTitle("WARNING");
                        alert.show();
                    }

                    ;

                });
            } else {    // dodaje se tranasakcija
                String[] niz = {"transactions", "sveTransakcije"};
                getPresenter().dajSveTransakcije(niz);
                final EditText transactionTitle1 = (EditText) view.findViewById(R.id.editTextTransactionTitle);
                final EditText amount1 = (EditText) view.findViewById(R.id.editTextAmount2);
                final EditText date1 = (EditText) view.findViewById(R.id.editTextDateNovi);
                final EditText itemDescription1 = (EditText) view.findViewById(R.id.editTextItemDescription);
                final EditText endDate1 = (EditText) view.findViewById(R.id.editTextEndDate);
                final EditText transactionInterval1 = (EditText) view.findViewById(R.id.editTextTransactionInterval);
                Button delete1 = (Button) view.findViewById(R.id.buttonDelete);
                Button save1 = (Button) view.findViewById(R.id.buttonSave);
                delete1.setClickable(false);

                save1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String title = transactionTitle1.getText().toString();
                        String date = date1.getText().toString();
                        final String amount = amount1.getText().toString();
                        final String itemDescription = itemDescription1.getText().toString();
                        String endDate = endDate1.getText().toString();
                        final String transactionInterval = transactionInterval1.getText().toString();
                        final Transaction.TransactionType type;
                        int tip = spinnerType.getSelectedItemPosition();
                        if (tip == 1) type = Transaction.TransactionType.INDIVIDUALPAYMENT;
                        else if (tip == 2) type = Transaction.TransactionType.REGULARPAYMENT;
                        else if (tip == 3) type = Transaction.TransactionType.PURCHASE;
                        else if (tip == 4) type = Transaction.TransactionType.INDIVIDUALINCOME;
                        else if (tip == 5) type = Transaction.TransactionType.REGULARINCOME;
                        else type = null;
                        boolean validanNaziv = true;
                        boolean validanDatum = true;
                        boolean validanIznos = true;
                        boolean validanItemDescription = true;
                        boolean validanInterval = true;
                        boolean validanKraj = true;
                        boolean validanTip = true;

                        if (spinnerType.getSelectedItemPosition() == 0) {
                            spinnerType.setBackgroundColor(Color.RED);
                            validanTip = false;
                        } else spinnerType.setBackgroundColor(Color.GREEN);


                        if (title.equals("") || title.length() < 3 || title.length() > 15) {
                            validanNaziv = false;
                            transactionTitle1.setBackgroundColor(Color.RED);
                        } else transactionTitle1.setBackgroundColor(Color.GREEN);
                        if (amount.equals("")) {
                            validanIznos = false;
                            amount1.setBackgroundColor(Color.RED);
                        } else {
                            try {
                                double provjera = Double.parseDouble(amount);
                                if (provjera < 0) {
                                    validanIznos = false;
                                    amount1.setBackgroundColor(Color.RED);
                                } else {
                                    amount1.setBackgroundColor(Color.GREEN);
                                }
                            } catch (NumberFormatException e) {
                                validanIznos = false;
                                amount1.setBackgroundColor(Color.RED);
                            }
                        }
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        Date datum = null;
                        if (date.equals("") || date.length() != 10 || date.charAt(0) < '0' || date.charAt(0) > '9' || date.charAt(1) < '0' || date.charAt(1) > '9' || date.charAt(2) != '-' || date.charAt(3) < '0' || date.charAt(3) > '9' ||
                                date.charAt(4) < '0' || date.charAt(4) > '9' || date.charAt(5) != '-' || date.charAt(6) < '0' || date.charAt(6) > '9' || date.charAt(7) < '0' || date.charAt(7) > '9' || date.charAt(8) < '0' || date.charAt(8) > '9' || date.charAt(9) < '0' || date.charAt(9) > '9') {
                            validanDatum = false;
                            date1.setBackgroundColor(Color.RED);
                        } else {
                            String[] niz = date.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if (godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if (godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec - 1]) {
                                validanDatum = false;
                                date1.setBackgroundColor(Color.RED);
                            }
                            try {
                                datum = (Date) format.parse(date);
                                date1.setBackgroundColor(Color.GREEN);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if ((tip == 4 || tip == 5) && !itemDescription.equals("")) {
                            validanItemDescription = false;
                            itemDescription1.setBackgroundColor(Color.RED);
                        } else if ((tip == 4 || tip == 5) && itemDescription.equals("")) {
                            itemDescription1.setBackgroundColor(Color.GREEN);
                        } else if ((tip == 1 || tip == 2 || tip == 3) && itemDescription.equals("")) {
                            validanItemDescription = false;
                            itemDescription1.setBackgroundColor(Color.RED);
                        } else if ((tip == 1 || tip == 2 || tip == 3) && !itemDescription.equals("")) {
                            itemDescription1.setBackgroundColor(Color.GREEN);
                        }

                        Date kraj = null;
                        if ((tip == 2 || tip == 5) && (endDate.equals("") || endDate.length() != 10 || endDate.charAt(0) < '0' || endDate.charAt(0) > '9' || endDate.charAt(1) < '0' || endDate.charAt(1) > '9' || endDate.charAt(2) != '-' || endDate.charAt(3) < '0' || endDate.charAt(3) > '9' ||
                                endDate.charAt(4) < '0' || endDate.charAt(4) > '9' || endDate.charAt(5) != '-' || endDate.charAt(6) < '0' || endDate.charAt(6) > '9' || endDate.charAt(7) < '0' || endDate.charAt(7) > '9' || endDate.charAt(8) < '0' || endDate.charAt(8) > '9' || endDate.charAt(9) < '0' || endDate.charAt(9) > '9')) {
                            validanKraj = false;
                            endDate1.setBackgroundColor(Color.RED);

                        } else if (tip == 2 || tip == 5) {
                            String[] niz = endDate.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if (godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if (godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec - 1]) {
                                validanKraj = false;
                                endDate1.setBackgroundColor(Color.RED);
                            }
                            try {
                                kraj = (Date) format.parse(endDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (kraj.before(datum)) {
                                validanKraj = false;
                                endDate1.setBackgroundColor(Color.RED);
                            } else {
                                endDate1.setBackgroundColor(Color.GREEN);
                            }
                        } else if ((tip == 1 || tip == 3 || tip == 4) && !endDate.equals("")) {
                            validanKraj = false;
                            endDate1.setBackgroundColor(Color.RED);
                        } else if ((tip == 1 || tip == 3 || tip == 4) && endDate.equals("")) {
                            endDate1.setBackgroundColor(Color.GREEN);
                        }

                        if ((tip == 2 || tip == 5) && transactionInterval.equals("")) {
                            validanInterval = false;
                            transactionInterval1.setBackgroundColor(Color.RED);
                        } else if ((tip == 2 || tip == 5) && !transactionInterval.equals("")) {
                            int provjera = -1;
                            try {
                                provjera = Integer.parseInt(transactionInterval1.getText().toString());
                            } catch (NumberFormatException e) {
                                validanInterval = false;
                                transactionInterval1.setBackgroundColor(Color.RED);
                            }
                            if (provjera < 0) {
                                validanInterval = false;
                                transactionInterval1.setBackgroundColor(Color.RED);
                            } else {
                                transactionInterval1.setBackgroundColor(Color.GREEN);
                            }
                        } else if ((tip == 1 || tip == 3 || tip == 4) && !transactionInterval.equals("")) {
                            validanInterval = false;
                            transactionInterval1.setBackgroundColor(Color.RED);
                        } else if ((tip == 1 || tip == 3 || tip == 4) && transactionInterval.equals("")) {
                            transactionInterval1.setBackgroundColor(Color.GREEN);
                        }

                        if (validanDatum && validanInterval && validanItemDescription && validanIznos && validanKraj && validanNaziv && validanTip) {
                            double iznos = Double.parseDouble(amount);
                            String tInterval;
                            if (transactionInterval.equals("")) tInterval = null;
                            else tInterval = transactionInterval;
                            String iDescription;
                            if (itemDescription.equals("")) iDescription = null;
                            else iDescription = itemDescription;
                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                            String end = null;
                            if(kraj != null) end = format1.format(kraj);
                            getPresenter().create(datum, iznos, title, type, iDescription, tInterval, end);
                            getAccountPresenter().searchAccount("account");
                        }
                    }
                });
            }
        }
        else if(!isConnected()) {
             Double monthLimit = 0.0;
             Double totalLimit = 0.0;
             accountListPresenter = new AccountListPresenter(getContext(), this, "");
             accountListPresenter.dajAccountIzBaze(getContext());
             if(getArguments()!=null && getArguments().containsKey("monthLimit")) {
                 monthLimit = (Double) getArguments().getSerializable("monthLimit");
                 totalLimit = (Double) getArguments().getSerializable("totalLimit");
             }
            if (getArguments() != null && getArguments().containsKey("transaction") && getArguments().getParcelable("transaction") != null) {
                getPresenter().setTransaction(getArguments().getParcelable("transaction"));
                final Transaction transaction = transactionDetailPresenter.getTransaction();
                final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                if(transaction.getAkcija().equalsIgnoreCase("offline brisanje")) buttonDelete.setText("UNDO");
                String dateString = format.format(transaction.getDate());
                editTextDateNovi.setText(dateString);

                if(!transaction.getAkcija().equals("")) textViewAkcija.setText(transaction.getAkcija());
                String endDateString = "";
                if (transaction.getEndDate() != null)
                    endDateString = format.format(transaction.getEndDate());

                editTextTransactionTitle.setText(transaction.getTitle());


                editTextAmount2.setText(Double.toString(transaction.getAmount()));

                if (transaction.getItemDescription() != null)
                    editTextItemDescription.setText(transaction.getItemDescription());

                if (transaction.getTransactionInterval() == null)
                    editTextTransactionInterval.setText("");
                else
                    editTextTransactionInterval.setText(Integer.toString(transaction.getTransactionInterval()));

                if (transaction.getEndDate() != null) editTextEndDate.setText(endDateString);
                else editTextEndDate.setText("");

                if (Transaction.TransactionType.valueOf("INDIVIDUALPAYMENT").equals(transaction.getType()))
                    spinnerType.setSelection(1);
                else if (Transaction.TransactionType.valueOf("REGULARPAYMENT").equals(transaction.getType()))
                    spinnerType.setSelection(2);
                else if (Transaction.TransactionType.valueOf("PURCHASE").equals(transaction.getType()))
                    spinnerType.setSelection(3);
                else if (Transaction.TransactionType.valueOf("INDIVIDUALINCOME").equals(transaction.getType()))
                    spinnerType.setSelection(4);
                else spinnerType.setSelection(5);

                editTextTransactionTitle.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                      textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextTransactionTitle.setBackgroundColor(Color.GREEN);
                    }
                });

                editTextAmount2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextAmount2.setBackgroundColor(Color.GREEN);
                    }
                });

                editTextDateNovi.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextDateNovi.setBackgroundColor(Color.GREEN);
                    }
                });

                editTextEndDate.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextEndDate.setBackgroundColor(Color.GREEN);
                    }
                });
                editTextTransactionInterval.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                    }
                });
                editTextItemDescription.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextItemDescription.setBackgroundColor(Color.GREEN);
                    }
                });


                final Double finalMonthLimit2 = monthLimit;
                final Double finalTotalLimit2 = totalLimit;

                buttonSave1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String title = editTextTransactionTitle.getText().toString();
                        String date = editTextDateNovi.getText().toString();
                        final String amount = editTextAmount2.getText().toString();
                        final String itemDescription = editTextItemDescription.getText().toString();
                        final String endDate = editTextEndDate.getText().toString();
                        final String transactionInterval = editTextTransactionInterval.getText().toString();
                        final Transaction.TransactionType type;
                        int tip = spinnerType.getSelectedItemPosition();
                        if (tip == 1) type = Transaction.TransactionType.INDIVIDUALPAYMENT;
                        else if (tip == 2) type = Transaction.TransactionType.REGULARPAYMENT;
                        else if (tip == 3) type = Transaction.TransactionType.PURCHASE;
                        else if (tip == 4) type = Transaction.TransactionType.INDIVIDUALINCOME;
                        else if(tip == 5)  type = Transaction.TransactionType.REGULARINCOME;
                        else type = null;
                        boolean validanNaziv = true;
                        boolean validanDatum = true;
                        boolean validanIznos = true;
                        boolean validanItemDescription = true;
                        boolean validanInterval = true;
                        boolean validanKraj = true;
                        boolean validanTip = true;

                        if(spinnerType.getSelectedItemPosition()==0) {
                            spinnerType.setBackgroundColor(Color.RED);
                            validanTip = false;
                        }
                        else spinnerType.setBackgroundColor(Color.GREEN);


                        if(title.equals("") || title.length()<3 || title.length()>15) {
                            validanNaziv = false;
                            editTextTransactionTitle.setBackgroundColor(Color.RED);
                        }
                        else editTextTransactionTitle.setBackgroundColor(Color.GREEN);
                        if(amount.equals("")){
                            validanIznos = false;
                            editTextAmount2.setBackgroundColor(Color.RED);
                        }
                        else{
                            try {
                                double provjera = Double.parseDouble(amount);
                                if(provjera<0){
                                    validanIznos = false;
                                    editTextAmount2.setBackgroundColor(Color.RED);
                                }
                                else {
                                    editTextAmount2.setBackgroundColor(Color.GREEN);
                                }
                            } catch(NumberFormatException e){
                                validanIznos = false;
                                editTextAmount2.setBackgroundColor(Color.RED);
                            }
                        }
                        final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        Date datum = null;
                        if(date.equals("") || date.length()!=10 || date.charAt(0)<'0' || date.charAt(0)>'9' || date.charAt(1)<'0' || date.charAt(1)>'9' || date.charAt(2)!='-' || date.charAt(3)<'0' || date.charAt(3)>'9' ||
                                date.charAt(4)<'0' || date.charAt(4)>'9' || date.charAt(5)!='-' || date.charAt(6)<'0' || date.charAt(6)>'9' || date.charAt(7)<'0' || date.charAt(7)>'9' || date.charAt(8)<'0' || date.charAt(8)>'9' || date.charAt(9)<'0' || date.charAt(9)>'9'){
                            validanDatum = false;
                            editTextDateNovi.setBackgroundColor(Color.RED);
                        }
                        else{
                            String[] niz = date.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if(godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if(godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec-1]){
                                validanDatum = false;
                                editTextDateNovi.setBackgroundColor(Color.RED);
                            }
                            try {
                                datum = (Date) format.parse(date);
                                editTextDateNovi.setBackgroundColor(Color.GREEN);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if((tip == 4 || tip == 5) && !itemDescription.equals("")) {
                            validanItemDescription = false;
                            editTextItemDescription.setBackgroundColor(Color.RED);
                        }
                        else if((tip==4 || tip==5) && itemDescription.equals("")){
                            editTextItemDescription.setBackgroundColor(Color.GREEN);
                        }
                        else if((tip == 1 || tip == 2 ||tip==3) && itemDescription.equals("")){
                            validanItemDescription = false;
                            editTextItemDescription.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==2 || tip==3) && !itemDescription.equals("")){
                            editTextItemDescription.setBackgroundColor(Color.GREEN);
                        }

                        Date kraj = null;
                        if((tip == 2 || tip ==5) && (endDate.equals("") || endDate.length()!=10 || endDate.charAt(0)<'0' || endDate.charAt(0)>'9' || endDate.charAt(1)<'0' || endDate.charAt(1)>'9' || endDate.charAt(2)!='-' || endDate.charAt(3)<'0' || endDate.charAt(3)>'9' ||
                                endDate.charAt(4)<'0' || endDate.charAt(4)>'9' || endDate.charAt(5)!='-' || endDate.charAt(6)<'0' || endDate.charAt(6)>'9' || endDate.charAt(7)<'0' || endDate.charAt(7)>'9' || endDate.charAt(8)<'0' || endDate.charAt(8)>'9' || endDate.charAt(9)<'0' || endDate.charAt(9)>'9')) {
                            validanKraj = false;
                            editTextEndDate.setBackgroundColor(Color.RED);

                        }
                        else if(tip == 2 || tip ==5) {
                            String[] niz = endDate.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if(godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if(godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec-1]){
                                validanKraj = false;
                                editTextEndDate.setBackgroundColor(Color.RED);
                            }
                            try {
                                kraj = (Date) format.parse(endDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(kraj.before(datum)){
                                validanKraj = false;
                                editTextEndDate.setBackgroundColor(Color.RED);
                            }
                            else {
                                editTextEndDate.setBackgroundColor(Color.GREEN);
                            }
                        }
                        else if((tip==1 || tip==3 || tip == 4) && !endDate.equals("")){
                            validanKraj = false;
                            editTextEndDate.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==3 || tip==4) && endDate.equals("")){
                            editTextEndDate.setBackgroundColor(Color.GREEN);
                        }

                        if((tip == 2 || tip ==5) && transactionInterval.equals("")) {
                            validanInterval = false;
                            editTextTransactionInterval.setBackgroundColor(Color.RED);
                        }
                        else if((tip==2 || tip==5) && !transactionInterval.equals("")) {
                            int provjera = -1;
                            try{
                                provjera = Integer.parseInt(editTextTransactionInterval.getText().toString());
                            } catch (NumberFormatException e) {
                                validanInterval = false;
                                editTextTransactionInterval.setBackgroundColor(Color.RED);
                            }
                            if(provjera<0){
                                validanInterval = false;
                                editTextTransactionInterval.setBackgroundColor(Color.RED);
                            }
                            else{
                                editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                            }
                        }

                        else if((tip==1 || tip==3 || tip == 4) && !transactionInterval.equals("")){
                            validanInterval = false;
                            editTextTransactionInterval.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==3 || tip==4) && transactionInterval.equals("")){
                            editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                        }
                        if(validanDatum && validanInterval && validanItemDescription && validanIznos && validanKraj && validanNaziv && validanTip){
                            double iznosUMjesecu  = transactionDetailPresenter.iznosZaMjesec(datum);
                            double ukupniIznos = transactionDetailPresenter.ukupanIznos();
                            double iznos = Double.parseDouble(amount);
                            if(tip==1 || tip == 2 || tip==3) iznos = -iznos;
                            if (iznos + iznosUMjesecu> finalMonthLimit2 || ukupniIznos+iznos> finalTotalLimit2){
                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                final Date finalDatum1 = datum;
                                final double finalIznos2 = iznos;
                                final Date finalKraj1 = kraj;
                                final double finalIznos = iznos;
                                builder.setMessage("Are you sure you want to change this transaction?")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                String end = "null";
                                                String iDescription = "null";
                                                String interval = "null";
                                                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                                                if(finalKraj1 != null) end = format1.format(finalKraj1);
                                                if(!transactionInterval.equals("")) interval = transactionInterval;
                                                if(!itemDescription.equalsIgnoreCase("")) iDescription = itemDescription;
                                                transactionDetailPresenter.dodajTransakciju(getContext(), transaction.getId(), finalDatum1, Math.abs(finalIznos2),  title, type, iDescription, interval, end, "Offline izmjena");
                                                double noviIznos = transaction.getAmount()- Math.abs(finalIznos);
                                                accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(), noviIznos, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                                                onClick.onClickButtons(transactionDetailPresenter.kreirajTransakciju(transaction.getId(), finalDatum1, Math.abs(finalIznos2),  title, type, iDescription, interval, end, "Offline izmjena"));
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.setTitle("WARNING");
                                alert.show();
                            } else {
                                String end = "null";
                                String iDescription = "null";
                                String interval = "null";
                                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                                if(kraj != null) end = format1.format(kraj);
                                if(!transactionInterval.equals("")) interval = transactionInterval;
                                if(!itemDescription.equalsIgnoreCase("")) iDescription = itemDescription;
                                transactionDetailPresenter.dodajTransakciju(getContext(), transaction.getId(), datum, Math.abs(iznos),  title, type, iDescription, interval, end, "Offline izmjena");
                                double noviIznos = transaction.getAmount()- Math.abs(iznos);
                                accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(), noviIznos, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                                onClick.onClickButtons(transactionDetailPresenter.kreirajTransakciju(transaction.getId(), datum, Math.abs(iznos),  title, type, iDescription, interval, end, "Offline izmjena"));
                            }}}});





                buttonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (buttonDelete.getText().toString().equalsIgnoreCase("UNDO")) {
                            getPresenter().getTransaction().setAkcija("");
                            transactionDetailPresenter.obrisiTransakcijuUBazi(getPresenter().getTransaction());
                            onClick.onClickButtons(getPresenter().getTransaction());
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setMessage("Are you sure you want to delete this transaction?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                          textViewAkcija.setText("offline brisanje");
                                          transaction.setAkcija("offline brisanje");
                                          String iDesc = "null";
                                          String tInt = "null";
                                          String eD = "null";
                                          if(transaction.getItemDescription() != null) iDesc = transaction.getItemDescription();
                                          if(transaction.getTransactionInterval()!=null) tInt = Integer.toString(transaction.getTransactionInterval());
                                          if(transaction.getEndDate()!=null) eD = format.format(transaction.getEndDate());
                                          transactionDetailPresenter.dodajTransakciju(getContext(),transaction.getId(),transaction.getDate(), transaction.getAmount(), transaction.getTitle(), transaction.getType(), iDesc, tInt, eD, "offline brisanje");
                                          double iznos = transaction.getAmount();
                                           if(transaction.getType().equals(Transaction.TransactionType.REGULARINCOME) || transaction.getType().equals(Transaction.TransactionType.INDIVIDUALINCOME)) iznos = transaction.getAmount()*(-1);
                                          accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(), iznos, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                                          onClick.onClickButtons(transaction);
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.setTitle("WARNING");
                            alert.show();

                    }
                }});
            }
            else if(getArguments() != null && getArguments().containsKey("transaction") && getArguments().getParcelable("transaction") == null) { //offline dodavanje transakcije
                textViewAkcija.setText("offline dodavanje");
                final Double finalMonthLimit = monthLimit;
                final Double finalTotalLimit = totalLimit;
                buttonSave1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String title = editTextTransactionTitle.getText().toString();
                        String date = editTextDateNovi.getText().toString();
                        final String amount = editTextAmount2.getText().toString();
                        final String itemDescription = editTextItemDescription.getText().toString();
                        final String endDate = editTextEndDate.getText().toString();
                        final String transactionInterval = editTextTransactionInterval.getText().toString();
                        final Transaction.TransactionType type;
                        int tip = spinnerType.getSelectedItemPosition();
                        if (tip == 1) type = Transaction.TransactionType.INDIVIDUALPAYMENT;
                        else if (tip == 2) type = Transaction.TransactionType.REGULARPAYMENT;
                        else if (tip == 3) type = Transaction.TransactionType.PURCHASE;
                        else if (tip == 4) type = Transaction.TransactionType.INDIVIDUALINCOME;
                        else if(tip == 5)  type = Transaction.TransactionType.REGULARINCOME;
                        else type = null;
                        boolean validanNaziv = true;
                        boolean validanDatum = true;
                        boolean validanIznos = true;
                        boolean validanItemDescription = true;
                        boolean validanInterval = true;
                        boolean validanKraj = true;
                        boolean validanTip = true;

                        if(spinnerType.getSelectedItemPosition()==0) {
                            spinnerType.setBackgroundColor(Color.RED);
                            validanTip = false;
                        }
                        else spinnerType.setBackgroundColor(Color.GREEN);


                        if(title.equals("") || title.length()<3 || title.length()>15) {
                            validanNaziv = false;
                            editTextTransactionTitle.setBackgroundColor(Color.RED);
                        }
                        else editTextTransactionTitle.setBackgroundColor(Color.GREEN);
                        if(amount.equals("")){
                            validanIznos = false;
                            editTextAmount2.setBackgroundColor(Color.RED);
                        }
                        else{
                            try {
                                double provjera = Double.parseDouble(amount);
                                if(provjera<0){
                                    validanIznos = false;
                                    editTextAmount2.setBackgroundColor(Color.RED);
                                }
                                else {
                                    editTextAmount2.setBackgroundColor(Color.GREEN);
                                }
                            } catch(NumberFormatException e){
                                validanIznos = false;
                                editTextAmount2.setBackgroundColor(Color.RED);
                            }
                        }
                        final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        Date datum = null;
                        if(date.equals("") || date.length()!=10 || date.charAt(0)<'0' || date.charAt(0)>'9' || date.charAt(1)<'0' || date.charAt(1)>'9' || date.charAt(2)!='-' || date.charAt(3)<'0' || date.charAt(3)>'9' ||
                                date.charAt(4)<'0' || date.charAt(4)>'9' || date.charAt(5)!='-' || date.charAt(6)<'0' || date.charAt(6)>'9' || date.charAt(7)<'0' || date.charAt(7)>'9' || date.charAt(8)<'0' || date.charAt(8)>'9' || date.charAt(9)<'0' || date.charAt(9)>'9'){
                            validanDatum = false;
                            editTextDateNovi.setBackgroundColor(Color.RED);
                        }
                        else{
                            String[] niz = date.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if(godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if(godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec-1]){
                                validanDatum = false;
                                editTextDateNovi.setBackgroundColor(Color.RED);
                            }
                            try {
                                datum = (Date) format.parse(date);
                                editTextDateNovi.setBackgroundColor(Color.GREEN);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if((tip == 4 || tip == 5) && !itemDescription.equals("")) {
                            validanItemDescription = false;
                            editTextItemDescription.setBackgroundColor(Color.RED);
                        }
                        else if((tip==4 || tip==5) && itemDescription.equals("")){
                            editTextItemDescription.setBackgroundColor(Color.GREEN);
                        }
                        else if((tip == 1 || tip == 2 ||tip==3) && itemDescription.equals("")){
                            validanItemDescription = false;
                            editTextItemDescription.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==2 || tip==3) && !itemDescription.equals("")){
                            editTextItemDescription.setBackgroundColor(Color.GREEN);
                        }

                        Date kraj = null;
                        if((tip == 2 || tip ==5) && (endDate.equals("") || endDate.length()!=10 || endDate.charAt(0)<'0' || endDate.charAt(0)>'9' || endDate.charAt(1)<'0' || endDate.charAt(1)>'9' || endDate.charAt(2)!='-' || endDate.charAt(3)<'0' || endDate.charAt(3)>'9' ||
                                endDate.charAt(4)<'0' || endDate.charAt(4)>'9' || endDate.charAt(5)!='-' || endDate.charAt(6)<'0' || endDate.charAt(6)>'9' || endDate.charAt(7)<'0' || endDate.charAt(7)>'9' || endDate.charAt(8)<'0' || endDate.charAt(8)>'9' || endDate.charAt(9)<'0' || endDate.charAt(9)>'9')) {
                            validanKraj = false;
                            editTextEndDate.setBackgroundColor(Color.RED);

                        }
                        else if(tip == 2 || tip ==5) {
                            String[] niz = endDate.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if(godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if(godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec-1]){
                                validanKraj = false;
                                editTextEndDate.setBackgroundColor(Color.RED);
                            }
                            try {
                                kraj = (Date) format.parse(endDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(kraj.before(datum)){
                                validanKraj = false;
                                editTextEndDate.setBackgroundColor(Color.RED);
                            }
                            else {
                                editTextEndDate.setBackgroundColor(Color.GREEN);
                            }
                        }
                        else if((tip==1 || tip==3 || tip == 4) && !endDate.equals("")){
                            validanKraj = false;
                            editTextEndDate.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==3 || tip==4) && endDate.equals("")){
                            editTextEndDate.setBackgroundColor(Color.GREEN);
                        }

                        if((tip == 2 || tip ==5) && transactionInterval.equals("")) {
                            validanInterval = false;
                            editTextTransactionInterval.setBackgroundColor(Color.RED);
                        }
                        else if((tip==2 || tip==5) && !transactionInterval.equals("")) {
                            int provjera = -1;
                            try{
                                provjera = Integer.parseInt(editTextTransactionInterval.getText().toString());
                            } catch (NumberFormatException e) {
                                validanInterval = false;
                                editTextTransactionInterval.setBackgroundColor(Color.RED);
                            }
                            if(provjera<0){
                                validanInterval = false;
                                editTextTransactionInterval.setBackgroundColor(Color.RED);
                            }
                            else{
                                editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                            }
                        }

                        else if((tip==1 || tip==3 || tip == 4) && !transactionInterval.equals("")){
                            validanInterval = false;
                            editTextTransactionInterval.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==3 || tip==4) && transactionInterval.equals("")){
                            editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                        }
            if(validanDatum && validanInterval && validanItemDescription && validanIznos && validanKraj && validanNaziv && validanTip){
                        double iznosUMjesecu  = transactionDetailPresenter.iznosZaMjesec(datum);
                        double ukupniIznos = transactionDetailPresenter.ukupanIznos();
                        double iznos = Double.parseDouble(amount);
                        if(tip==1 || tip == 2 || tip==3) iznos*=(-1);
                        if (iznos + iznosUMjesecu> finalMonthLimit || ukupniIznos+iznos> finalTotalLimit){
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            final Date finalDatum1 = datum;
                            final double finalIznos2 = iznos;
                            final Date finalKraj1 = kraj;
                            builder.setMessage("Are you sure you want to add this transaction?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            String end = "null";
                                            String iDescription = "null";
                                            String interval = "null";
                                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                                            if(finalKraj1 != null) end = format1.format(finalKraj1);
                                            if(!transactionInterval.equals("")) interval = transactionInterval;
                                            if(!itemDescription.equalsIgnoreCase("")) iDescription = itemDescription;
                                            System.out.println("kraj " + end);
                                            transactionDetailPresenter.dodajTransakciju(getContext(), -1, finalDatum1, Math.abs(finalIznos2),  title, type, iDescription, interval, end, "offline dodavanje");
                                            accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(), finalIznos2, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                                            onClick.onClickButtons(transactionDetailPresenter.kreirajTransakciju(TransactionDetailInteractor.id, finalDatum1, Math.abs(finalIznos2),  title, type, iDescription, interval, end, "offline dodavanje"));
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.setTitle("WARNING");
                            alert.show();
                        } else {
                            String end = "null";
                            String iDescription = "null";
                            String interval = "null";
                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                            if(kraj != null) end = format1.format(kraj);
                            if(!transactionInterval.equals("")) interval = transactionInterval;
                            if(!itemDescription.equalsIgnoreCase("")) iDescription = itemDescription;
                            System.out.println("kraj " + end);
                            transactionDetailPresenter.dodajTransakciju(getContext(), -1, datum, Math.abs(iznos),  title, type, iDescription, interval, end, "offline dodavanje");
                            accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(), iznos, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                            onClick.onClickButtons(transactionDetailPresenter.kreirajTransakciju(TransactionDetailInteractor.id, datum, Math.abs(iznos),  title, type, iDescription, interval, end, "offline dodavanje"));
                        }}}});
            }
            else if (getArguments() != null && getArguments().containsKey("internal_id")) {
                int id = getArguments().getInt("internal_id");
                getPresenter().getDatabaseTransaction(id);
                refreshView();

                editTextTransactionTitle.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextTransactionTitle.setBackgroundColor(Color.GREEN);
                    }
                });

                editTextAmount2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextAmount2.setBackgroundColor(Color.GREEN);
                    }
                });

                editTextDateNovi.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextDateNovi.setBackgroundColor(Color.GREEN);
                    }
                });

                editTextEndDate.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextEndDate.setBackgroundColor(Color.GREEN);
                    }
                });
                editTextTransactionInterval.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                    }
                });
                editTextItemDescription.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        textViewAkcija.setText("Offline izmjena");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editTextItemDescription.setBackgroundColor(Color.GREEN);
                    }
                });




                buttonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (buttonDelete.getText().toString().equalsIgnoreCase("UNDO")) {
                            getPresenter().getTransaction().setAkcija("");
                            transactionDetailPresenter.obrisiTransakcijuUBazi(getPresenter().getTransaction());
                            onClick.onClickButtons(getPresenter().getTransaction());
                        } else {
                            textViewAkcija.setText("offline brisanje");
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setMessage("Are you sure you want to delete this transaction?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                                Transaction tr = getPresenter().getTransaction();
                                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                                                String interval = "null";
                                                if(tr.getTransactionInterval()!=null) interval = Integer.toString(tr.getTransactionInterval());
                                                String end = "null";
                                                if(tr.getEndDate() != null) end = format.format(tr.getEndDate());
                                                String iDesc = "null";
                                                if(tr.getItemDescription() != null) iDesc = tr.getItemDescription();

                                                    transactionDetailPresenter.izmijeniTransakciju(getContext(), tr.getId(), tr.getDate(), tr.getAmount(), tr.getTitle(), tr.getType(), iDesc, interval, end, "offline brisanje");
                                                    getPresenter().getTransaction().setAkcija("offline brisanje");
                                            double iznos = getPresenter().getTransaction().getAmount();
                                            if(getPresenter().getTransaction().getType().equals(Transaction.TransactionType.REGULARINCOME) || getPresenter().getTransaction().getType().equals(Transaction.TransactionType.INDIVIDUALINCOME)) iznos = getPresenter().getTransaction().getAmount()*(-1);
                                            accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(), iznos, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                                            onClick.onClickButtons(getPresenter().getTransaction());
                                    }})
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.setTitle("WARNING");
                            alert.show();

                        }
                    }});


                final Double finalMonthLimit1 = monthLimit;
                final Double finalTotalLimit1 = totalLimit;

                buttonSave1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String title = editTextTransactionTitle.getText().toString();
                        String date = editTextDateNovi.getText().toString();
                        final String amount = editTextAmount2.getText().toString();
                        final String itemDescription = editTextItemDescription.getText().toString();
                        final String endDate = editTextEndDate.getText().toString();
                        final String transactionInterval = editTextTransactionInterval.getText().toString();
                        final Transaction.TransactionType type;
                        int tip = spinnerType.getSelectedItemPosition();
                        if (tip == 1) type = Transaction.TransactionType.INDIVIDUALPAYMENT;
                        else if (tip == 2) type = Transaction.TransactionType.REGULARPAYMENT;
                        else if (tip == 3) type = Transaction.TransactionType.PURCHASE;
                        else if (tip == 4) type = Transaction.TransactionType.INDIVIDUALINCOME;
                        else if(tip == 5)  type = Transaction.TransactionType.REGULARINCOME;
                        else type = null;
                        boolean validanNaziv = true;
                        boolean validanDatum = true;
                        boolean validanIznos = true;
                        boolean validanItemDescription = true;
                        boolean validanInterval = true;
                        boolean validanKraj = true;
                        boolean validanTip = true;

                        if(spinnerType.getSelectedItemPosition()==0) {
                            spinnerType.setBackgroundColor(Color.RED);
                            validanTip = false;
                        }
                        else spinnerType.setBackgroundColor(Color.GREEN);


                        if(title.equals("") || title.length()<3 || title.length()>15) {
                            validanNaziv = false;
                            editTextTransactionTitle.setBackgroundColor(Color.RED);
                        }
                        else editTextTransactionTitle.setBackgroundColor(Color.GREEN);
                        if(amount.equals("")){
                            validanIznos = false;
                            editTextAmount2.setBackgroundColor(Color.RED);
                        }
                        else{
                            try {
                                double provjera = Double.parseDouble(amount);
                                if(provjera<0){
                                    validanIznos = false;
                                    editTextAmount2.setBackgroundColor(Color.RED);
                                }
                                else {
                                    editTextAmount2.setBackgroundColor(Color.GREEN);
                                }
                            } catch(NumberFormatException e){
                                validanIznos = false;
                                editTextAmount2.setBackgroundColor(Color.RED);
                            }
                        }
                        final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        Date datum = null;
                        if(date.equals("") || date.length()!=10 || date.charAt(0)<'0' || date.charAt(0)>'9' || date.charAt(1)<'0' || date.charAt(1)>'9' || date.charAt(2)!='-' || date.charAt(3)<'0' || date.charAt(3)>'9' ||
                                date.charAt(4)<'0' || date.charAt(4)>'9' || date.charAt(5)!='-' || date.charAt(6)<'0' || date.charAt(6)>'9' || date.charAt(7)<'0' || date.charAt(7)>'9' || date.charAt(8)<'0' || date.charAt(8)>'9' || date.charAt(9)<'0' || date.charAt(9)>'9'){
                            validanDatum = false;
                            editTextDateNovi.setBackgroundColor(Color.RED);
                        }
                        else{
                            String[] niz = date.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if(godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if(godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec-1]){
                                validanDatum = false;
                                editTextDateNovi.setBackgroundColor(Color.RED);
                            }
                            try {
                                datum = (Date) format.parse(date);
                                editTextDateNovi.setBackgroundColor(Color.GREEN);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if((tip == 4 || tip == 5) && !itemDescription.equals("")) {
                            validanItemDescription = false;
                            editTextItemDescription.setBackgroundColor(Color.RED);
                        }
                        else if((tip==4 || tip==5) && itemDescription.equals("")){
                            editTextItemDescription.setBackgroundColor(Color.GREEN);
                        }
                        else if((tip == 1 || tip == 2 ||tip==3) && itemDescription.equals("")){
                            validanItemDescription = false;
                            editTextItemDescription.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==2 || tip==3) && !itemDescription.equals("")){
                            editTextItemDescription.setBackgroundColor(Color.GREEN);
                        }

                        Date kraj = null;
                        if((tip == 2 || tip ==5) && (endDate.equals("") || endDate.length()!=10 || endDate.charAt(0)<'0' || endDate.charAt(0)>'9' || endDate.charAt(1)<'0' || endDate.charAt(1)>'9' || endDate.charAt(2)!='-' || endDate.charAt(3)<'0' || endDate.charAt(3)>'9' ||
                                endDate.charAt(4)<'0' || endDate.charAt(4)>'9' || endDate.charAt(5)!='-' || endDate.charAt(6)<'0' || endDate.charAt(6)>'9' || endDate.charAt(7)<'0' || endDate.charAt(7)>'9' || endDate.charAt(8)<'0' || endDate.charAt(8)>'9' || endDate.charAt(9)<'0' || endDate.charAt(9)>'9')) {
                            validanKraj = false;
                            editTextEndDate.setBackgroundColor(Color.RED);

                        }
                        else if(tip == 2 || tip ==5) {
                            String[] niz = endDate.split("-");
                            int dan = Integer.parseInt(niz[0]);
                            int mjesec = Integer.parseInt(niz[1]);
                            int godina = Integer.parseInt(niz[2]);
                            int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                            if(godina % 4 == 0 && godina % 100 != 0 || godina % 400 == 0)
                                brojDana[1]++;
                            if(godina < 1 || mjesec < 1 || mjesec > 12 || dan < 1
                                    || dan > brojDana[mjesec-1]){
                                validanKraj = false;
                                editTextEndDate.setBackgroundColor(Color.RED);
                            }
                            try {
                                kraj = (Date) format.parse(endDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(kraj.before(datum)){
                                validanKraj = false;
                                editTextEndDate.setBackgroundColor(Color.RED);
                            }
                            else {
                                editTextEndDate.setBackgroundColor(Color.GREEN);
                            }
                        }
                        else if((tip==1 || tip==3 || tip == 4) && !endDate.equals("")){
                            validanKraj = false;
                            editTextEndDate.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==3 || tip==4) && endDate.equals("")){
                            editTextEndDate.setBackgroundColor(Color.GREEN);
                        }

                        if((tip == 2 || tip ==5) && transactionInterval.equals("")) {
                            validanInterval = false;
                            editTextTransactionInterval.setBackgroundColor(Color.RED);
                        }
                        else if((tip==2 || tip==5) && !transactionInterval.equals("")) {
                            int provjera = -1;
                            try{
                                provjera = Integer.parseInt(editTextTransactionInterval.getText().toString());
                            } catch (NumberFormatException e) {
                                validanInterval = false;
                                editTextTransactionInterval.setBackgroundColor(Color.RED);
                            }
                            if(provjera<0){
                                validanInterval = false;
                                editTextTransactionInterval.setBackgroundColor(Color.RED);
                            }
                            else{
                                editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                            }
                        }

                        else if((tip==1 || tip==3 || tip == 4) && !transactionInterval.equals("")){
                            validanInterval = false;
                            editTextTransactionInterval.setBackgroundColor(Color.RED);
                        }
                        else if((tip==1 || tip==3 || tip==4) && transactionInterval.equals("")){
                            editTextTransactionInterval.setBackgroundColor(Color.GREEN);
                        }
                        if(validanDatum && validanInterval && validanItemDescription && validanIznos && validanKraj && validanNaziv && validanTip){
                            double iznosUMjesecu  = transactionDetailPresenter.iznosZaMjesec(datum);
                            double ukupniIznos = transactionDetailPresenter.ukupanIznos();
                            double iznos = Double.parseDouble(amount);
                            if(tip==1 || tip == 2 || tip==3) iznos = -iznos;
                            if (iznos + iznosUMjesecu> finalMonthLimit1 || ukupniIznos+iznos> finalTotalLimit1){
                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                final Date finalDatum1 = datum;
                                final double finalIznos2 = iznos;
                                final Date finalKraj1 = kraj;
                                builder.setMessage("Are you sure you want to change this transaction?")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                String end = "null";
                                                String iDescription = "null";
                                                String interval = "null";
                                                if(finalKraj1 != null) end = format.format(finalKraj1);
                                                if(!transactionInterval.equals("")) interval = transactionInterval;
                                                if(!itemDescription.equalsIgnoreCase("")) iDescription = itemDescription;
                                                transactionDetailPresenter.izmijeniTransakciju(getContext(), getPresenter().getTransaction().getId(), finalDatum1, Math.abs(finalIznos2),  title, type, iDescription, interval, end, "Offline izmjena");
                                                double noviIznos = getPresenter().getTransaction().getAmount()- Math.abs(finalIznos2);
                                                accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(), noviIznos, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                                                onClick.onClickButtons(transactionDetailPresenter.kreirajTransakciju(getPresenter().getTransaction().getId(), finalDatum1, Math.abs(finalIznos2),  title, type, iDescription, interval, end, "Offline izmjena"));
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.setTitle("WARNING");
                                alert.show();
                            } else {
                                String end = "null";
                                String iDescription = "null";
                                String interval = "null";
                                if(kraj != null) end = format.format(kraj);
                                if(!transactionInterval.equals("")) interval = transactionInterval;
                                if(!itemDescription.equalsIgnoreCase("")) iDescription = itemDescription;
                                transactionDetailPresenter.izmijeniTransakciju(getContext(), getPresenter().getTransaction().getId(), datum, Math.abs(iznos),  title, type, iDescription, interval, end, "Offline izmjena");
                                double noviIznos = getPresenter().getTransaction().getAmount()- Math.abs(iznos);
                                accountListPresenter.updateAccount(getContext(), accountListPresenter.getAccount().getId(), noviIznos, accountListPresenter.getAccount().getMonthLimit(), accountListPresenter.getAccount().getTotalLimit());
                                onClick.onClickButtons(transactionDetailPresenter.kreirajTransakciju(getPresenter().getTransaction().getId(), datum, Math.abs(iznos),  title, type, iDescription, interval, end, "Offline izmjena"));
                            }}}});

            }
        }
     return view;
    }

    @Override
    public void refreshView() {

        Transaction transaction = getPresenter().getTransaction();
        if(transaction.getAkcija().equalsIgnoreCase("offline brisanje")) buttonDelete.setText("UNDO");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        editTextTransactionTitle.setText(transaction.getTitle());
        editTextDateNovi.setText(format.format(transaction.getDate()));
        editTextAmount2.setText(Double.toString(transaction.getAmount()));
        editTextItemDescription.setText(transaction.getItemDescription());
        if(transaction.getEndDate()!=null) editTextEndDate.setText(format.format(transaction.getEndDate()));
        else editTextEndDate.setText("");
        if(transaction.getTransactionInterval()!=null) editTextTransactionInterval.setText(Integer.toString(transaction.getTransactionInterval()));
        else editTextTransactionInterval.setText("");
        textViewAkcija.setText(transaction.getAkcija());
        if (Transaction.TransactionType.valueOf("INDIVIDUALPAYMENT").equals(transaction.getType()))
            spinnerType.setSelection(1);
        else if (Transaction.TransactionType.valueOf("REGULARPAYMENT").equals(transaction.getType()))
            spinnerType.setSelection(2);
        else if (Transaction.TransactionType.valueOf("PURCHASE").equals(transaction.getType()))
            spinnerType.setSelection(3);
        else if (Transaction.TransactionType.valueOf("INDIVIDUALINCOME").equals(transaction.getType()))
            spinnerType.setSelection(4);
        else spinnerType.setSelection(5);

    }

    @Override
    public void pozoviOnClick() {
        onClick.onClickButtons();
    }
    @Override
    public void postaviPodatke(Transaction transaction) {
            final EditText editTextTransactionTitle2 = getView().findViewById(R.id.editTextTransactionTitle);
            final EditText editTextDate1 = getView().findViewById(R.id.editTextDateNovi);
            final EditText editTextAmount3 = getView().findViewById(R.id.editTextAmount2);
            final EditText editTextItemDescription1 = getView().findViewById(R.id.editTextItemDescription);
            final EditText editTextTransactionInterval1 = getView().findViewById(R.id.editTextTransactionInterval);
            final EditText editTextEndDate1 = getView().findViewById(R.id.editTextEndDate);
            Spinner spinnerType1 = getView().findViewById(R.id.spinnerType);
            final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

            String dateString = format.format(transaction.getDate());
            editTextDate1.setText(dateString);

            String endDateString = "";
            if (transaction.getEndDate() != null)
                endDateString = format.format(transaction.getEndDate());


            editTextTransactionTitle2.setText(transaction.getTitle());


            editTextAmount3.setText(Double.toString(transaction.getAmount()));

            if (transaction.getItemDescription() != null)
                editTextItemDescription1.setText(transaction.getItemDescription());

            if (transaction.getTransactionInterval() == null) editTextTransactionInterval1.setText("");
            else
                editTextTransactionInterval1.setText(Integer.toString(transaction.getTransactionInterval()));

            if (transaction.getEndDate() != null) editTextEndDate1.setText(endDateString);
            else editTextEndDate1.setText("");

            if (Transaction.TransactionType.valueOf("INDIVIDUALPAYMENT").equals(transaction.getType()))
                spinnerType1.setSelection(1);
            else if (Transaction.TransactionType.valueOf("REGULARPAYMENT").equals(transaction.getType()))
                spinnerType1.setSelection(2);
            else if (Transaction.TransactionType.valueOf("PURCHASE").equals(transaction.getType()))
                spinnerType1.setSelection(3);
            else if (Transaction.TransactionType.valueOf("INDIVIDUALINCOME").equals(transaction.getType()))
                spinnerType1.setSelection(4);
            else spinnerType1.setSelection(5);


            editTextTransactionTitle2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    editTextTransactionTitle2.setBackgroundColor(Color.GREEN);
                }
            });

            editTextAmount3.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    editTextAmount3.setBackgroundColor(Color.GREEN);
                }
            });

            editTextDate1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    editTextDate1.setBackgroundColor(Color.GREEN);
                }
            });

            editTextEndDate1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    editTextEndDate1.setBackgroundColor(Color.GREEN);
                }
            });
            editTextTransactionInterval1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    editTextTransactionInterval1.setBackgroundColor(Color.GREEN);
                }
            });
            editTextItemDescription1.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    editTextItemDescription1.setBackgroundColor(Color.GREEN);
                }
            });
            idT = transaction.getId();

    }
    @Override
    public void editujTransakciju(Account account) {

        double monthLimit = account.getMonthLimit();
        double totalLimit = account.getTotalLimit();

        double iznosUMjesecu = transactionDetailPresenter.iznosZaMjesec(transactionDetailPresenter.getTransaction().getDate());
        double ukupniIznos = transactionDetailPresenter.ukupanIznos();

        final Transaction.TransactionType type = transactionDetailPresenter.getTransaction().getType();
        final double iznos = transactionDetailPresenter.getTransaction().getAmount();
        double praviIznos = iznos;
        if (type == Transaction.TransactionType.INDIVIDUALPAYMENT || type == Transaction.TransactionType.REGULARPAYMENT || type == Transaction.TransactionType.PURCHASE) {
            praviIznos = -iznos;
        }


        String title = transactionDetailPresenter.getTransaction().getTitle();
        Date datum = transactionDetailPresenter.getTransaction().getDate();
        Date kraj = transactionDetailPresenter.getTransaction().getEndDate();
        Integer tInterval1 = transactionDetailPresenter.getTransaction().getTransactionInterval();
        String iDescription = transactionDetailPresenter.getTransaction().getItemDescription();
        String jsonInputString = "{\n" +
                "\"title\" : " + "\"" + title + "\"" + "," + "\n" +
                "\"date\" : " + "\"" + datum + "\"" + "," + "\n" +
                "\"amount\" : " + Math.abs(iznos);

        if (kraj != null) {
            jsonInputString += "," + "\n" + "\"endDate\": " + "\"" + kraj + "\"";
        }

        if (tInterval1 != null) {
            jsonInputString += "," + "\n" + "\"transactionInterval\": " + tInterval1;
        }
        if (iDescription != null)
            jsonInputString += "," + "\n" + "\"itemDescription\": " + "\"" + iDescription + "\"";

        if (praviIznos + iznosUMjesecu > monthLimit || ukupniIznos + praviIznos > totalLimit) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            final String finalJsonInputString = jsonInputString;
            builder.setMessage("Are you sure you want to change this transaction?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        transactionDetailPresenter.changeTransaction("transactionID", Integer.toString(idT), type.toString(), finalJsonInputString);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle("WARNING");
        alert.show();
        } else {
            transactionDetailPresenter.changeTransaction("transactionID", Integer.toString(idT), type.toString(), jsonInputString);
    }

        onClick.onClickButtons();
    }

    @Override
    public void setAccount() {
        if (accountListPresenter != null && accountListPresenter.getAccount() != null) {
            Account account = accountListPresenter.getAccount();
            double iznos = getPresenter().getTransaction().getAmount();
            Transaction.TransactionType tip = getPresenter().getTransaction().getType();
            if (tip == Transaction.TransactionType.INDIVIDUALPAYMENT || tip == Transaction.TransactionType.REGULARPAYMENT || tip == Transaction.TransactionType.PURCHASE)
                iznos = -iznos;
            String[] values = {"account", Double.toString(account.getBudget() + iznos), Double.toString(account.getTotalLimit()), Double.toString(account.getMonthLimit())};
            accountListPresenter.editAccount(values);

        }
    }
    @Override
    public void dodajTransakciju(final Account account) {
        double monthLimit = account.getMonthLimit();
        double totalLimit = account.getTotalLimit();

        double iznosUMjesecu = transactionDetailPresenter.iznosZaMjesec(transactionDetailPresenter.getTransaction().getDate());
        double ukupniIznos = transactionDetailPresenter.ukupanIznos();

        final Transaction.TransactionType type = transactionDetailPresenter.getTransaction().getType();
        final double iznos = transactionDetailPresenter.getTransaction().getAmount();
        double praviIznos = iznos;
        if (type == Transaction.TransactionType.INDIVIDUALPAYMENT || type == Transaction.TransactionType.REGULARPAYMENT || type == Transaction.TransactionType.PURCHASE) {
            praviIznos = -iznos;
        }

        if (praviIznos + iznosUMjesecu > monthLimit || ukupniIznos + praviIznos > totalLimit) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("Are you sure you want to add this transaction?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
            String title = transactionDetailPresenter.getTransaction().getTitle();
            Date datum = transactionDetailPresenter.getTransaction().getDate();
            Date kraj = transactionDetailPresenter.getTransaction().getEndDate();
            Integer tInterval1 = transactionDetailPresenter.getTransaction().getTransactionInterval();
            String iDescription = transactionDetailPresenter.getTransaction().getItemDescription();
            String jsonInputString = "{\n" +
                    "\"title\" : " + "\"" + title + "\"" + "," + "\n" +
                    "\"date\" : " + "\"" + datum + "\"" + "," + "\n" +
                    "\"amount\" : " + Math.abs(iznos);

            if (kraj != null) {
                jsonInputString += "," + "\n" + "\"endDate\": " + "\"" + kraj + "\"";
            }

            if (tInterval1 != null) {
                jsonInputString += "," + "\n" + "\"transactionInterval\": " + tInterval1;
            }
            if (iDescription != null)
                jsonInputString += "," + "\n" + "\"itemDescription\": " + "\"" + iDescription + "\"";

            String[] zaQuery = {"transactions", jsonInputString, type.toString()};
            transactionDetailPresenter.addTransaction(zaQuery);
        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.setTitle("WARNING");
            alert.show();
        } else {
            String title = transactionDetailPresenter.getTransaction().getTitle();
            Date datum = transactionDetailPresenter.getTransaction().getDate();
            Date kraj = transactionDetailPresenter.getTransaction().getEndDate();
            Integer tInterval1 = transactionDetailPresenter.getTransaction().getTransactionInterval();
            String iDescription = transactionDetailPresenter.getTransaction().getItemDescription();
            String jsonInputString = "{\n" +
                    "\"title\" : " + "\"" + title + "\"" + "," + "\n" +
                    "\"date\" : " + "\"" + datum + "\"" + "," + "\n" +
                    "\"amount\" : " + Math.abs(iznos);

            if (kraj != null) {
                jsonInputString += "," + "\n" + "\"endDate\": " + "\"" + kraj + "\"";
            }

            if (tInterval1 != null) {
                jsonInputString += "," + "\n" + "\"transactionInterval\": " + tInterval1;
            }
            if (iDescription != null)
                jsonInputString += "," + "\n" + "\"itemDescription\": " + "\"" + iDescription + "\"";

            String[] zaQuery = {"transactions", jsonInputString, type.toString()};
            transactionDetailPresenter.addTransaction(zaQuery);
        }
    }

    @Override
    public void vratiSeNaView() {

    }

    @Override
    public void obrisiTransakciju() {
        transactionDetailPresenter.deleteTransaction(idT);
    }

    @Override
    public void vratiSeNazad() {
        onClick.onClickButtons();
    }

    public boolean isConnected() {
        return ((ConnectivityManager) getContext().getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
}
