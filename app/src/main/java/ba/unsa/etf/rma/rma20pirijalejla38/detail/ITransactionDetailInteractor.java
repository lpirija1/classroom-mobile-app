package ba.unsa.etf.rma.rma20pirijalejla38.detail;

import android.content.Context;

import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public interface ITransactionDetailInteractor {
    Transaction getTransaction(Context context, Integer id);
    void dodajTransakciju(Context context, int idT, Date datum, double iznos, String title, Transaction.TransactionType type, String itemDescription, String tInterval, String endDate, String akcija);
    void obrisiTransakcijuUBazi(Context context, int idTransakcije);
    void izmijeniTransakciju(Context context, int idT, Date datum, double iznos, String title, Transaction.TransactionType type, String itemDescription, String tInterval, String endDate, String akcija);
}
