package ba.unsa.etf.rma.rma20pirijalejla38.detailAccount;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;
import ba.unsa.etf.rma.rma20pirijalejla38.data.AccountsModel;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.IAccountListInteractor;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionDBOpenHelper;

public class AccountListInteractor extends AsyncTask<String, Integer, Void> implements IAccountListInteractor {
    private OnAccountSearchDone caller;
    private EditAccount caller1;
    private AccountZbogBrisanjaTransakcije caller2;
    private AccountZbogEditovanjaTransakcije caller3;
    private static Account  account1;
    private  Account account2;


    public AccountListInteractor() {
    }

    public interface OnAccountSearchDone{
        public void onDone(Account account);
    }

    public interface EditAccount{
        public void onDone(Account account1, Account account2);
    }

    public interface AccountZbogBrisanjaTransakcije{
        void onDone(Account account, String brisanje);
    }
    public interface AccountZbogEditovanjaTransakcije{
        void onDone(Account account1, String editovanje, boolean pomocna);
    }

    public AccountListInteractor(OnAccountSearchDone p) {
        caller = p;
    };

    public AccountListInteractor(EditAccount p) {
        caller1 = p;
    }

    public AccountListInteractor(AccountZbogBrisanjaTransakcije p) {
        caller2 = p;
    }

    public AccountListInteractor(AccountZbogEditovanjaTransakcije p) {caller3 = p;}

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }


    @Override
    protected Void doInBackground(String... strings) {
        String query = null;
        try {
            query = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/" + query + "/f3237344-c9ef-46aa-ad98-f549896c2ab9";
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jsonObject = new JSONObject(result);

                int id = jsonObject.getInt("id");
                double budget = jsonObject.getDouble("budget");
                double totalLimit = jsonObject.getDouble("totalLimit");
                double monthLimit = jsonObject.getDouble("monthLimit");
                String acHash = jsonObject.getString("acHash");
                String email = jsonObject.getString("email");
                account1 = new Account(budget, totalLimit, monthLimit, id, acHash, email);
                AccountsModel.account.setBudget(account1.getBudget());
                AccountsModel.account.setTotalLimit(account1.getTotalLimit());
                AccountsModel.account.setMonthLimit(account1.getMonthLimit());
                AccountsModel.account.setId(account1.getId());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        if(strings.length>1 && strings[1] != null){
            String url2 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/f3237344-c9ef-46aa-ad98-f549896c2ab9";
            URL url = null;
            //1 - budzet, 2- totalLimit, 3- monthLimit, 4-id, 5-acHash, 6- email

            account1.setBudget(Double.parseDouble(strings[1]));
            account1.setTotalLimit(Double.parseDouble(strings[2]));
            account1.setMonthLimit(Double.parseDouble(strings[3]));
            AccountsModel.account.setBudget(account1.getBudget());
            AccountsModel.account.setTotalLimit(account1.getTotalLimit());
            AccountsModel.account.setMonthLimit(account1.getMonthLimit());
            AccountsModel.account.setId(account1.getId());
            account2=null;
            String jsonInputString = "{\n"+
                    "\"budget\":"  + Double.parseDouble(strings[1]) + "," + "\n" +
                    "\"totalLimit\":" + Double.parseDouble(strings[2]) + "," + "\n" +
                      "\"monthLimit\":" + Double.parseDouble(strings[3]) + "\n" + "}";
            System.out.println(jsonInputString);
            try {
                url = new URL(url2);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.connect();
                try(OutputStream os = con.getOutputStream()){
                    byte[] input = jsonInputString.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = con.getResponseCode();
                System.out.println(code);

                try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))){
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    System.out.println(response.toString());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);;
        if(caller != null) caller.onDone(account1);
        if(caller1 != null) caller1.onDone(account1, account2);
        if(caller2 != null) caller2.onDone(account1, "brisanje");
        if(caller3 != null) caller3.onDone(account1, "edit",true);
    }

    @Override
    public void updateAccountInDB(Context context, int id, double iznos, double monthLimit, double totalLimit) {
        Cursor cursor = getAccountCursor(context);
        double budget = 0;
        if(cursor.moveToFirst()) {
             budget = cursor.getDouble(cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET));
        }
        ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.ACCOUNT_ID, id);
        values.put(TransactionDBOpenHelper.ACCOUNT_BUDGET, iznos+budget);
        values.put(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT, monthLimit);
        values.put(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT, totalLimit);

        ContentResolver resolver = context.getApplicationContext().getContentResolver();
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"), id);
        String where = TransactionDBOpenHelper.ACCOUNT_ID + "=" +id;
        String[] whereArgs = null;
        resolver.update(uri, values, where, whereArgs);
    }

     @Override
     public void dodajAccountUBazu(Context context) {
         ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.ACCOUNT_ID, account1.getId());
         values.put(TransactionDBOpenHelper.ACCOUNT_BUDGET, account1.getBudget());
         values.put(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT, account1.getMonthLimit());
         values.put(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT, account1.getTotalLimit());


         ContentResolver resolver = context.getApplicationContext().getContentResolver();
         Uri uri = Uri.parse("content://rma.provider.account/elements");
         resolver.insert(uri, values);
     }

    @Override
    public void kreirajAccountUBazi(Context context) {
        ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.ACCOUNT_ID, 0);
        values.put(TransactionDBOpenHelper.ACCOUNT_BUDGET, 0);
        values.put(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT, 0);
        values.put(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT, 0);


        ContentResolver resolver = context.getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://rma.provider.account/elements");
        resolver.insert(uri, values);
    }


    @Override
    public Account dajAccountOffline() {
        return account1;
    }

    @Override
    public Cursor getAccountCursor(Context context) {
        ContentResolver cr = context.getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.ACCOUNT_INTERNAL_ID,
                TransactionDBOpenHelper.ACCOUNT_ID,
                TransactionDBOpenHelper.ACCOUNT_BUDGET,
                TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT,
                TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT,
        };
        Uri adresa = Uri.parse("content://rma.provider.account/elements");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cur = cr.query(adresa,kolone,where,whereArgs,order);
        return cur;
    }

    @Override
    public void deleteAccountInDB(Context context, int id){
        ContentResolver resolver = context.getApplicationContext().getContentResolver();
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"), id);
        String where = TransactionDBOpenHelper.ACCOUNT_ID + "=" + id;
        String[] whereArgs = null;

        resolver.delete(uri, where, whereArgs);
    }

    }
