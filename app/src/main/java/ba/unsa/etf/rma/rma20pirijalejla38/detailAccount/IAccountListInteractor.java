package ba.unsa.etf.rma.rma20pirijalejla38.detailAccount;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;

public interface IAccountListInteractor {
    Account dajAccountOffline();
    void dodajAccountUBazu(Context context);
    void updateAccountInDB(Context context, int id, double iznos, double monthLimit, double totalLimit);
    Cursor getAccountCursor(Context context);
    void kreirajAccountUBazi(Context context);
    void deleteAccountInDB(Context context, int id);
}
