package ba.unsa.etf.rma.rma20pirijalejla38.list;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.FrameLayout;


import java.util.ArrayList;
import java.util.Date;


import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Account;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;
import ba.unsa.etf.rma.rma20pirijalejla38.detail.TransactionDetailFragment;
import ba.unsa.etf.rma.rma20pirijalejla38.detailAccount.BudgetFragment;
import ba.unsa.etf.rma.rma20pirijalejla38.graphs.GraphsFragment;

public class MainActivity extends AppCompatActivity implements TransactionListFragment.OnItemClick, TransactionDetailFragment.OnClickButton, BudgetFragment.OnClicButtonsInBudgetFragment, GraphsFragment.OnClickButtonsInFragmentGraphs {
   private boolean twoPaneMode = false;
    private ConnectivityBroadcastReceiver receiver = new ConnectivityBroadcastReceiver();
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
    public static int pokretanjeOnline = 0;
    public static int pokretanjeOffline = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FrameLayout details = findViewById(R.id.transaction_detail);
            if (details != null) {
                twoPaneMode = true;
                TransactionDetailFragment detailFragment = (TransactionDetailFragment) fragmentManager.findFragmentById(R.id.transaction_detail);
                detailFragment = new TransactionDetailFragment();
                fragmentManager.beginTransaction().
                        replace(R.id.transaction_detail, detailFragment)
                        .commit();
            } else {
                twoPaneMode = false;
            }
            Fragment listFragment = fragmentManager.findFragmentById(R.id.transactions_list);
            if (listFragment == null) {
                listFragment = new TransactionListFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.transactions_list, listFragment)
                        .commit();
            } else {
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
    }

    @Override
    public void onItemClicked(Integer transactionID) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("transaction", transactionID);
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, detailFragment)
                    .commit();
        }
        else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transactions_list,detailFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }


    @Override
    public void onItemClicked(Boolean inDatabase, int id) {
        Bundle arguments = new Bundle();
        if (!inDatabase)
            arguments.putInt("id", id);
        else
            arguments.putInt("internal_id",id);
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, detailFragment)
                    .commit();
        }
        else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transactions_list,detailFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }


    @Override
    public void onItemClick(Transaction transaction, Double monthLimit, Double totalLimit) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("transaction", transaction);
        arguments.putSerializable("monthLimit", monthLimit);
        arguments.putSerializable("totalLimit", totalLimit);
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, detailFragment)
                    .commit();
        }
        else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transactions_list,detailFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }


    @Override
    public void openFragmentRight(Date datum) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("datum", datum);
        GraphsFragment graphsFragment = new GraphsFragment();
        graphsFragment.setArguments(arguments);
        if(!twoPaneMode) {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, graphsFragment).addToBackStack(null).commit();
        }

    }

    @Override
    public void openFragmentLeft(Date datum) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("datum", datum);
        BudgetFragment budgetFragment = new BudgetFragment();
        budgetFragment.setArguments(arguments);
        if(!twoPaneMode) {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, budgetFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onClickButtons() {
        Bundle arguments1 = new Bundle();
        if (twoPaneMode){
            TransactionListFragment t = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transactions_list);
            t.changeList();
            TransactionDetailFragment detailFragment = new TransactionDetailFragment();
            detailFragment.setArguments(arguments1);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, detailFragment)
                    .commit();
        }
        else {
            Fragment listFragment = new TransactionListFragment();
            listFragment.setArguments(arguments1);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transactions_list, listFragment).addToBackStack(null)
                    .commit();
        }

    }


    @Override
    public void onClickButtonRightFragmentForBudget() {
     Bundle arguments = new Bundle();
     TransactionListFragment listFragment = new TransactionListFragment();
     listFragment.setArguments(arguments);
     if(!twoPaneMode) {
         getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, listFragment).addToBackStack(null).commit();
     }
    }

    @Override
    public void onClickButtonLeftFragmentForBudget(Date datum) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("datum", datum);
        GraphsFragment graphsFragment = new GraphsFragment();
        graphsFragment.setArguments(arguments);
        if(!twoPaneMode) {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, graphsFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onClickLeftFragmentForGraphs() {
        Bundle arguments = new Bundle();
        TransactionListFragment listFragment = new TransactionListFragment();
        listFragment.setArguments(arguments);
        if(!twoPaneMode) {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, listFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onClickRightFragmentForGraphs(Date datum) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("datum", datum);
        BudgetFragment budgetFragment = new BudgetFragment();
        budgetFragment.setArguments(arguments);
        if(!twoPaneMode) {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_list, budgetFragment).addToBackStack(null).commit();
        }
    }


    @Override
    public void onClickButtons(Transaction transaction) {
        Bundle arguments1 = new Bundle();
        arguments1.putParcelable("transaction", transaction);
        if (twoPaneMode){
            TransactionListFragment t = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transactions_list);
            t.changeList();
            TransactionDetailFragment detailFragment = new TransactionDetailFragment();
            detailFragment.setArguments(arguments1);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, detailFragment)
                    .commit();
        }
        else {
            Fragment listFragment = new TransactionListFragment();
            listFragment.setArguments(arguments1);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transactions_list, listFragment).addToBackStack(null)
                    .commit();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }
}
