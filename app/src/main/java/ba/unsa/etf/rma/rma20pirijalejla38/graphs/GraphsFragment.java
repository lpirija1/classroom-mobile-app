package ba.unsa.etf.rma.rma20pirijalejla38.graphs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20pirijalejla38.R;
import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;
import ba.unsa.etf.rma.rma20pirijalejla38.list.OnSwipeTouchListener;
import ba.unsa.etf.rma.rma20pirijalejla38.list.TransactionListPresenter;

public class GraphsFragment extends Fragment implements IGraphView{
    private BarChart barChartPotrosnja;
    private BarChart barChartZarada;
    private BarChart barChartUkupnoStanje;
    private Spinner spinnerJedinica;
    private IGraphsPresenter graphsPresenter;
    private int trenutnaGodina;
    private int trenutniMjesec;
    private OnClickButtonsInFragmentGraphs onclickButton;
    private Calendar c = Calendar.getInstance();
    private Date datum = c.getTime();

    public IGraphsPresenter getPresenter() {
        if (graphsPresenter == null) {
            graphsPresenter = new GraphsPresenter(getContext(), this);
        }
        return graphsPresenter;
    }


    public interface OnClickButtonsInFragmentGraphs {
        void onClickLeftFragmentForGraphs();
        void onClickRightFragmentForGraphs(Date datum);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.graphs_fragment, container, false);
        getPresenter();
        onclickButton = (OnClickButtonsInFragmentGraphs) getActivity();
       graphsPresenter.getListTransactios("transactions");

        view.setOnTouchListener(new OnSwipeTouchListener(getContext()){
            @Override
            public void onSwipeLeft() {
                onclickButton.onClickLeftFragmentForGraphs();
            }

            @Override
            public void onSwipeRight() {
                onclickButton.onClickRightFragmentForGraphs((Date) getArguments().getSerializable("datum"));
            }
        });

        if(getArguments() != null && getArguments().containsKey("datum")){
            graphsPresenter.setDatum(getArguments().getSerializable("datum"));
            SimpleDateFormat f1 = new SimpleDateFormat("MM");
            SimpleDateFormat f2 = new SimpleDateFormat("yyyy");
            trenutniMjesec = Integer.parseInt(f1.format(datum));
            trenutnaGodina = Integer.parseInt(f2.format(datum));
        }

        barChartPotrosnja = (BarChart)view.findViewById(R.id.barChartPotrosnja);
        barChartZarada = (BarChart) view.findViewById(R.id.barChartZarada);
        barChartUkupnoStanje = (BarChart) view.findViewById(R.id.barChartUkupnoStanje);
        spinnerJedinica = (Spinner) view.findViewById(R.id.spinnerJedinica);



        String[] myItems= getResources().getStringArray(R.array.jedinice);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.select_dialog_item, myItems);
        spinnerJedinica.setAdapter(arrayAdapter);
        spinnerJedinica.setSelection(0);

        spinnerJedinica.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinnerJedinica.getSelectedItemPosition() == 0) {
                    BarDataSet barDataSet1 = new BarDataSet(getDataForMonthsPotrosnja(), "Inducesmile");
                    barDataSet1.setBarBorderWidth(0.9f);
                    barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData1 = new BarData(barDataSet1);
                    XAxis xAxis1 = barChartPotrosnja.getXAxis();
                    xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM);
                    final String[] months = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
                    IndexAxisValueFormatter formatter1 = new IndexAxisValueFormatter(months);
                    xAxis1.setGranularity(1f);
                    xAxis1.setValueFormatter(formatter1);
                    barChartPotrosnja.setData(barData1);
                    barChartPotrosnja.setFitBars(true);
                    barChartPotrosnja.animateXY(5000, 5000);
                    barChartPotrosnja.invalidate();


                    BarDataSet barDataSet2 = new BarDataSet(getDataForMonthsZarada(), "Inducesmile");
                    barDataSet2.setBarBorderWidth(0.3f);
                    barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData2 = new BarData(barDataSet2);
                    XAxis xAxis2 = barChartPotrosnja.getXAxis();
                    xAxis2.setPosition(XAxis.XAxisPosition.BOTTOM);
                    IndexAxisValueFormatter formatter2 = new IndexAxisValueFormatter(months);
                    xAxis2.setGranularity(1f);
                    xAxis2.setValueFormatter(formatter2);
                    barChartZarada.setData(barData2);
                    barChartZarada.setFitBars(true);
                    barChartZarada.animateXY(5000, 5000);
                    barChartZarada.invalidate();

                    BarDataSet barDataSet3 = new BarDataSet(getDataForMonthsUkupno(), "Inducesmile");
                    barDataSet3.setBarBorderWidth(0.3f);
                    barDataSet3.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData3 = new BarData(barDataSet3);
                    XAxis xAxis3 = barChartPotrosnja.getXAxis();
                    xAxis3.setPosition(XAxis.XAxisPosition.BOTTOM);
                    IndexAxisValueFormatter formatter3 = new IndexAxisValueFormatter(months);
                    xAxis3.setGranularity(1f);
                    xAxis3.setValueFormatter(formatter3);
                    barChartUkupnoStanje.setData(barData3);
                    barChartUkupnoStanje.setFitBars(true);
                    barChartUkupnoStanje.animateXY(5000, 5000);
                    barChartUkupnoStanje.invalidate();
                }
                else if(spinnerJedinica.getSelectedItemPosition() == 1) {
                    int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                    if (trenutnaGodina % 4 == 0 && trenutnaGodina % 100 != 0 || trenutnaGodina % 400 == 0)
                        brojDana[1]++;
                    BarDataSet barDataSet1 = new BarDataSet(getDataForDayPotrosnja(), "Inducesmile");
                    barDataSet1.setBarBorderWidth(0.9f);
                    barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData1 = new BarData(barDataSet1);
                    XAxis xAxis1 = barChartPotrosnja.getXAxis();
                    xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM);

                    String[] days = new String[brojDana[trenutniMjesec-1]];
                    for(int i = 1; i <= brojDana[trenutniMjesec-1]; i++){
                        days[i-1] = Integer.toString(i);
                    }
                    IndexAxisValueFormatter formatter1 = new IndexAxisValueFormatter(days);
                    xAxis1.setGranularity(1f);
                    xAxis1.setValueFormatter(formatter1);
                    barChartPotrosnja.setData(barData1);
                    barChartPotrosnja.setFitBars(true);
                    barChartPotrosnja.animateXY(5000, 5000);
                    barChartPotrosnja.invalidate();

                    BarDataSet barDataSet2 = new BarDataSet(getDataForDayZarada(), "Inducesmile");
                    barDataSet2.setBarBorderWidth(0.3f);
                    barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData2 = new BarData(barDataSet2);
                    XAxis xAxis2 = barChartPotrosnja.getXAxis();
                    xAxis2.setPosition(XAxis.XAxisPosition.BOTTOM);
                    IndexAxisValueFormatter formatter2 = new IndexAxisValueFormatter(days);
                    xAxis2.setGranularity(1f);
                    xAxis2.setValueFormatter(formatter2);
                    barChartZarada.setData(barData2);
                    barChartZarada.setFitBars(true);
                    barChartZarada.animateXY(5000, 5000);
                    barChartZarada.invalidate();

                    BarDataSet barDataSet3 = new BarDataSet(getUkupnoZaDan(), "Inducesmile");
                    barDataSet3.setBarBorderWidth(0.3f);
                    barDataSet3.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData3 = new BarData(barDataSet3);
                    XAxis xAxis3 = barChartPotrosnja.getXAxis();
                    xAxis3.setPosition(XAxis.XAxisPosition.BOTTOM);
                    IndexAxisValueFormatter formatter3 = new IndexAxisValueFormatter(days);
                    xAxis3.setGranularity(1f);
                    xAxis3.setValueFormatter(formatter3);
                    barChartUkupnoStanje.setData(barData3);
                    barChartUkupnoStanje.setFitBars(true);
                    barChartUkupnoStanje.animateXY(5000, 5000);
                    barChartUkupnoStanje.invalidate();
                }
                else {
                    BarDataSet barDataSet1 = new BarDataSet(getDataForWeekPotrosnja(), "Inducesmile");
                    barDataSet1.setBarBorderWidth(0.9f);
                    barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData1 = new BarData(barDataSet1);
                    XAxis xAxis1 = barChartPotrosnja.getXAxis();
                    xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM);
                    final String[] weeks = new String[]{"1", "2", "3", "4", "5"};
                    IndexAxisValueFormatter formatter1 = new IndexAxisValueFormatter(weeks);
                    xAxis1.setGranularity(1f);
                    xAxis1.setValueFormatter(formatter1);
                    barChartPotrosnja.setData(barData1);
                    barChartPotrosnja.setFitBars(true);
                    barChartPotrosnja.animateXY(5000, 5000);
                    barChartPotrosnja.invalidate();

                    BarDataSet barDataSet2 = new BarDataSet(getDataForWeekZarada(), "Inducesmile");
                    barDataSet2.setBarBorderWidth(0.3f);
                    barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData2 = new BarData(barDataSet2);
                    XAxis xAxis2 = barChartPotrosnja.getXAxis();
                    xAxis2.setPosition(XAxis.XAxisPosition.BOTTOM);
                    IndexAxisValueFormatter formatter2 = new IndexAxisValueFormatter(weeks);
                    xAxis2.setGranularity(1f);
                    xAxis2.setValueFormatter(formatter2);
                    barChartZarada.setData(barData2);
                    barChartZarada.setFitBars(true);
                    barChartZarada.animateXY(5000, 5000);
                    barChartZarada.invalidate();

                    BarDataSet barDataSet3 = new BarDataSet(getDataForWeekUkupnoStanje(), "Inducesmile");
                    barDataSet3.setBarBorderWidth(0.3f);
                    barDataSet3.setColors(ColorTemplate.COLORFUL_COLORS);
                    BarData barData3 = new BarData(barDataSet3);
                    XAxis xAxis3 = barChartPotrosnja.getXAxis();
                    xAxis3.setPosition(XAxis.XAxisPosition.BOTTOM);
                    IndexAxisValueFormatter formatter3 = new IndexAxisValueFormatter(weeks);
                    xAxis3.setGranularity(1f);
                    xAxis3.setValueFormatter(formatter3);
                    barChartUkupnoStanje.setData(barData3);
                    barChartUkupnoStanje.setFitBars(true);
                    barChartUkupnoStanje.animateXY(5000, 5000);
                    barChartUkupnoStanje.invalidate();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }});
        return view;
    }

    private ArrayList getDataForMonthsPotrosnja(){
        ArrayList<BarEntry> entries = new ArrayList<>();
        for(int i = 1; i <= 12; i++) {
            double iznosZaMjesec = graphsPresenter.dajPotrosnjuZaMjesec(i, trenutnaGodina, datum);
            entries.add(new BarEntry(i, (float) iznosZaMjesec));
        }

        return entries;
    }
    private ArrayList getDataForMonthsZarada(){
        ArrayList<BarEntry> entries = new ArrayList<>();
        for(int i = 1; i <= 12; i++) {
            double iznosZaMjesec = graphsPresenter.dajZaraduZaMjesec(i, trenutnaGodina, datum);
            entries.add(new BarEntry(i, (float) iznosZaMjesec));
        }

        return entries;
    }

    private ArrayList getDataForMonthsUkupno(){
        ArrayList<BarEntry> entries = new ArrayList<>();
        for(int i = 1; i <= 12; i++) {
            double iznosZaMjesec = graphsPresenter.dajUkupanIznosZaMjesec(i, trenutnaGodina, datum);
            entries.add(new BarEntry(i, (float) iznosZaMjesec));
        }

        return entries;
    }

    private ArrayList getDataForDayPotrosnja() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (trenutnaGodina % 4 == 0 && trenutnaGodina % 100 != 0 || trenutnaGodina % 400 == 0)
            brojDana[1]++;
        double iznosZaDan = 0;
        Calendar pomocna = Calendar.getInstance();
        pomocna.set(Calendar.DAY_OF_MONTH, 1);
        pomocna.set(Calendar.MONTH, trenutniMjesec-1);
        pomocna.set(Calendar.YEAR, trenutnaGodina);
        System.out.println(pomocna.getTime());
        for(int i = 1; i <= brojDana[trenutniMjesec-1]; i++) {
            iznosZaDan = graphsPresenter.dajPotrosnjuZaDan(pomocna.getTime());
            entries.add(new BarEntry(i, (float) iznosZaDan));
            pomocna.set(Calendar.DAY_OF_MONTH, i+1);
            System.out.println("promjena" + pomocna.getTime());
        }
        return entries;
    }

    private ArrayList getDataForDayZarada() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (trenutnaGodina % 4 == 0 && trenutnaGodina % 100 != 0 || trenutnaGodina % 400 == 0)
            brojDana[1]++;
        double iznosZaDan = 0;
        Calendar pomocna = Calendar.getInstance();
        pomocna.set(Calendar.DAY_OF_MONTH, 1);
        pomocna.set(Calendar.MONTH, trenutniMjesec-1);
        pomocna.set(Calendar.YEAR, trenutnaGodina);
        for(int i = 1; i <= brojDana[trenutniMjesec-1]; i++) {
            iznosZaDan = graphsPresenter.dajZaraduZaDan(pomocna.getTime());
            entries.add(new BarEntry(i, (float) iznosZaDan));
            pomocna.set(Calendar.DAY_OF_MONTH, i+1);
        }
        return entries;
    }

    private ArrayList getUkupnoZaDan() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        int[] brojDana = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (trenutnaGodina % 4 == 0 && trenutnaGodina % 100 != 0 || trenutnaGodina % 400 == 0)
            brojDana[1]++;
        double iznosZaDan = 0;
        Calendar pomocna = Calendar.getInstance();
        pomocna.set(Calendar.DAY_OF_MONTH, 1);
        pomocna.set(Calendar.MONTH, trenutniMjesec-1);
        pomocna.set(Calendar.YEAR, trenutnaGodina);
        for(int i = 1; i <= brojDana[trenutniMjesec-1]; i++) {
            iznosZaDan = graphsPresenter.dajUkupnoStanjeZaDan(pomocna.getTime());
            entries.add(new BarEntry(i, (float) iznosZaDan));
            pomocna.set(Calendar.DAY_OF_MONTH, i+1);
        }
        return entries;

    }
    private ArrayList getDataForWeekPotrosnja() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        for(int i = 1; i <= 5; i++) {
            double iznosZaSedmicu = graphsPresenter.dajPotrosnjuZaSedmicu(trenutniMjesec, trenutnaGodina, i);
            entries.add(new BarEntry(i, (float) iznosZaSedmicu));
        }
        return entries;
    }

    private ArrayList getDataForWeekZarada() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        for(int i = 1; i <= 5; i++) {
            double iznosZaSedmicu = graphsPresenter.dajZaraduZaSedmicu(trenutniMjesec, trenutnaGodina, i);
            entries.add(new BarEntry(i, (float) iznosZaSedmicu));
        }
        return entries;
    }

    private ArrayList getDataForWeekUkupnoStanje() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        for(int i = 1; i <= 5; i++) {
            double iznosZaSedmicu = graphsPresenter.dajUkupanIznosZaSedmicu(trenutniMjesec, trenutnaGodina, i);
            entries.add(new BarEntry(i, (float) iznosZaSedmicu));
        }
        return entries;
    }
   @Override
   public void setDate() {
       BarDataSet barDataSet1 = new BarDataSet(getDataForMonthsPotrosnja(), "Inducesmile");
       barDataSet1.setBarBorderWidth(0.9f);
       barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
       BarData barData1 = new BarData(barDataSet1);
       XAxis xAxis1 = barChartPotrosnja.getXAxis();
       xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM);
       final String[] months = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
       IndexAxisValueFormatter formatter1 = new IndexAxisValueFormatter(months);
       xAxis1.setGranularity(1f);
       xAxis1.setValueFormatter(formatter1);
       barChartPotrosnja.setData(barData1);
       barChartPotrosnja.setFitBars(true);
       barChartPotrosnja.animateXY(5000, 5000);
       barChartPotrosnja.invalidate();


       BarDataSet barDataSet2 = new BarDataSet(getDataForMonthsZarada(), "Inducesmile");
       barDataSet2.setBarBorderWidth(0.3f);
       barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
       BarData barData2 = new BarData(barDataSet2);
       XAxis xAxis2 = barChartPotrosnja.getXAxis();
       xAxis2.setPosition(XAxis.XAxisPosition.BOTTOM);
       IndexAxisValueFormatter formatter2 = new IndexAxisValueFormatter(months);
       xAxis2.setGranularity(1f);
       xAxis2.setValueFormatter(formatter2);
       barChartZarada.setData(barData2);
       barChartZarada.setFitBars(true);
       barChartZarada.animateXY(5000, 5000);
       barChartZarada.invalidate();

       BarDataSet barDataSet3 = new BarDataSet(getDataForMonthsUkupno(), "Inducesmile");
       barDataSet3.setBarBorderWidth(0.3f);
       barDataSet3.setColors(ColorTemplate.COLORFUL_COLORS);
       BarData barData3 = new BarData(barDataSet3);
       XAxis xAxis3 = barChartPotrosnja.getXAxis();
       xAxis3.setPosition(XAxis.XAxisPosition.BOTTOM);
       IndexAxisValueFormatter formatter3 = new IndexAxisValueFormatter(months);
       xAxis3.setGranularity(1f);
       xAxis3.setValueFormatter(formatter3);
       barChartUkupnoStanje.setData(barData3);
       barChartUkupnoStanje.setFitBars(true);
       barChartUkupnoStanje.animateXY(5000, 5000);
       barChartUkupnoStanje.invalidate();
   }



}
