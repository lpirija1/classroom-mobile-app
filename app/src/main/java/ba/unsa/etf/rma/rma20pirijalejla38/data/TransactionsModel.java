package ba.unsa.etf.rma.rma20pirijalejla38.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20pirijalejla38.data.Transaction;

public class TransactionsModel {
   public static ArrayList<Transaction> transactions = new ArrayList<Transaction>(){
        {/*

            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            try {
                add(new Transaction(format.parse("24-03-2020"), 10000, "Payment", Transaction.TransactionType.INDIVIDUALINCOME, null, 0, null));
            add(new Transaction(format.parse("21-04-2020"), 250, "CommonStock", Transaction.TransactionType.INDIVIDUALPAYMENT, "To recognize issuance of common stock", 0, null));
            add(new Transaction(format.parse("11-02-2020"), 300, "Transaction", Transaction.TransactionType.REGULARPAYMENT, "to use resources", 30, format.parse("11-11-2020")));
            add(new Transaction(format.parse("15-06-2020"), 3000, "SalariesExpense", Transaction.TransactionType.REGULARPAYMENT, "Paid employee salaries", 30, format.parse("15-12-2020")));
            add(new Transaction(format.parse("08-08-2020"), 1000, "ServiceRevenue", Transaction.TransactionType.PURCHASE, "Collected cach for services rendered", 0, null));
            add(new Transaction(format.parse("10-07-2020"), 2000, "Equipiment", Transaction.TransactionType.REGULARINCOME, null, 30, format.parse("10-11-2020")));
            add(new Transaction(format.parse("05-05-2020"), 50, "UtilityExpense", Transaction.TransactionType.REGULARPAYMENT, "Paid utility bill", 30, format.parse("50-10-2020")));
            add(new Transaction(format.parse("07-06-2020"), 800, "Payment2", Transaction.TransactionType.INDIVIDUALINCOME, null, 0, null));
            add(new Transaction(format.parse("24-05-2020"), 2500, "AccountsPayable", Transaction.TransactionType.PURCHASE, "Paid liability for equipiment in full", 0, null));
            add(new Transaction(format.parse("09-09-2020"), 4000, "TaxeAndLicences", Transaction.TransactionType.INDIVIDUALPAYMENT, "paid registration and licensing fees for the business", 0, null));
            add(new Transaction(format.parse("15-07-2020"), 500, "Rent", Transaction.TransactionType.REGULARPAYMENT, "Rent Payment",30, format.parse("15-11-2020")));
            add(new Transaction(format.parse("23-04-2020"), 5000, "BuyingSpace", Transaction.TransactionType.INDIVIDUALPAYMENT, "Bying office space", 0, null));
            add(new Transaction(format.parse("01-06-2020"), 1300, "ComputerSales", Transaction.TransactionType.PURCHASE, "Payment to sell a computer", 0, null));
            add(new Transaction(format.parse("25-05-2020"), 1000, "DebitCollection", Transaction.TransactionType.REGULARINCOME, null, 30, format.parse("25-10-2020")));
            add(new Transaction(format.parse("10-04-2020"), 500, "Services", Transaction.TransactionType.REGULARINCOME, null, 30,format.parse("10-10-2020")));
            add(new Transaction(format.parse("02-02-2020"), 10000, "CarSale", Transaction.TransactionType.INDIVIDUALINCOME, null, 0, null));
            add(new Transaction(format.parse("29-03-2020"), 5000, "EquipimentSale", Transaction.TransactionType.INDIVIDUALINCOME, null, 0, null));
            add(new Transaction(format.parse("01-04-2020"), 7000, "BuyingACar", Transaction.TransactionType.INDIVIDUALPAYMENT, "Buying a car for a company", 0,  null));
            add(new Transaction(format.parse("01-01-2020"), 100, "Parking", Transaction.TransactionType.REGULARPAYMENT, "for parking use", 30, format.parse("01-11-2020")));
            add(new Transaction(format.parse("13-05-2020"), 800, "HotelServices", Transaction.TransactionType.INDIVIDUALPAYMENT, "Payment for hotel services", 0, null));
            } catch (ParseException e) {
                e.printStackTrace();
            }*/
            }};

}
